/************************************************************//**
*
*	@file: gui.h
*	@author: Martin Fouilleul
*	@date: 05/03/2021
*	@revision:
*
*****************************************************************/
#ifndef __GUI_H_
#define __GUI_H_

#ifdef __cplusplus
extern "C" {
#endif

const u32 MP_GUI_MAX_INPUT_CHAR_PER_FRAME = 16;

typedef struct mp_gui_input_mouse
{
	f32 x;
	f32 y;
	f32 deltaX;
	f32 deltaY;
	f32 wheelX;
	f32 wheelY;
	mp_key_state buttons[MP_MOUSE_BUTTON_COUNT];

} mp_gui_input_mouse;

typedef struct mp_gui_input_text
{
	u8 count;
	u32 codePoints[MP_GUI_MAX_INPUT_CHAR_PER_FRAME];

} mp_gui_input_text;

typedef struct mp_gui_io
{
	f64 beginFrameTime;

	mp_gui_input_mouse mouse;
	mp_key_state keys[MP_KEY_COUNT];
	mp_key_mods  mods;
	mp_gui_input_text text;

	f32 displayWidth;
	f32 displayHeight;

} mp_gui_io;

typedef u64 mp_gui_id;

typedef enum
{
	MP_GUI_STYLE_COLOR_BG = 0,
	MP_GUI_STYLE_COLOR_BG_HOVER,
	MP_GUI_STYLE_COLOR_BG_ACTIVE,

	MP_GUI_STYLE_COLOR_BORDER,
	MP_GUI_STYLE_COLOR_BORDER_HOVER,
	MP_GUI_STYLE_COLOR_BORDER_ACTIVE,

	MP_GUI_STYLE_COLOR_ELT,
	MP_GUI_STYLE_COLOR_ELT_HOVER,
	MP_GUI_STYLE_COLOR_ELT_ACTIVE,

	MP_GUI_STYLE_COLOR_TEXT,
	MP_GUI_STYLE_COLOR_TEXT_HOVER,
	MP_GUI_STYLE_COLOR_TEXT_ACTIVE,
	MP_GUI_STYLE_COLOR_TEXT_SELECTION_BG,
	MP_GUI_STYLE_COLOR_TEXT_SELECTION_FG,

	MP_GUI_STYLE_COLOR_VIEW_BG,
	MP_GUI_STYLE_COLOR_VIEW_TITLE_BG,
	MP_GUI_STYLE_COLOR_VIEW_TITLE_TEXT,
	MP_GUI_STYLE_COLOR_VIEW_BORDER,

	MP_GUI_STYLE_COLOR_SCROLLBAR,
	MP_GUI_STYLE_COLOR_SCROLLBAR_ACTIVE,

	MP_GUI_STYLE_COLOR_MAX
} mp_gui_style_color_id;

typedef u32 mp_gui_style_flags;
const mp_gui_style_flags MP_GUI_STYLE_NO_FRAME = 1,
                         MP_GUI_SYLE_NO_BORDER = 1<<1;

typedef enum { MP_GUI_STYLE_TEXT_ALIGN_CENTER = 0,
               MP_GUI_STYLE_TEXT_ALIGN_LEFT,
	       MP_GUI_STYLE_TEXT_ALIGN_RIGHT } mp_gui_style_text_align;

typedef struct mp_gui_style
{
	mp_gui_style_flags flags;
	mp_gui_style_text_align textAlign;
	mp_graphics_font font;
	f32 fontSize;

	f32 borderWidth;
	f32 borderWidthHover;
	f32 borderWidthActive;

	f32 viewBorderWidth;

	f32 frameRoundness;
	f32 elementRoundess;
	f32 scrollbarRoundness;

	f32 margin;
	f32 titleHeight;

	mp_graphics_color colors[MP_GUI_STYLE_COLOR_MAX];
} mp_gui_style;

typedef struct mp_gui_context mp_gui_context;

typedef u32 mp_gui_view_flags;
const mp_gui_view_flags MP_GUI_VIEW_FLAG_ROOT = 1,
                     MP_GUI_VIEW_FLAG_TITLED  = 1<<1,
		     //scrolling
		     MP_GUI_VIEW_FLAG_VSCROLL = 1<<2,
		     MP_GUI_VIEW_FLAG_HSCROLL = 1<<3,
		     MP_GUI_VIEW_FLAG_SCROLL  = MP_GUI_VIEW_FLAG_HSCROLL
		                              | MP_GUI_VIEW_FLAG_VSCROLL,
		     //resizing
		     MP_GUI_VIEW_FLAG_RESIZEABLE_LEFT   = 1<<4,
		     MP_GUI_VIEW_FLAG_RESIZEABLE_RIGHT  = 1<<5,
		     MP_GUI_VIEW_FLAG_RESIZEABLE_TOP    = 1<<6,
		     MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM = 1<<7,
		     MP_GUI_VIEW_FLAG_RESIZEABLE = MP_GUI_VIEW_FLAG_RESIZEABLE_LEFT
		                              | MP_GUI_VIEW_FLAG_RESIZEABLE_RIGHT
					      | MP_GUI_VIEW_FLAG_RESIZEABLE_TOP
					      | MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM,

		     MP_GUI_VIEW_FLAG_START_CLOSED = 1<<8;

mp_gui_context* mp_gui_context_create(f32 displayWidth, f32 displayHeight, mp_graphics_context graphics, mp_graphics_font font);
void mp_gui_context_destroy(mp_gui_context* context);

mp_gui_io* mp_gui_context_get_io(mp_gui_context* context);
void mp_gui_process_event(mp_gui_io* io, mp_event* event);
void mp_gui_input_next_frame(mp_gui_io* input);

f64 mp_gui_time(mp_gui_context* gui);

mp_gui_id mp_gui_id_from_string(mp_gui_context* gui, const char* str);
mp_gui_id mp_gui_id_from_u64(mp_gui_context* gui, u64 val);
mp_gui_id mp_gui_push_id(mp_gui_context* gui, mp_gui_id id);
mp_gui_id mp_gui_push_id_string(mp_gui_context* gui, const char* name);
mp_gui_id mp_gui_push_id_u64(mp_gui_context* gui, u64 val);
void mp_gui_pop_id(mp_gui_context* gui);

bool mp_gui_is_active(mp_gui_context* gui, mp_gui_id id);
bool mp_gui_is_hovered(mp_gui_context* gui, mp_gui_id id);
bool mp_gui_is_focus(mp_gui_context* gui, mp_gui_id id);
void mp_gui_set_active(mp_gui_context* gui, mp_gui_id id);
void mp_gui_set_hovered(mp_gui_context* gui, mp_gui_id id);
void mp_gui_set_focus(mp_gui_context* gui, mp_gui_id id);

mp_gui_id mp_gui_get_active_id(mp_gui_context* context);
mp_gui_id mp_gui_get_hovered_id(mp_gui_context* context);
mp_gui_id mp_gui_get_focus_id(mp_gui_context* context);

void mp_gui_transform_pop(mp_gui_context* gui);
void mp_gui_transform_push(mp_gui_context* gui, f32 x, f32 y);

void mp_gui_clip_pop(mp_gui_context* gui);
void mp_gui_clip_push(mp_gui_context* gui, mp_aligned_rect clip);

mp_gui_style* mp_gui_style_top(mp_gui_context* gui);
void mp_gui_style_pop(mp_gui_context* gui);
void mp_gui_style_push(mp_gui_context* gui, mp_gui_style* style);

f32 mp_gui_mouse_x(mp_gui_context* gui);
f32 mp_gui_mouse_y(mp_gui_context* gui);
f32 mp_gui_mouse_delta_x(mp_gui_context* gui);
f32 mp_gui_mouse_delta_y(mp_gui_context* gui);
f32 mp_gui_mouse_wheel_x(mp_gui_context* gui);
f32 mp_gui_mouse_wheel_y(mp_gui_context* gui);

bool mp_gui_is_mouse_over(mp_gui_context* gui, mp_aligned_rect box);
bool mp_gui_is_mouse_button_pressed(mp_gui_context* gui, mp_mouse_button button);
bool mp_gui_is_mouse_button_down(mp_gui_context* gui, mp_mouse_button button);
bool mp_gui_is_mouse_button_simple_clicked(mp_gui_context* gui, mp_mouse_button button);
bool mp_gui_is_mouse_button_double_clicked(mp_gui_context* gui, mp_mouse_button button);
bool mp_gui_is_mouse_button_released(mp_gui_context* gui, mp_mouse_button button);

bool mp_gui_is_key_pressed(mp_gui_context* gui, mp_key_code key);
mp_key_mods mp_gui_key_mods(mp_gui_context* gui);

bool mp_gui_button_behaviour(mp_gui_context* gui, mp_gui_id id, mp_aligned_rect box);

void mp_gui_view_set_frame(mp_gui_context* gui, mp_aligned_rect frame);
void mp_gui_view_set_contents_dimensions(mp_gui_context* gui, f32 width, f32 height);
void mp_gui_view_set_scroll(mp_gui_context* gui, f32 scrollX, f32 scrollY);

mp_aligned_rect mp_gui_view_get_frame(mp_gui_context* gui);
void mp_gui_view_get_contents_dimensions(mp_gui_context* gui, f32* width, f32* height);
void mp_gui_view_get_scroll(mp_gui_context* gui, f32* scrollX, f32* scrollY);

mp_aligned_rect mp_gui_view_get_next_frame(mp_gui_context* gui);
bool mp_gui_view_frame_changed(mp_gui_context* gui);

void mp_gui_next_view_frame(mp_gui_context* gui, mp_aligned_rect frame);


typedef u32 mp_gui_widget_signals;
const mp_gui_widget_signals MP_GUI_WIDGET_IS_HOVERED = 1<<0,
                            MP_GUI_WIDGET_IS_ACTIVE  = 1<<1,
			    MP_GUI_WIDGET_HAS_FOCUS  = 1<<2,
                            MP_GUI_WIDGET_ENTER      = 1<<3,
			    MP_GUI_WIDGET_LEAVE      = 1<<4,
			    MP_GUI_WIDGET_ACTIVATE   = 1<<5,
			    MP_GUI_WIDGET_DEACTIVATE = 1<<6,
			    MP_GUI_WIDGET_TRIGGER    = 1<<7,
			    MP_GUI_WIDGET_GOT_FOCUS  = 1<<8,
			    MP_GUI_WIDGET_LOST_FOCUS = 1<<9;

typedef u32 mp_gui_widget_flags;
const mp_gui_widget_flags MP_GUI_WIDGET_FLAG_CAN_FOCUS = 1,
                          MP_GUI_WIDGET_FLAG_FOCUS_DOUBLE_CLICK = 1<<1;

mp_gui_widget_signals mp_gui_widget_behaviour(mp_gui_context* gui, mp_gui_id id, mp_aligned_rect box, mp_gui_widget_flags flags);


void mp_gui_draw_label(mp_gui_context* gui, const char* text, mp_aligned_rect box, mp_graphics_color textColor);
void mp_gui_draw_string(mp_gui_context* gui, mp_string string, mp_aligned_rect box, mp_gui_style* style);

bool mp_gui_begin_view(mp_gui_context* gui, const char* title, mp_aligned_rect initFrame, mp_gui_view_flags flags);
void mp_gui_end_view(mp_gui_context* gui);

void mp_gui_begin_frame(mp_gui_context* gui);
void mp_gui_end_frame(mp_gui_context* gui);

bool mp_gui_text_button(mp_gui_context* gui, const char* label, mp_aligned_rect box);
bool mp_gui_collapse_button(mp_gui_context* gui, const char* label, mp_aligned_rect box, bool* collapsed, f32* animation);

bool mp_gui_checkbox(mp_gui_context* gui, const char* label, mp_aligned_rect box, bool* checked);

typedef u32 mp_gui_text_field_flags;
const mp_gui_text_field_flags MP_GUI_TEXT_FIELD_FOCUS_DOUBLE_CLICK = 1,
                              MP_GUI_TEXT_FIELD_NO_HOVER_CURSOR    = 1<<1,
                              MP_GUI_TEXT_FIELD_AUTOSELECT_ALL     = 1<<2,
                              MP_GUI_TEXT_FILTER_DIGIT             = 1<<16,
                              MP_GUI_TEXT_FILTER_ALPHA             = 1<<17,
                              MP_GUI_TEXT_FILTER_DOT               = 1<<18,
                              MP_GUI_TEXT_FILTER_MASK              = MP_GUI_TEXT_FILTER_DIGIT
                                                                   | MP_GUI_TEXT_FILTER_ALPHA
                                                                   | MP_GUI_TEXT_FILTER_DOT;

bool mp_gui_text_field(mp_gui_context* gui,
                       const char* label,
		       mp_aligned_rect box,
		       u32 maxLen,
		       u32 *textSize,
		       u32* text,
		       mp_gui_text_field_flags flags);


//WARN(martin): temporary helpers added for QED convenience.

mp_graphics_context mp_gui_get_graphics(mp_gui_context* gui);

void mp_gui_cancel_text_input(mp_gui_context* gui);

void mp_gui_set_mouse_cursor(mp_gui_context* gui, mp_mouse_cursor cursor);
mp_mouse_cursor mp_gui_get_mouse_cursor(mp_gui_context* gui);

bool mp_gui_int_text_field(mp_gui_context* gui, const char* label, mp_aligned_rect box, int* number, mp_gui_text_field_flags flags);
bool mp_gui_float_text_field(mp_gui_context* gui, const char* label, mp_aligned_rect box, float* number, mp_gui_text_field_flags flags);

void mp_gui_open_popup(mp_gui_context* gui, const char* label);
bool mp_gui_begin_popup(mp_gui_context* gui, const char* label, mp_aligned_rect box);
void mp_gui_end_popup(mp_gui_context* gui);
bool mp_gui_popup_menu(mp_gui_context* gui, const char* label, mp_aligned_rect buttonBox, u32 optionCount, mp_string* options, int* optionIndex);

bool mp_gui_slider(mp_gui_context* gui, const char* label, mp_aligned_rect box, f32 thumbSize, f32* value);

void mp_gui_view_close(mp_gui_context* gui);

#ifdef __cplusplus
} // extern "C"
#endif


#endif //__GUI_H_
