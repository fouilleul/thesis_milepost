/************************************************************//**
*
*	@file: gui.c
*	@author: Martin Fouilleul
*	@date: 05/03/2021
*	@revision:
*
*****************************************************************/
#include<ctype.h> // isalpha()
#include"gui.h"
#include"platform_clock.h" // get timestamp...

#define LOG_SUBSYSTEM "GUI"


const u32 MP_GUI_VIEW_STACK_MAX_DEPTH = 64,
          MP_GUI_VIEW_MAX_COUNT = 64,
          MP_GUI_CLIP_STACK_MAX_DEPTH = 64,
	  MP_GUI_TRANSFORM_STACK_MAX_DEPTH = 64,
	  MP_GUI_ID_STACK_MAX_DEPTH = 128,
	  MP_GUI_STYLE_STACK_MAX_DEPTH = 64;

const u32 MP_GUI_HASH_INITIAL_SEED = 5381;

const u32 MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS = 8,
          MP_GUI_VIEW_MIN_WIDTH = 100,
	  MP_GUI_VIEW_MIN_HEIGHT = 100;

const f32 MP_GUI_SCROLL_SCALE = 10;

typedef struct mp_gui_view
{
	//...
	list_elt listElt;

	mp_gui_id id;
	mp_gui_view_flags flags;

	bool open;
	u64 lastFrameDidOpen;
	bool wasClosedInScope;
	bool used;
	i32 z;
	mp_aligned_rect frame;    // frame, including title bar
	mp_aligned_rect viewport; // view port relative to frame

	f32 refX;
	f32 refY;

	f32 scrollX;
	f32 scrollY;
	f32 contentsWidth;
	f32 contentsHeight;

	mp_graphics_stream stream;
	mp_graphics_stream parentStream;

	u8 resizeLeft;
	u8 resizeRight;
	u8 resizeTop;
	u8 resizeBottom;
	bool frameChanged;

	mp_aligned_rect nextFrame;
	f32 nextContentsWidth;
	f32 nextContentsHeight;
	f32 nextScrollX;
	f32 nextScrollY;


	//DEBUG
	int debugTransformStackDepth;
	int debugClipStackDepth;

} mp_gui_view;

typedef struct mp_gui_transform
{
	f32 x;
	f32 y;
} mp_gui_transform;

const u32 MP_GUI_EDIT_BUFFER_MAX_SIZE = 256;

typedef struct mp_gui_context
{
	mp_graphics_context graphics;

	mp_gui_io input;

	u64 frameCounter;

	mp_gui_id prevHovered;
	mp_gui_id prevActive;
	mp_gui_id prevFocus;
	mp_gui_id hovered;
	mp_gui_id active;
	mp_gui_id focus;

	mp_gui_view* hoveredRoot;
	mp_gui_view* nextHoveredRoot;
	mp_gui_view* hoveredView;
	mp_gui_view* nextHoveredView;

	mp_gui_view* frontView;

	u32 nextZ;

	bool nextViewFrameSet;
	mp_aligned_rect nextViewFrame;

	mp_mouse_cursor mouseCursor;

	//NOTE(martin): text input
	f64 editCursorBlinkStart;
	u32 firstDisplayedChar;
	u32 editCursor;
	u32 editMark;
	u32 editBufferSize; // _not_ including a null codepoint
	u32 editBuffer[MP_GUI_EDIT_BUFFER_MAX_SIZE];

	//NOTE(martin): stacks
	mp_gui_id idStack[MP_GUI_ID_STACK_MAX_DEPTH];
	u32 idStackSize;

	mp_gui_view* viewStack[MP_GUI_VIEW_STACK_MAX_DEPTH];
	u32 viewStackSize;

	mp_aligned_rect clipStack[MP_GUI_CLIP_STACK_MAX_DEPTH];
	u32 clipStackSize;

	mp_gui_transform transformStack[MP_GUI_TRANSFORM_STACK_MAX_DEPTH];
	u32 transformStackSize;

	mp_gui_style styleStack[MP_GUI_STYLE_STACK_MAX_DEPTH];
	u32 styleStackSize;
	mp_gui_style defaultStyle;

	//NOTE(martin): root views and view move-to-front list
	mp_gui_view* rootViews[MP_GUI_VIEW_STACK_MAX_DEPTH];
	u32 rootViewCount;

	list_info views;
	mp_gui_view viewPool[MP_GUI_VIEW_MAX_COUNT];

} mp_gui_context;

mp_gui_context* mp_gui_context_create(f32 displayWidth, f32 displayHeight, mp_graphics_context graphics, mp_graphics_font font)
{
	mp_gui_context* context = malloc_type(mp_gui_context);
	memset(context, 0, sizeof(mp_gui_context));

	context->graphics = graphics;

	ListInit(&context->views);
	for(int i=0; i<MP_GUI_VIEW_MAX_COUNT; i++)
	{
		ListAppend(&context->views, &context->viewPool[i].listElt);
	}

	context->input.displayWidth = displayWidth;
	context->input.displayHeight = displayHeight;

	//NOTE(martin): init style
	context->defaultStyle = (mp_gui_style){
		.font = font,
		.fontSize = 34,
		.borderWidth = 2,
		.borderWidthHover = 2,
		.borderWidthActive = 4,
		.viewBorderWidth = 2,
		.frameRoundness = 0,
		.elementRoundess = 0,
		.scrollbarRoundness = 1,
		.margin = 10,
		.titleHeight = 80,

		.colors = {
			[MP_GUI_STYLE_COLOR_BG] = {0.2, 0.2, 0.2, 1},
			[MP_GUI_STYLE_COLOR_BG_HOVER] = {0.2, 0.2, 0.2, 1},
			[MP_GUI_STYLE_COLOR_BG_ACTIVE] = {0.2, 0.2, 0.2, 1},

			[MP_GUI_STYLE_COLOR_BORDER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_BORDER_HOVER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_BORDER_ACTIVE] = {1, 1, 1, 1},

			[MP_GUI_STYLE_COLOR_ELT] = {0.5, 0.5, 0.5, 1},
			[MP_GUI_STYLE_COLOR_ELT_HOVER] = {0.5, 0.5, 0.5, 1},
			[MP_GUI_STYLE_COLOR_ELT_ACTIVE] = {0.1, 0.1, 0.1, 1},

			[MP_GUI_STYLE_COLOR_TEXT] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_HOVER] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_ACTIVE] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_SELECTION_BG] = {0, 0, 1, 1},
			[MP_GUI_STYLE_COLOR_TEXT_SELECTION_FG] = {1, 1, 1, 1},

			[MP_GUI_STYLE_COLOR_VIEW_BG] = {0.3, 0.3, 0.3, 1},
			[MP_GUI_STYLE_COLOR_VIEW_TITLE_BG] = {0.1, 0.1, 0.1, 1},
			[MP_GUI_STYLE_COLOR_VIEW_TITLE_TEXT] = {1, 1, 1, 1},
			[MP_GUI_STYLE_COLOR_VIEW_BORDER] = {1, 1, 1, 1},

			[MP_GUI_STYLE_COLOR_SCROLLBAR] = {0.2, 0.2, 0.2, 1},
			[MP_GUI_STYLE_COLOR_SCROLLBAR_ACTIVE] = {0.1, 0.1, 0.1, 1},
		}
	};

	return(context);
}

void mp_gui_context_destroy(mp_gui_context* context)
{
	free(context);
}

void mp_gui_process_event(mp_gui_io* input, mp_event* event)
{
	switch(event->type)
	{
		case MP_EVENT_WINDOW_RESIZE:
		{
			input->displayWidth = event->frame.rect.w * 2;
			input->displayHeight = event->frame.rect.h * 2;
		} break;

		case MP_EVENT_MOUSE_MOVE:
		{
			input->mods = event->move.mods;

			input->mouse.deltaX += event->move.x * 2 - input->mouse.x;
			input->mouse.deltaY += (input->displayHeight - event->move.y * 2) - input->mouse.y;
			input->mouse.x = event->move.x * 2;
			input->mouse.y = input->displayHeight - event->move.y * 2;

		} break;

		case MP_EVENT_MOUSE_WHEEL:
		{
			input->mouse.wheelX += event->move.deltaX * MP_GUI_SCROLL_SCALE;
			input->mouse.wheelY += event->move.deltaY * MP_GUI_SCROLL_SCALE;
		} break;

		case MP_EVENT_MOUSE_BUTTON:
		{
			input->mods = event->key.mods;

			if(event->key.action == MP_KEY_PRESS)
			{
				input->mouse.buttons[event->key.code] |= MP_KEY_STATE_PRESSED;
				input->mouse.buttons[event->key.code] |= MP_KEY_STATE_DOWN;

				if(event->key.clickCount > 1)
				{
					input->mouse.buttons[event->key.code] |= MP_KEY_STATE_DOUBLE_CLICKED;
				}
				else
				{
					input->mouse.buttons[event->key.code] |= MP_KEY_STATE_SIMPLE_CLICKED;
				}
			}
			else
			{
				input->mouse.buttons[event->key.code] &= ~MP_KEY_STATE_DOWN;
				input->mouse.buttons[event->key.code] |= MP_KEY_STATE_RELEASED;
			}

		} break;

		case MP_EVENT_KEYBOARD_CHAR:
		{
			//TODO(martin): shouldn't check seqLen while we only store 1 codepoint at a time ?
			if(input->text.count + event->character.seqLen < MP_GUI_MAX_INPUT_CHAR_PER_FRAME)
			{
				input->text.codePoints[input->text.count] = event->character.codepoint;
				input->text.count++;
			}
		} break;

		case MP_EVENT_KEYBOARD_MODS:
			input->mods = event->key.mods;
			break;

		case MP_EVENT_KEYBOARD_KEY:
		{
			input->mods = event->key.mods;

			if(event->key.action == MP_KEY_RELEASE)
			{
				input->keys[event->key.code] &= ~MP_KEY_STATE_DOWN;
				input->keys[event->key.code] |= MP_KEY_STATE_RELEASED;
			}
			else if(  event->key.action == MP_KEY_PRESS
			       || event->key.action == MP_KEY_REPEAT)
			{
				//TODO: maybe distinguish pressed and repeat ?
				input->keys[event->key.code] |= (MP_KEY_STATE_DOWN | MP_KEY_STATE_PRESSED);
			}
		} break;

		default:
			break;
	}
}

u32 mp_gui_hash_string(const char* str, u32 seed)
{
	u32 hash = seed;
	int c;
	while((c = *str++))
	{
		hash = ((hash<<5)+hash)+c;
	}
	return(hash);
}

u32 mp_gui_hash_data(u32 size, const char* data, u32 seed)
{
	u32 hash = seed;
	int c;
	for(u32 i = 0; i<size; i++)
	{
		c = data[i];
		hash = ((hash<<5)+hash)+c;
	}
	return(hash);
}

mp_gui_id mp_gui_id_from_string(mp_gui_context* gui, const char* str)
{
	u32 seed = gui->idStackSize ? gui->idStack[gui->idStackSize-1] : MP_GUI_HASH_INITIAL_SEED;
	mp_gui_id id = mp_gui_hash_string(str, seed);
	#ifdef DEBUG
	if(!id)
	{
		LOG_ERROR("id generated from string '%s' is null\n", str);
	}
	#endif
	return(id);
}

mp_gui_id mp_gui_id_from_u64(mp_gui_context* gui, u64 val)
{
	u32 seed = gui->idStackSize ? gui->idStack[gui->idStackSize-1] : MP_GUI_HASH_INITIAL_SEED;
	mp_gui_id id = mp_gui_hash_data(sizeof(val), (const char*)&val, seed);
	#ifdef DEBUG
	if(!id)
	{
		LOG_ERROR("id generated from u64 value '%llu' is null\n", val);
	}
	#endif
	return(id);
}

mp_gui_id mp_gui_push_id(mp_gui_context* gui, mp_gui_id id)
{
	if(gui->idStackSize < MP_GUI_ID_STACK_MAX_DEPTH)
	{
		gui->idStack[gui->idStackSize] = id;
		gui->idStackSize++;
		return(id);
	}
	else
	{
		LOG_ERROR("gui id stack overflow\n");
		return(0);
	}
}

mp_gui_id mp_gui_push_id_string(mp_gui_context* gui, const char* name)
{
	mp_gui_id id = mp_gui_id_from_string(gui, name);
	return(mp_gui_push_id(gui, id));
}

mp_gui_id mp_gui_push_id_u64(mp_gui_context* gui, u64 val)
{
	mp_gui_id id = mp_gui_id_from_u64(gui, val);
	return(mp_gui_push_id(gui, id));
}

void mp_gui_pop_id(mp_gui_context* gui)
{
	if(gui->idStackSize)
	{
		gui->idStackSize--;
	}
	else
	{
		LOG_ERROR("trying to pop a gui id scope out of an empty id stack\n");
	}
}

bool mp_gui_is_active(mp_gui_context* gui, mp_gui_id id)
{
	return(gui->active == id);
}

bool mp_gui_is_hovered(mp_gui_context* gui, mp_gui_id id)
{
	return(gui->hovered == id);
}

bool mp_gui_is_focus(mp_gui_context* gui, mp_gui_id id)
{
	return(gui->focus == id);
}

void mp_gui_set_active(mp_gui_context* gui, mp_gui_id id)
{
	gui->active = id;
}

void mp_gui_set_hovered(mp_gui_context* gui, mp_gui_id id)
{
	gui->hovered = id;
}

void mp_gui_set_focus(mp_gui_context* gui, mp_gui_id id)
{
	gui->focus = id;
}

mp_gui_id mp_gui_get_active_id(mp_gui_context* context)
{
	return(context->active);
}

mp_gui_id mp_gui_get_hovered_id(mp_gui_context* context)
{
	return(context->hovered);
}

mp_gui_id mp_gui_get_focus_id(mp_gui_context* context)
{
	return(context->focus);
}

mp_gui_view* mp_gui_view_find(mp_gui_context* gui, mp_gui_id id)
{
	for_each_in_list(&gui->views, view, mp_gui_view, listElt)
	{
		if(view->id == id)
		{
			return(view);
		}
	}
	return(0);
}

mp_gui_view* mp_gui_view_alloc(mp_gui_context* gui, mp_gui_id id)
{
	//NOTE(martin): steal last element (least recently used), and reset it
	list_elt* last = ListLast(&gui->views);
	mp_gui_view* view = ListEntry(last, mp_gui_view, listElt);

	ASSERT(!view->used, "too many used views\n");

	ListRemove(&gui->views, &view->listElt);
	memset(view, 0, sizeof(mp_gui_view));

//	ListAppend(&gui->views, &view->listElt);
	ListPush(&gui->views, &view->listElt);

	//TODO: we don't necessarily want to put it back at the end of the view list
	//TODO: init view
	view->id = id;
	view->open = 1;

	//...

	return(view);
}

void mp_gui_view_begin_used(mp_gui_context* gui, mp_gui_view* view)
{
	view->used = true;
	ListRemove(&gui->views, &view->listElt);
	ListPush(&gui->views, &view->listElt);
}

void mp_gui_view_end_used(mp_gui_view* view)
{
	view->used = false;
}


void mp_gui_view_push(mp_gui_context* gui, mp_gui_view* view)
{
	if(gui->viewStackSize >= MP_GUI_VIEW_STACK_MAX_DEPTH)
	{
		LOG_ERROR("gui view stack overflow\n");
	}
	else
	{
		gui->viewStack[gui->viewStackSize] = view;
		gui->viewStackSize++;
	}
}

void mp_gui_view_pop(mp_gui_context* gui)
{
	if(gui->viewStackSize <= 0)
	{
		LOG_ERROR("gui view stack underflow\n");
	}
	else
	{
		gui->viewStackSize--;
	}
}

mp_gui_view* mp_gui_view_top(mp_gui_context* gui)
{
	return((gui->viewStackSize > 0) ? gui->viewStack[gui->viewStackSize-1] : 0);
}

mp_gui_transform mp_gui_transform_top(mp_gui_context* gui)
{
	return(gui->transformStackSize > 0 ?
	       gui->transformStack[gui->transformStackSize-1] :
	       (mp_gui_transform){0, 0});
}

void mp_gui_transform_pop(mp_gui_context* gui)
{
	if(gui->transformStackSize <= 0)
	{
		LOG_ERROR("gui transform stack underflow\n");
	}
	else
	{
		gui->transformStackSize--;
	}
//	mp_graphics_matrix_pop(gui->graphics);
}

void mp_gui_transform_push(mp_gui_context* gui, f32 x, f32 y)
{
	if(gui->transformStackSize >= MP_GUI_TRANSFORM_STACK_MAX_DEPTH)
	{
		LOG_ERROR("gui transform stack overflow\n");
	}
	else
	{
		mp_gui_transform tr = mp_gui_transform_top(gui);
		tr.x += x;
		tr.y += y;
		gui->transformStack[gui->transformStackSize] = tr;
		gui->transformStackSize++;
	}
//	mp_graphics_matrix_push(gui->graphics, (mp_mat2x3){1, 0, -x, 0, 1, -y});
}

mp_aligned_rect mp_gui_clip_top(mp_gui_context* gui)
{
	return(gui->clipStackSize > 0 ?
	       gui->clipStack[gui->clipStackSize-1] :
	       (mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX});
}

void mp_gui_clip_pop(mp_gui_context* gui)
{
	ASSERT(gui->clipStackSize != 0);
	if(gui->clipStackSize <= 0)
	{
		ASSERT(0);
		LOG_ERROR("gui clip stack underflow\n");
	}
	else
	{
		gui->clipStackSize--;
	}
}

mp_aligned_rect mp_gui_clip_intersect(mp_aligned_rect lhs, mp_aligned_rect rhs)
{
	//NOTE(martin): intersect with current clip
	f32 x0 = maximum(lhs.x, rhs.x);
	f32 y0 = maximum(lhs.y, rhs.y);
	f32 x1 = minimum(lhs.x + lhs.w, rhs.x + rhs.w);
	f32 y1 = minimum(lhs.y + lhs.h, rhs.y + rhs.h);

	mp_aligned_rect r = {x0, y0, maximum(0, x1-x0), maximum(0, y1-y0)};
	return(r);
}

void mp_gui_clip_push(mp_gui_context* gui, mp_aligned_rect clip)
{
	ASSERT(gui->clipStackSize < MP_GUI_CLIP_STACK_MAX_DEPTH);

	if(gui->clipStackSize >= MP_GUI_CLIP_STACK_MAX_DEPTH)
	{
		ASSERT(0);
		LOG_ERROR("gui clip stack overflow\n");
	}
	else
	{
		mp_gui_transform tr = mp_gui_transform_top(gui);
		clip.x -= tr.x;
		clip.y -= tr.y;
		clip = mp_gui_clip_intersect(clip, mp_gui_clip_top(gui));
		gui->clipStack[gui->clipStackSize] = clip;
		gui->clipStackSize++;
	}
}

void mp_gui_clip_push_root(mp_gui_context* gui, mp_aligned_rect clip)
{
	ASSERT(gui->clipStackSize < MP_GUI_CLIP_STACK_MAX_DEPTH);

	if(gui->clipStackSize >= MP_GUI_CLIP_STACK_MAX_DEPTH)
	{
		ASSERT(0);
		LOG_ERROR("gui clip stack overflow\n");
	}
	else
	{
		mp_gui_transform tr = mp_gui_transform_top(gui);
		clip.x -= tr.x;
		clip.y -= tr.y;
		gui->clipStack[gui->clipStackSize] = clip;
		gui->clipStackSize++;
	}
}

mp_gui_style* mp_gui_style_top(mp_gui_context* gui)
{
	return(gui->styleStackSize > 0 ?
	       &gui->styleStack[gui->styleStackSize-1] :
	       &gui->defaultStyle);
}

void mp_gui_style_pop(mp_gui_context* gui)
{
	if(gui->styleStackSize <= 0)
	{
		LOG_ERROR("gui style stack underflow\n");
	}
	else
	{
		gui->styleStackSize--;
	}
}

void mp_gui_style_push(mp_gui_context* gui, mp_gui_style* style)
{
	if(gui->styleStackSize >= MP_GUI_STYLE_STACK_MAX_DEPTH)
	{
		LOG_ERROR("gui style stack overflow\n");
	}
	else
	{
		gui->styleStack[gui->styleStackSize] = *style;
		gui->styleStackSize++;
	}
}

void mp_gui_view_order_front(mp_gui_context* gui, mp_gui_view* view)
{
	view->z = gui->nextZ++;
}


mp_gui_view* mp_gui_view_root(mp_gui_context* gui)
{
	for(int i=gui->viewStackSize-1; i>=0; i--)
	{
		if(gui->viewStack[i]->flags & MP_GUI_VIEW_FLAG_ROOT)
		{
			return(gui->viewStack[i]);
		}
	}
	return(0);
}

bool mp_gui_is_point_inside_rect(f32 x, f32 y, mp_aligned_rect rect)
{
	return((x > rect.x) && (x < rect.x + rect.w) && (y > rect.y) && (y < rect.y + rect.h));
}

bool mp_gui_is_mouse_over(mp_gui_context* gui, mp_aligned_rect box)
{
	mp_gui_view* root = mp_gui_view_top(gui);
	mp_aligned_rect clip = mp_gui_clip_top(gui);
	mp_gui_transform tr = mp_gui_transform_top(gui);
	f32 x = gui->input.mouse.x;
	f32 y = gui->input.mouse.y;

	return((root == gui->hoveredView)
	      && mp_gui_is_point_inside_rect(x, y, clip)
	      && mp_gui_is_point_inside_rect(x + tr.x, y + tr.y, box));
}
mp_gui_widget_signals mp_gui_widget_behaviour(mp_gui_context* gui, mp_gui_id id, mp_aligned_rect box, mp_gui_widget_flags flags)
{
	mp_gui_io* input = &gui->input;
	mp_gui_widget_signals signals = 0;

	if(mp_gui_is_mouse_over(gui, box))
	{
		gui->hovered = id;

		if(id != gui->prevHovered)
		{
			signals |= MP_GUI_WIDGET_ENTER;
		}
	}
	else
	{
		if(id == gui->hovered)
		{
			gui->hovered = 0;
		}
		if(id == gui->prevHovered)
		{
			signals |= MP_GUI_WIDGET_LEAVE;
		}
	}

	if( id == gui->focus
	  && id != gui->hovered
	  && (input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_PRESSED))
	{
		gui->focus = 0;
	}

	if(  id == gui->hovered
	  && (input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_PRESSED))
	{
		gui->active = id;
		if((flags & MP_GUI_WIDGET_FLAG_CAN_FOCUS)
		  && (!(flags & MP_GUI_WIDGET_FLAG_FOCUS_DOUBLE_CLICK)
		     ||(input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_DOUBLE_CLICKED)))
		{
			gui->focus = id;
		}
	}

	//TODO: handle fast clicks where we have press/release in the same frame ?
	if(  id == gui->active
	  && (input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_RELEASED))
	{
		if(id == gui->hovered)
		{
			signals |= MP_GUI_WIDGET_TRIGGER;
		}
		gui->active = 0;
	}

	if(id == gui->hovered)
	{
		signals |= MP_GUI_WIDGET_IS_HOVERED;
		if(id != gui->prevHovered)
		{
			signals |= MP_GUI_WIDGET_ENTER;
		}
	}
	else if(id == gui->prevHovered)
	{
		signals |= MP_GUI_WIDGET_LEAVE;
	}

	if(id == gui->active)
	{
		signals |= MP_GUI_WIDGET_IS_ACTIVE;
		if(id != gui->prevActive)
		{
			signals |= MP_GUI_WIDGET_ACTIVATE;
		}
	}
	else if(id == gui->prevActive)
	{
		signals |= MP_GUI_WIDGET_DEACTIVATE;
		if(id == gui->hovered)
		{
			signals |= MP_GUI_WIDGET_TRIGGER;
		}
	}
	if(id == gui->focus)
	{
		signals |= MP_GUI_WIDGET_HAS_FOCUS;
		if(id != gui->prevFocus)
		{
			signals |= MP_GUI_WIDGET_GOT_FOCUS;
		}
	}
	else if(id == gui->prevFocus)
	{
		signals |= MP_GUI_WIDGET_LOST_FOCUS;
	}

	return(signals);
}

bool mp_gui_button_behaviour(mp_gui_context* gui, mp_gui_id id, mp_aligned_rect box)
{
	mp_gui_widget_signals signals = mp_gui_widget_behaviour(gui, id, box, 0);
	return(signals & MP_GUI_WIDGET_TRIGGER);
}


void mp_gui_view_set_frame(mp_gui_context* gui, mp_aligned_rect frame)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		view->frame = frame;
	}
}

void mp_gui_view_set_contents_dimensions(mp_gui_context* gui, f32 width, f32 height)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		view->nextContentsWidth = width;
		view->nextContentsHeight = height;
	}
}

void mp_gui_view_set_scroll(mp_gui_context* gui, f32 scrollX, f32 scrollY)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		view->nextScrollX = scrollX;
		view->nextScrollY = scrollY;
	}
}

mp_aligned_rect mp_gui_view_get_frame(mp_gui_context* gui)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		return(view->frame);
	}
	return((mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX});
}

mp_aligned_rect mp_gui_view_get_next_frame(mp_gui_context* gui)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		return(view->nextFrame);
	}
	return((mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX});
}

bool mp_gui_view_frame_changed(mp_gui_context* gui)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		return(view->frameChanged);
	}
	return(false);
}

void mp_gui_next_view_frame(mp_gui_context* gui, mp_aligned_rect frame)
{
	gui->nextViewFrame = frame;
	gui->nextViewFrameSet = true;
}

void mp_gui_view_get_contents_dimensions(mp_gui_context* gui, f32* width, f32* height)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		*width = view->nextContentsWidth;
		*height = view->nextContentsHeight;
	}
}

void mp_gui_view_get_scroll(mp_gui_context* gui, f32* scrollX, f32* scrollY)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		*scrollX = view->nextScrollX;
		*scrollY = view->nextScrollY;
	}
}

void mp_gui_draw_string(mp_gui_context* gui,
                       mp_string string,
                       mp_aligned_rect box,
                       mp_gui_style* style)
{
	mp_graphics_color textColor = style->colors[MP_GUI_STYLE_COLOR_TEXT];

	//NOTE(martin): draw a text label centered in a box.
	//              intended for short labels: text has a max size of maxTextLength (see below)
	mp_graphics_context graphics = gui->graphics;

	//NOTE(martin): convert text to glyphs
	mp_string32 codePoints = utf8_push_to_codepoints(mem_scratch(), string);
	mp_string32 glyphs = mp_graphics_font_push_glyph_indices(graphics, style->font, mem_scratch(), codePoints);

	//NOTE(martin): compute text metrics
	mp_graphics_set_font(graphics, style->font);

	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);

	vec2 textDimensions = mp_graphics_get_glyphs_dimensions(graphics, glyphs);

	f32 textX = 0;
	switch(style->textAlign)
	{
		case MP_GUI_STYLE_TEXT_ALIGN_CENTER:
			textX = box.x + 0.5*(box.w - textDimensions.x);
			break;

		case MP_GUI_STYLE_TEXT_ALIGN_LEFT:
			textX = box.x + style->margin;
			break;

		case MP_GUI_STYLE_TEXT_ALIGN_RIGHT:
			textX = box.x + box.w - style->margin - textDimensions.x;
			break;
	}

	f32 baseLine =  box.y
		      + 0.5*(box.h + (fontExtents.capHeight + fontExtents.descent)*fontScale)
		      - fontExtents.descent*fontScale;

	//NOTE(martin): draw text
	mp_graphics_clip_push(graphics, box.x, box.y, box.w, box.h);

	mp_graphics_move_to(graphics, textX, baseLine);
	mp_graphics_glyph_outlines(graphics, glyphs);
	mp_graphics_set_color(graphics, textColor);
	mp_graphics_fill(graphics);

	mp_graphics_clip_pop(graphics);
}


void mp_gui_draw_label(mp_gui_context* gui,
                       const char* text,
		       mp_aligned_rect box,
		       mp_graphics_color textColor)
{
	mp_gui_style* style = mp_gui_style_top(gui);

	//NOTE(martin): draw a text label centered in a box.
	//              intended for short labels: text has a max size of maxTextLength (see below)
	mp_graphics_context graphics = gui->graphics;

	//NOTE(martin): convert text to glyphs
	mp_string32 codePoints = utf8_push_to_codepoints(mem_scratch(), mp_string_from_cstring((char*)text));
	mp_string32 glyphs = mp_graphics_font_push_glyph_indices(graphics, style->font, mem_scratch(), codePoints);

	//NOTE(martin): compute text metrics
	mp_graphics_set_font(graphics, style->font);

	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);

	vec2 textDimensions = mp_graphics_get_glyphs_dimensions(graphics, glyphs);

	f32 textX = 0;
	switch(style->textAlign)
	{
		case MP_GUI_STYLE_TEXT_ALIGN_CENTER:
			textX = box.x + 0.5*(box.w - textDimensions.x);
			break;

		case MP_GUI_STYLE_TEXT_ALIGN_LEFT:
			textX = box.x + style->margin;
			break;

		case MP_GUI_STYLE_TEXT_ALIGN_RIGHT:
			textX = box.x + box.w - style->margin - textDimensions.x;
			break;
	}

	f32 baseLine =  box.y
		      + 0.5*(box.h + (fontExtents.capHeight + fontExtents.descent)*fontScale)
		      - fontExtents.descent*fontScale;

	//NOTE(martin): draw text
	mp_graphics_clip_push(graphics, box.x, box.y, box.w, box.h);

	mp_graphics_move_to(graphics, textX, baseLine);
	mp_graphics_glyph_outlines(graphics, glyphs);
	mp_graphics_set_color(graphics, textColor);
	mp_graphics_fill(graphics);

	mp_graphics_clip_pop(graphics);
}

bool mp_gui_begin_view(mp_gui_context* gui, const char* title, mp_aligned_rect initFrame, mp_gui_view_flags flags)
{
	mp_graphics_context graphics = gui->graphics;
	mp_gui_io* input = &gui->input;
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_gui_view* parent = mp_gui_view_top(gui);

	//NOTE(martin): push the id and try to find the cached view
	mp_gui_id id = mp_gui_push_id_string(gui, title);
	mp_gui_view* view = mp_gui_view_find(gui, id);
	if(!view)
	{
		//NOTE(martin): if the view was not cached, create it
		view = mp_gui_view_alloc(gui, id);

		//TODO  init the view in alloc
		view->flags = flags;
		view->frame = initFrame;
		view->nextFrame = view->frame;
		view->viewport = view->frame;
		view->contentsWidth = view->frame.w;
		view->contentsHeight = view->frame.h;
		if(flags & MP_GUI_VIEW_FLAG_TITLED)
		{
			view->contentsHeight -= style->titleHeight;
		}
		view->open = (~flags & MP_GUI_VIEW_FLAG_START_CLOSED);
		if(view->open)
		{
			view->lastFrameDidOpen = gui->frameCounter;
			mp_gui_view_order_front(gui, view);
		}
	}
	view->debugTransformStackDepth = gui->transformStackSize;
	view->debugClipStackDepth = gui->clipStackSize;

	view->wasClosedInScope = false;
	mp_gui_view_push(gui, view);

	if(!view->open)
	{
		return(false);
	}
	else
	{
		mp_gui_view_begin_used(gui, view);
	}

	if(gui->nextViewFrameSet)
	{
		view->frame = gui->nextViewFrame;
		gui->nextViewFrameSet = false;
	}
	else
	{
		view->nextFrame = view->frame;
	}

	if(flags & MP_GUI_VIEW_FLAG_ROOT)
	{
		//NOTE(martin): add the view to the root list
		ASSERT(gui->rootViewCount < MP_GUI_VIEW_STACK_MAX_DEPTH);
		gui->rootViews[gui->rootViewCount] = view;
		gui->rootViewCount++;
	}
	//NOTE(martin): create and swap the view's graphics command stream
	view->stream = mp_graphics_stream_create(graphics);
	view->parentStream = mp_graphics_stream_swap(graphics, view->stream);

	//NOTE(martin): push the view and the view frame's transform.
	//WARN(martin): we must do this after creating the view graphics stream, but before testing
	//              if the view is hovered. (hence the redundant MP_GUI_VIEW_FLAG_ROOT check above and below)
	mp_gui_transform_push(gui, -view->frame.x, -view->frame.y);


	//if(flags & MP_GUI_VIEW_FLAG_ROOT)
	{
		//NOTE(martin): if mouse is inside frame and view is in front of current hoveredRoot candidate,
		//              view becomes the hoveredRoot candidate. We extend this rect test if view is frontview
		//              and resizeable, to allow resize handles to overflow the frame
		mp_aligned_rect hoverRect = {0, 0, view->frame.w, view->frame.h};

		if(view == gui->frontView)
		{
			if(flags & MP_GUI_VIEW_FLAG_RESIZEABLE_LEFT)
			{
				hoverRect.x -= MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS;
			}
			if(flags & MP_GUI_VIEW_FLAG_RESIZEABLE_RIGHT)
			{
				hoverRect.w += 2*MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS;
			}
			if(flags & MP_GUI_VIEW_FLAG_RESIZEABLE_TOP)
			{
				hoverRect.y -= MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS;
			}
			if(flags & MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM)
			{
				hoverRect.h += 2*MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS;
			}
		}

		mp_gui_view* rootView = mp_gui_view_root(gui);

		mp_gui_transform tr = mp_gui_transform_top(gui);

		if(	(view == rootView || !rootView || mp_gui_is_point_inside_rect(input->mouse.x, input->mouse.y, rootView->frame))
		  &&  mp_gui_is_point_inside_rect(input->mouse.x + tr.x, input->mouse.y + tr.y, hoverRect)
		  && (!gui->nextHoveredRoot || gui->nextHoveredRoot->z < view->z))
		{
			gui->nextHoveredRoot = rootView;
			gui->nextHoveredView = view;
		}

		//NOTE(martin): put view in front if it was clicked
		if(  mp_gui_is_mouse_over(gui, (mp_aligned_rect){0, 0, view->frame.w, view->frame.h})
		  && (input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_PRESSED)
		  && rootView)
		{
			mp_gui_view_order_front(gui, rootView);
		}
	}

	//NOTE(martin): resize
	bool resize = false;
	view->frameChanged = false;
	view->nextFrame = view->frame;

	if(  (flags & MP_GUI_VIEW_FLAG_RESIZEABLE)
	  && (  mp_gui_view_root(gui) == gui->frontView
	     || !mp_gui_view_root(gui)))
	{
		f32 sz = MP_GUI_VIEW_RESIZE_HANDLE_THICKNESS;
		mp_aligned_rect leftHandle = {-sz, -sz, 2*sz, view->frame.h + 2*sz};
		mp_aligned_rect rightHandle = {view->frame.w - sz, -sz, 2*sz, view->frame.h + 2*sz};
		mp_aligned_rect topHandle = {-sz, -sz, view->frame.w + 2*sz, 2*sz};
		mp_aligned_rect bottomHandle = {-sz, view->frame.h - sz, view->frame.w + 2*sz, 2*sz};

		u8 hoverResizeLeft = ((flags & MP_GUI_VIEW_FLAG_RESIZEABLE_LEFT) && mp_gui_is_mouse_over(gui, leftHandle)) ? 1 : 0;
		u8 hoverResizeRight = ((flags & MP_GUI_VIEW_FLAG_RESIZEABLE_RIGHT) && mp_gui_is_mouse_over(gui, rightHandle)) ? 1 : 0;
		u8 hoverResizeTop = ((flags & MP_GUI_VIEW_FLAG_RESIZEABLE_TOP) && mp_gui_is_mouse_over(gui, topHandle)) ? 1 : 0;
		u8 hoverResizeBottom = ((flags & MP_GUI_VIEW_FLAG_RESIZEABLE_BOTTOM) && mp_gui_is_mouse_over(gui, bottomHandle)) ? 1 : 0;

		if(input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_PRESSED)
		{
			view->resizeLeft = hoverResizeLeft ? 1 : 0;
			view->resizeRight = hoverResizeRight ? 1 : 0;
			view->resizeTop = hoverResizeTop ? 1 : 0;
			view->resizeBottom = hoverResizeBottom ? 1 : 0;
		}
		if(input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_RELEASED)
		{
			view->resizeLeft = 0;
			view->resizeRight = 0;
			view->resizeTop = 0;
			view->resizeBottom = 0;
		}
		resize = view->resizeLeft || view->resizeRight || view->resizeTop || view->resizeBottom;

		f32 maxResizeX = maximum(0, view->frame.w - MP_GUI_VIEW_MIN_WIDTH);
		f32 maxResizeY = maximum(0, view->frame.h - MP_GUI_VIEW_MIN_HEIGHT);

		f32 deltaLeft = minimum(input->mouse.deltaX, maxResizeX) * view->resizeLeft;
		f32 deltaRight = maximum(input->mouse.deltaX, -maxResizeX) * view->resizeRight;
		f32 deltaTop = minimum(input->mouse.deltaY, maxResizeY) * view->resizeTop;
		f32 deltaBottom = maximum(input->mouse.deltaY, -maxResizeY) * view->resizeBottom;

		view->nextFrame = view->frame;
		view->nextFrame.x += deltaLeft;
		view->nextFrame.y += deltaTop;
		view->nextFrame.w += deltaRight - deltaLeft;
		view->nextFrame.h += deltaBottom - deltaTop;

		view->frameChanged = deltaLeft || deltaRight || deltaTop || deltaBottom;
		if(view->frameChanged)
		{
			view->frame = view->nextFrame;
		}

		mp_gui_id resizeHandlesID = mp_gui_id_from_string(gui, "!resize_handles");

		//NOTE(martin): handle resize cursor changes
		if(resize || hoverResizeLeft || hoverResizeRight || hoverResizeTop || hoverResizeBottom)
		{
			gui->hovered = resizeHandlesID;
			// set cursor
			i32 cursorKind = hoverResizeLeft
				       | (hoverResizeRight<<1)
				       | (hoverResizeTop<<2)
				       | (hoverResizeBottom<<3);

			switch(cursorKind)
			{
				case 1: case 2: case 3:
					mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_RESIZE_0);
					break;
				case 4: case 8: case 12:
					mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_RESIZE_90);
					break;
				case 5: case 10:
					mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_RESIZE_135);
					break;
				case 6: case 9:
					mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_RESIZE_45);
					break;
			}
		}
		else if(resizeHandlesID == gui->hovered)
		{
			gui->hovered = 0;
			mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_ARROW);
		}
	}

	//NOTE(martin): move
	if((flags & MP_GUI_VIEW_FLAG_TITLED)
	  && !resize)
	{
		mp_aligned_rect titleBar = {0, 0, view->frame.w, style->titleHeight};
		if(mp_gui_is_mouse_over(gui, titleBar))
		{
			gui->hovered = id;
		}
		else if(id == gui->hovered)
		{
			gui->hovered = 0;
		}
		if(  id == gui->hovered
		  && input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_PRESSED)
		{
			gui->active = id;
		}
		if(id == gui->active)
		{
			if(!(input->mouse.buttons[MP_MOUSE_LEFT] & MP_KEY_STATE_DOWN))
			{
				gui->active = 0;
			}
			else
			{
				view->frame.x += input->mouse.deltaX;
				view->frame.y += input->mouse.deltaY;
			}
		}
		view->nextFrame = view->frame;
	}

	//NOTE(martin): set next contents
	view->nextContentsWidth = view->contentsWidth;
	view->nextContentsHeight = view->contentsHeight;
	view->nextScrollX = view->scrollX;
	view->nextScrollY = view->scrollY;

	//NOTE(martin): recompute viewport from frame
	view->viewport = (mp_aligned_rect){0, 0, view->frame.w, view->frame.h};
	if(flags & MP_GUI_VIEW_FLAG_TITLED)
	{
		view->viewport.y += style->titleHeight;
		view->viewport.h -= style->titleHeight;
	}

	//NOTE(martin): once the rects have been updated, update the frame's transform and  draw the view
	mp_gui_transform_pop(gui);
	mp_gui_transform tr = mp_gui_transform_top(gui);
	view->refX = -tr.x;
	view->refY = -tr.y;
	mp_gui_transform_push(gui, -view->frame.x, -view->frame.y);

	if(view->flags | MP_GUI_VIEW_FLAG_ROOT)
	{
		mp_gui_clip_push_root(gui, view->viewport);
	}
	else
	{
		mp_gui_clip_push(gui, view->viewport);
	}

	mp_gui_transform_push(gui, -view->viewport.x + view->scrollX, -view->viewport.y + view->scrollY);
    mp_graphics_matrix_push(gui->graphics, (mp_mat2x3){1, 0, -view->scrollX,
			                                           0, 1, -view->scrollY});

	return(true);
}

bool mp_gui_scroll_slider(mp_gui_context* gui, const char* label, mp_aligned_rect box, f32 thumbSize, f32* value)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_context graphics = gui->graphics;

	mp_gui_id id = mp_gui_push_id_string(gui, label);

	f32 nextValue = Clamp(*value, 0, 1);
	bool vertical = box.h > box.w;
	thumbSize = vertical ? Clamp(thumbSize, 1, box.h) : Clamp(thumbSize, 1, box.w);
	f32 runway = (vertical ? box.h : box.w) - thumbSize;

	mp_aligned_rect thumb = { vertical ? box.x : box.x + (box.w - thumbSize) * nextValue,
	                          vertical ? box.y + (box.h - thumbSize) * (1 - nextValue) : box.y,
				  vertical ? box.w : thumbSize,
				  vertical ? thumbSize : box.h};

	mp_gui_button_behaviour(gui, id, thumb);

	f32 delta = 0;
	if(id == gui->active)
	{
		delta = vertical ? -gui->input.mouse.deltaY : gui->input.mouse.deltaX;
		delta /= runway;
	}
	if(id == gui->hovered)
	{
		//NOTE(martin): wheel
		delta += vertical ? -gui->input.mouse.wheelY : gui->input.mouse.wheelX;
	}

	nextValue = Clamp(nextValue + delta, 0, 1);

	if(id == gui->active)
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_SCROLLBAR_ACTIVE]);
	}
	else
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_SCROLLBAR]);
	}
	f32 roundness = 0.5*minimum(box.w, box.h)*style->scrollbarRoundness;
	mp_graphics_rounded_rectangle_fill(graphics, thumb.x, thumb.y, thumb.w, thumb.h, roundness);

	mp_gui_pop_id(gui);

	if(nextValue != *value)
	{
		*value = nextValue;
		return(true);
	}
	else
	{
		return(false);
	}
}

void mp_gui_view_scrollbars(mp_gui_context* gui, mp_gui_view* view)
{
	//TODO put that in styling struct
	f32 margin = 5;
	f32 thickness = 20;
	bool hscroll = (view->flags & MP_GUI_VIEW_FLAG_HSCROLL) && view->contentsWidth > view->viewport.w;
	bool vscroll = (view->flags & MP_GUI_VIEW_FLAG_VSCROLL) && view->contentsHeight > view->viewport.h;

	if(hscroll)
	{
		mp_aligned_rect box = { margin, view->viewport.y + view->viewport.h - thickness - margin, view->viewport.w - 2*margin, thickness };
		if(vscroll)
		{
			box.w -= thickness;
		}
		f32 thumbSize = box.w * view->viewport.w / view->contentsWidth;
		f32 sliderValue = view->scrollX / (view->contentsWidth - view->viewport.w);

		if(mp_gui_scroll_slider(gui, "hscroll", box, thumbSize, &sliderValue))
		{
			view->scrollX = sliderValue * (view->contentsWidth - view->viewport.w);;
		}
		else if(mp_gui_is_mouse_over(gui, (mp_aligned_rect){0, 0, view->viewport.w, view->viewport.h}))
		{
			view->scrollX -= gui->input.mouse.wheelX;
		}
		view->scrollX = Clamp(view->scrollX, 0, view->contentsWidth - view->viewport.w);
	}
	if(vscroll)
	{
		mp_aligned_rect box = { view->viewport.w - thickness - margin, view->viewport.y + margin, thickness, view->viewport.h - 2*margin};
		if(hscroll)
		{
			box.h -= thickness;
		}
		f32 thumbSize = box.h * view->viewport.h / view->contentsHeight;
		f32 sliderValue = 1. - view->scrollY / (view->contentsHeight - view->viewport.h);

		if(mp_gui_scroll_slider(gui, "vscroll", box, thumbSize, &sliderValue))
		{
			view->scrollY = (1 - sliderValue) * (view->contentsHeight - view->viewport.h);
		}
		else if(mp_gui_is_mouse_over(gui, (mp_aligned_rect){0, 0, view->viewport.w, view->viewport.h}))
		{
			view->scrollY += gui->input.mouse.wheelY;
		}
		view->scrollY = Clamp(view->scrollY, 0, view->contentsHeight - view->viewport.h);
	}
}

void mp_gui_end_view(mp_gui_context* gui)
{
	mp_gui_view* view = mp_gui_view_top(gui);

	if(view->open || view->wasClosedInScope)
	{
		mp_gui_style* style = mp_gui_style_top(gui);
		mp_graphics_context graphics = gui->graphics;

		mp_gui_transform_pop(gui);
		mp_graphics_matrix_pop(graphics);
		/*WARN(martin):
			The order of operations is important here.
			We set next scroll before processing the scrollbars, otherwise we would overwrite the potential scroll
			change of the scroll bars. We also need to keep contents dimension coherent with the scroll value,
			otherwise the slider value would be computed for the old contents dimension, and the slider code
			would clamp the new scroll value based on the old contents dimensions.
		*/
		view->scrollX = view->nextScrollX;
		view->scrollY = view->nextScrollY;
		view->contentsWidth = view->nextContentsWidth;
		view->contentsHeight = view->nextContentsHeight;

		mp_gui_view_scrollbars(gui, view);

		mp_gui_clip_pop(gui);
		mp_gui_transform_pop(gui);

		if(view->flags & MP_GUI_VIEW_FLAG_ROOT)
		{
			mp_graphics_stream_swap(graphics, view->parentStream);
		}
		else
		{
			mp_graphics_stream_swap(graphics, view->parentStream);

			//NOTE(martin): draw background
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_BG]);
			mp_graphics_rectangle_fill(graphics, view->frame.x, view->frame.y, view->frame.w, view->frame.h);

			if(view->flags & MP_GUI_VIEW_FLAG_TITLED)
			{
				//NOTE(martin): draw the title bar
				mp_aligned_rect titleBar = {view->frame.x, view->frame.y, view->frame.w, style->titleHeight};
				mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_TITLE_BG]);
				mp_graphics_rectangle_fill(graphics, titleBar.x, titleBar.y, titleBar.w, titleBar.h);

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//TODO Currently we can't draw the title because it's a raw string passed in begin_view()
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//	mp_gui_draw_label(gui, title, titleBar, style->colors[MP_GUI_STYLE_COLOR_VIEW_TITLE_TEXT]);
			}

			mp_graphics_clip_push(graphics, view->frame.x + view->viewport.x, view->frame.y + view->viewport.y, view->viewport.w, view->viewport.h);
			mp_graphics_matrix_push(gui->graphics,
			                        (mp_mat2x3){1, 0, view->frame.x + view->viewport.x,
			                                    0, 1, view->frame.y + view->viewport.y});

				mp_graphics_stream_append(graphics, view->stream);

			mp_graphics_matrix_pop(gui->graphics);
			mp_graphics_clip_pop(graphics);

			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_BORDER]);
			mp_graphics_set_width(graphics, style->viewBorderWidth);
			mp_graphics_rectangle_stroke(graphics, view->frame.x, view->frame.y, view->frame.w, view->frame.h);
		}
		mp_gui_view_end_used(view);
	}
	mp_gui_view_pop(gui);
	mp_gui_pop_id(gui);

	DEBUG_ASSERT(view->debugTransformStackDepth == gui->transformStackSize);
	DEBUG_ASSERT(view->debugClipStackDepth == gui->clipStackSize);
}


void mp_gui_begin_frame(mp_gui_context* gui)
{
	//TODO...

	gui->input.beginFrameTime = ClockGetTime(SYS_CLOCK_MONOTONIC);

	mp_graphics_matrix_push(gui->graphics, (mp_mat2x3){1, 0, 0,
	                                                   0, -1, gui->input.displayHeight});
	mp_graphics_set_text_flip(gui->graphics, true);

	gui->mouseCursor = MP_MOUSE_CURSOR_ARROW;

	gui->frameCounter++;
}

int mp_gui_view_z_compare(const void* lhs, const void* rhs)
{
	return((*(mp_gui_view**)lhs)->z - (*(mp_gui_view**)rhs)->z);
}

void mp_gui_input_next_frame(mp_gui_io* input)
{
	for(int i=0; i<MP_MOUSE_BUTTON_COUNT; i++)
	{
		input->mouse.buttons[i] &= ~(MP_KEY_STATE_PRESSED | MP_KEY_STATE_RELEASED | MP_KEY_STATE_SIMPLE_CLICKED | MP_KEY_STATE_DOUBLE_CLICKED);
	}
	for(int i=0; i<MP_KEY_COUNT; i++)
	{
		input->keys[i] &= ~(MP_KEY_STATE_PRESSED | MP_KEY_STATE_RELEASED);
	}
	input->text.count = 0;

	input->mouse.deltaX = 0;
	input->mouse.deltaY = 0;
	input->mouse.wheelX = 0;
	input->mouse.wheelY = 0;
}

void mp_gui_end_frame(mp_gui_context* gui)
{
	//NOTE(martin): sort view by z index, append graphics command streams in z order
	//              and renumber views with consecutive z indices.
	qsort(gui->rootViews, gui->rootViewCount, sizeof(mp_gui_view*), mp_gui_view_z_compare);

	u32 z = 0;
	for(int i=0; i<gui->rootViewCount; i++)
	{
		mp_gui_view* view = gui->rootViews[i];

		//NOTE(martin): draw background
		mp_graphics_context graphics = gui->graphics;

		mp_graphics_matrix_push(graphics, (mp_mat2x3){1, 0, view->refX,
			                                             0, 1, view->refY});

		//TODO: should be cached in the views
		mp_gui_style* style = mp_gui_style_top(gui);

		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_BG]);
		mp_graphics_rectangle_fill(graphics, view->frame.x, view->frame.y, view->frame.w, view->frame.h);

		if(view->flags & MP_GUI_VIEW_FLAG_TITLED)
		{
			//NOTE(martin): draw the title bar
			mp_aligned_rect titleBar = {view->frame.x, view->frame.y, view->frame.w, style->titleHeight};
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_TITLE_BG]);
			mp_graphics_rectangle_fill(graphics, titleBar.x, titleBar.y, titleBar.w, titleBar.h);

//			mp_gui_draw_label(gui, title, titleBar, style->colors[MP_GUI_STYLE_COLOR_VIEW_TITLE_TEXT]);
		}

		mp_graphics_clip_push(graphics, view->frame.x + view->viewport.x, view->frame.y + view->viewport.y, view->viewport.w, view->viewport.h);
		mp_graphics_matrix_push(gui->graphics, (mp_mat2x3){1, 0, view->frame.x + view->viewport.x, 0, 1, view->frame.y + view->viewport.y});

			mp_graphics_stream_append(graphics, view->stream);

		mp_graphics_matrix_pop(gui->graphics);
		mp_graphics_clip_pop(graphics);

		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_VIEW_BORDER]);
		mp_graphics_set_width(graphics, style->viewBorderWidth);
		mp_graphics_rectangle_stroke(graphics, view->frame.x, view->frame.y, view->frame.w, view->frame.h);

		mp_graphics_matrix_pop(graphics);

		view->z = z;
		z++;
	}
	gui->frontView = gui->rootViewCount ? gui->rootViews[gui->rootViewCount-1] : 0;
	gui->nextZ = z;
	gui->rootViewCount = 0;

	gui->hoveredRoot = gui->nextHoveredRoot;
	gui->nextHoveredRoot = 0;

	gui->hoveredView = gui->nextHoveredView;
	gui->nextHoveredView = 0;

	mp_graphics_matrix_pop(gui->graphics);

	mp_gui_input_next_frame(&gui->input);

	gui->prevHovered = gui->hovered;
	gui->prevActive = gui->active;
	gui->prevFocus = gui->focus;

	mp_set_cursor(gui->mouseCursor);

	DEBUG_ASSERT(gui->idStackSize == 0, "unbalanced id stack at end of frame");
	DEBUG_ASSERT(gui->viewStackSize == 0, "unbalanced view stack at end of frame");
	DEBUG_ASSERT(gui->clipStackSize == 0, "unbalanced clip stack at end of frame");
	DEBUG_ASSERT(gui->transformStackSize == 0, "unbalanced transform stack at end of frame");
}

bool mp_gui_text_button(mp_gui_context* gui, const char* label, mp_aligned_rect box)
{
	//NOTE(martin): button behaviour
	mp_gui_id id = mp_gui_id_from_string(gui, label);

	bool buttonClicked = mp_gui_button_behaviour(gui, id, box);

	//NOTE(martin): draw button
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_context graphics = gui->graphics;

	if(id == gui->active && id == gui->hovered)
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT_ACTIVE]);
	}
	else if(id == gui->hovered)
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT_HOVER]);
	}
	else
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT]);
	}
	mp_graphics_rectangle_fill(graphics, box.x, box.y, box.w, box.h);

	mp_gui_draw_label(gui, label, box, style->colors[MP_GUI_STYLE_COLOR_TEXT]);

	if(id == gui->active && id == gui->hovered)
	{
		mp_graphics_set_width(graphics, style->borderWidthActive);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER_ACTIVE]);
	}
	else
	{
		mp_graphics_set_width(graphics, style->borderWidth);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER]);
	}
	mp_graphics_rectangle_stroke(graphics, box.x, box.y, box.w, box.h);

	return(buttonClicked);
}

bool mp_gui_collapse_button(mp_gui_context* gui, const char* label, mp_aligned_rect box, bool* collapsed, f32* animation)
{
	mp_gui_id id = mp_gui_id_from_string(gui, label);
	bool buttonClicked = mp_gui_button_behaviour(gui, id, box);
	if(buttonClicked)
	{
		*collapsed = !(*collapsed);
	}

	//NOTE(martin): draw collapse button
	mp_graphics_context graphics = gui->graphics;
	mp_gui_style* style = mp_gui_style_top(gui);

	f32 centerX = box.x + 0.5*box.w;
	f32 centerY = box.y + 0.5*box.h;
	f32 radius = minimum(box.w, box.h) * 0.25;

	f32 rotCos = cos(*animation*M_PI/2.);
	f32 rotSin = -sin(*animation*M_PI/2.);

	f32 p0x = 0;
	f32 p0y = radius;
	f32 p1x = radius*cos(M_PI/16.);
	f32 p1y = -radius*sin(M_PI/16.);
	f32 p2x = -radius*cos(M_PI/16.);
	f32 p2y = -radius*sin(M_PI/16.);

	mp_graphics_move_to(graphics, centerX + p0x*rotCos - p0y*rotSin, centerY + p0x*rotSin + p0y*rotCos);
	mp_graphics_line_to(graphics, centerX + p1x*rotCos - p1y*rotSin, centerY + p1x*rotSin + p1y*rotCos);
	mp_graphics_line_to(graphics, centerX + p2x*rotCos - p2y*rotSin, centerY + p2x*rotSin + p2y*rotCos);

	/*
	if(*collapsed)
	{
		//NOTE(martin): draw collapse button in collapsed position
		mp_graphics_move_to(graphics, centerX + radius, centerY);
		mp_graphics_line_to(graphics, centerX - radius*sin(M_PI/16.), centerY + radius*cos(M_PI/16.));
		mp_graphics_line_to(graphics, centerX - radius*sin(M_PI/16.), centerY - radius*cos(M_PI/16.));
	}
	else
	{
		//NOTE(martin): draw collapse button in deployed position
		mp_graphics_move_to(graphics, centerX, centerY + radius);
		mp_graphics_line_to(graphics, centerX + radius*cos(M_PI/16.), centerY - radius*sin(M_PI/16.));
		mp_graphics_line_to(graphics, centerX - radius*cos(M_PI/16.), centerY - radius*sin(M_PI/16.));
	}
	*/
	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
	mp_graphics_fill(graphics);

	f32 animationUpdate = (*collapsed) ? 0.30 : -0.30;
	*animation = Clamp(*animation + animationUpdate, 0, 1);

	return(buttonClicked);
}

bool mp_gui_checkbox(mp_gui_context* gui, const char* label, mp_aligned_rect box, bool* checked)
{
	mp_gui_id id = mp_gui_id_from_string(gui, label);
	bool buttonClicked = mp_gui_button_behaviour(gui, id, box);
	if(buttonClicked)
	{
		*checked = !(*checked);
	}

	//NOTE(martin): draw checkbox
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_context graphics = gui->graphics;

	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BG]);
	mp_graphics_rectangle_fill(graphics, box.x, box.y, box.w, box.h);

	if(id == gui->active && id == gui->hovered)
	{
		mp_graphics_set_width(graphics, style->borderWidthActive);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER_ACTIVE]);
	}
	else
	{
		mp_graphics_set_width(graphics, style->borderWidth);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER]);
	}
	mp_graphics_rectangle_stroke(graphics, box.x, box.y, box.w, box.h);

	if(*checked)
	{
		f32 margin = style->margin;
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
		mp_graphics_set_width(graphics, 4);
		mp_graphics_move_to(graphics, box.x + margin, box.y + margin + 3*(box.h-2*margin)/4.0);
		mp_graphics_line_to(graphics, box.x + margin + (box.w-2*margin)/4, box.y + box.h - margin);
		mp_graphics_line_to(graphics, box.x + box.w - margin, box.y + margin);
		mp_graphics_stroke(graphics);
	}

	return(buttonClicked);

}

void mp_gui_edit_set_cursor_from_mouse(mp_gui_context* gui, mp_gui_style* style, f32 textStartX)
{
	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(gui->graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(gui->graphics, style->font, style->fontSize);

	//NOTE(martin): find cursor position from mouse position
	gui->editCursor = gui->editBufferSize;

	mp_graphics_text_extents glyphExtents[MP_GUI_EDIT_BUFFER_MAX_SIZE];
	mp_string32 codePoints = {gui->editBufferSize, gui->editBuffer};
	mp_string32 glyphs = mp_graphics_font_push_glyph_indices(gui->graphics, style->font, mem_scratch(), codePoints);
	mp_graphics_font_get_glyph_extents(gui->graphics, style->font, glyphs, glyphExtents);

	mp_graphics_set_font(gui->graphics, style->font);
	vec2 dimToFirst = mp_graphics_get_glyphs_dimensions(gui->graphics, mp_string32_slice(glyphs, 0, gui->firstDisplayedChar));

	mp_gui_transform tr = mp_gui_transform_top(gui);
	f32 x = gui->input.mouse.x + tr.x - textStartX;
	f32 glyphX = -dimToFirst.x;

	for(int i=0; i<gui->editBufferSize; i++)
	{
		if(x < glyphX + glyphExtents[i].xAdvance*fontScale/2)
		{
			gui->editCursor = i;
			return;
		}
		glyphX += glyphExtents[i].xAdvance*fontScale;
	}
	return;
}

void mp_gui_edit_delete_selection(mp_gui_context* gui)
{
	DEBUG_ASSERT(gui->focus);

	u32 selLeft = minimum(gui->editCursor, gui->editMark);
	u32 selRight = maximum(gui->editCursor, gui->editMark);

	DEBUG_ASSERT(selLeft <= gui->editBufferSize);
	DEBUG_ASSERT(selRight <= gui->editBufferSize);

	memmove(gui->editBuffer + selLeft,
		gui->editBuffer + selRight,
		(gui->editBufferSize - selRight)*sizeof(u32));

	gui->editCursor = selLeft;
	gui->editMark = gui->editCursor;
	gui->editBufferSize -= (selRight - selLeft);
}

void mp_gui_edit_replace_selection_with_codepoints(mp_gui_context* gui, u32 count, u32* codepoints, mp_gui_text_field_flags filters)
{
	DEBUG_ASSERT(gui->focus);

	if(count && gui->editCursor != gui->editMark)
	{
		mp_gui_edit_delete_selection(gui);
	}

	for(int i=0; i<count; i++)
	{
		utf32 c = codepoints[i];

		if((filters & MP_GUI_TEXT_FILTER_MASK)
		  && !((filters & MP_GUI_TEXT_FILTER_DIGIT) && c >= '0' && c <= '9')
		  && !((filters & MP_GUI_TEXT_FILTER_ALPHA) && isalpha(c))
		  && !((filters & MP_GUI_TEXT_FILTER_DOT) && c == '.'))
		{
			continue;
		}

		if(  c < 0x20
		  || c == 127
		  || (c >= 0xe000 && c <= 0xf8ff))
		{
			continue;
		}

		if(gui->editBufferSize < MP_GUI_EDIT_BUFFER_MAX_SIZE)
		{
			memmove(gui->editBuffer + gui->editCursor + 1,
				gui->editBuffer + gui->editCursor,
				(gui->editBufferSize - gui->editCursor)*sizeof(u32));

			gui->editBuffer[gui->editCursor] = c;
			gui->editCursor++;
			gui->editBufferSize++;
		}
		else
		{
			LOG_WARNING("text field full, cannot insert characters\n");
		}
		gui->editMark = gui->editCursor;
	}
}

void mp_gui_edit_copy_selection_to_clipboard(mp_gui_context* gui)
{
	DEBUG_ASSERT(gui->focus);
	if(gui->editCursor == gui->editMark)
	{
		return;
	}

	u32 selLeft = minimum(gui->editCursor, gui->editMark);
	u32 selRight = maximum(gui->editCursor, gui->editMark);
	DEBUG_ASSERT(selLeft <= gui->editBufferSize);
	DEBUG_ASSERT(selRight <= gui->editBufferSize);

	mp_string32 codePoints = {selRight-selLeft, gui->editBuffer+selLeft};
	mp_string string = utf8_push_from_codepoints(mem_scratch(), codePoints);

	mp_clipboard_clear();
	mp_clipboard_set_string(string);
}

void mp_gui_edit_replace_selection_with_clipboard(mp_gui_context* gui, mp_gui_text_field_flags filters)
{
	mp_string string = mp_clipboard_get_string(mem_scratch());
	mp_string32 codePoints = utf8_push_to_codepoints(mem_scratch(), string);

	mp_gui_edit_replace_selection_with_codepoints(gui, codePoints.len, codePoints.ptr, filters);
}

typedef enum { MP_GUI_EDIT_MOVE,
               MP_GUI_EDIT_SELECT,
	       MP_GUI_EDIT_SELECT_EXTEND,
	       MP_GUI_EDIT_DELETE,
	       MP_GUI_EDIT_CUT,
	       MP_GUI_EDIT_COPY,
	       MP_GUI_EDIT_PASTE,
	       MP_GUI_EDIT_SELECT_ALL } mp_gui_edit_op;

typedef enum { MP_GUI_EDIT_MOVE_NONE = 0,
	       MP_GUI_EDIT_MOVE_ONE,
	       MP_GUI_EDIT_MOVE_WORD,
	       MP_GUI_EDIT_MOVE_LINE } mp_gui_edit_move;

typedef struct mp_gui_edit_command
{
	mp_key_code key;
	mp_key_mods mods;

	mp_gui_edit_op operation;
	mp_gui_edit_move move;
	int direction;

} mp_gui_edit_command;

const u32 MP_GUI_EDIT_COMMAND_COUNT = 18;
static const mp_gui_edit_command MP_GUI_EDIT_COMMANDS[MP_GUI_EDIT_COMMAND_COUNT] = {
	//NOTE(martin): move one left
	{
		.key = MP_KEY_LEFT,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = -1
	},
	//NOTE(martin): move one right
	{
		.key = MP_KEY_RIGHT,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = 1
	},
	//NOTE(martin): move start
	{
		.key = MP_KEY_Q,
		.mods = MP_KEYMOD_CTRL,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = -1
	},
	{
		.key = MP_KEY_UP,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = -1
	},
	//NOTE(martin): move end
	{
		.key = MP_KEY_E,
		.mods = MP_KEYMOD_CTRL,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = 1
	},
	{
		.key = MP_KEY_DOWN,
		.operation = MP_GUI_EDIT_MOVE,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = 1
	},
	//NOTE(martin): select one left
	{
		.key = MP_KEY_LEFT,
		.mods = MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = -1
	},
	//NOTE(martin): select one right
	{
		.key = MP_KEY_RIGHT,
		.mods = MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = 1
	},
	//NOTE(martin): extend select to start
	{
		.key = MP_KEY_Q,
		.mods = MP_KEYMOD_CTRL | MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT_EXTEND,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = -1
	},
	{
		.key = MP_KEY_UP,
		.mods = MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT_EXTEND,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = -1
	},
	//NOTE(martin): extend select to end
	{
		.key = MP_KEY_E,
		.mods = MP_KEYMOD_CTRL | MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT_EXTEND,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = 1
	},
	{
		.key = MP_KEY_DOWN,
		.mods = MP_KEYMOD_SHIFT,
		.operation = MP_GUI_EDIT_SELECT_EXTEND,
		.move = MP_GUI_EDIT_MOVE_LINE,
		.direction = 1
	},
	//NOTE(martin): select all
	{
		.key = MP_KEY_Q,
		.mods = MP_KEYMOD_CMD,
		.operation = MP_GUI_EDIT_SELECT_ALL,
		.move = MP_GUI_EDIT_MOVE_NONE
	},
	//NOTE(martin): delete
	{
		.key = MP_KEY_DELETE,
		.operation = MP_GUI_EDIT_DELETE,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = 1
	},
	//NOTE(martin): backspace
	{
		.key = MP_KEY_BACKSPACE,
		.operation = MP_GUI_EDIT_DELETE,
		.move = MP_GUI_EDIT_MOVE_ONE,
		.direction = -1
	},
	//NOTE(martin): cut
	{
		.key = MP_KEY_X,
		.mods = MP_KEYMOD_CMD,
		.operation = MP_GUI_EDIT_CUT,
		.move = MP_GUI_EDIT_MOVE_NONE
	},
	//NOTE(martin): copy
	{
		.key = MP_KEY_C,
		.mods = MP_KEYMOD_CMD,
		.operation = MP_GUI_EDIT_COPY,
		.move = MP_GUI_EDIT_MOVE_NONE
	},
	//NOTE(martin): paste
	{
		.key = MP_KEY_V,
		.mods = MP_KEYMOD_CMD,
		.operation = MP_GUI_EDIT_PASTE,
		.move = MP_GUI_EDIT_MOVE_NONE
	}
};

void mp_gui_edit_perform_move(mp_gui_context* gui, mp_gui_edit_move move, int direction, u32* cursor)
{
	switch(move)
	{
		case MP_GUI_EDIT_MOVE_NONE:
			break;

		case MP_GUI_EDIT_MOVE_ONE:
		{
			if(direction < 0 && (*cursor) > 0)
			{
				(*cursor)--;
			}
			else if(direction > 0 && (*cursor) < gui->editBufferSize)
			{
				(*cursor)++;
			}
		} break;

		case MP_GUI_EDIT_MOVE_LINE:
		{
			if(direction < 0)
			{
				(*cursor) = 0;
			}
			else if(direction > 0)
			{
				(*cursor) = gui->editBufferSize;
			}
		} break;

		case MP_GUI_EDIT_MOVE_WORD:
			DEBUG_ASSERT(0, "not implemented yet");
			break;
	}
}

void mp_gui_edit_perform_operation(mp_gui_context* gui, mp_gui_edit_op operation, mp_gui_edit_move move, int direction, mp_gui_text_field_flags filters)
{
	DEBUG_ASSERT(gui->focus);
	DEBUG_ASSERT(gui->editBufferSize <= MP_GUI_EDIT_BUFFER_MAX_SIZE);
	DEBUG_ASSERT(gui->editCursor <= gui->editBufferSize);
	DEBUG_ASSERT(gui->editMark <= gui->editBufferSize);

	switch(operation)
	{
		case MP_GUI_EDIT_MOVE:
		{
			//NOTE(martin): we place the cursor on the direction-most side of the selection
			//              before performing the move
			u32 cursor = direction < 0 ?
				     minimum(gui->editCursor, gui->editMark) :
				     maximum(gui->editCursor, gui->editMark);

			if(gui->editCursor == gui->editMark || move != MP_GUI_EDIT_MOVE_ONE)
			{
				//NOTE: we special case move-one when there is a selection
				//      (just place the cursor at begining/end of selection)
				mp_gui_edit_perform_move(gui, move, direction, &cursor);
			}
			gui->editMark = gui->editCursor = cursor;
		} break;

		case MP_GUI_EDIT_SELECT:
		{
			mp_gui_edit_perform_move(gui, move, direction, &gui->editCursor);
		} break;

		case MP_GUI_EDIT_SELECT_EXTEND:
		{
			u32* cursor = direction > 0 ?
			              (gui->editCursor > gui->editMark ? &gui->editCursor : &gui->editMark) :
				      (gui->editCursor < gui->editMark ? &gui->editCursor : &gui->editMark);

			mp_gui_edit_perform_move(gui, move, direction, cursor);
		} break;

		case MP_GUI_EDIT_DELETE:
		{
			if(gui->editCursor == gui->editMark)
			{
				mp_gui_edit_perform_move(gui, move, direction, &gui->editCursor);
			}
			mp_gui_edit_delete_selection(gui);
			gui->editMark = gui->editCursor;
		} break;

		case MP_GUI_EDIT_CUT:
		{
			mp_gui_edit_copy_selection_to_clipboard(gui);
			mp_gui_edit_delete_selection(gui);
		} break;

		case MP_GUI_EDIT_COPY:
		{
			mp_gui_edit_copy_selection_to_clipboard(gui);
		} break;

		case MP_GUI_EDIT_PASTE:
		{
			mp_gui_edit_replace_selection_with_clipboard(gui, filters);
		} break;

		case MP_GUI_EDIT_SELECT_ALL:
		{
			gui->editCursor = 0;
			gui->editMark = gui->editBufferSize;
		} break;
	}
	gui->editCursorBlinkStart  = gui->input.beginFrameTime;
}

bool mp_gui_text_field(mp_gui_context* gui,
                       const char* label,
		       mp_aligned_rect box,
		       u32 maxSize,
		       u32 *textSize,
		       u32* text,
		       mp_gui_text_field_flags flags)
{
	ASSERT(*textSize <= MP_GUI_EDIT_BUFFER_MAX_SIZE);

	mp_gui_io* input = &gui->input;
	mp_gui_style* style = mp_gui_style_top(gui);

	//TODO: could cache this at the beginning of frame
	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(gui->graphics, style->font, &fontExtents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(gui->graphics, style->font, style->fontSize);


	mp_gui_id id = mp_gui_id_from_string(gui, label);
	bool textChanged = false;


	mp_gui_widget_flags widgetFlags = MP_GUI_WIDGET_FLAG_CAN_FOCUS;
	if(flags & MP_GUI_TEXT_FIELD_FOCUS_DOUBLE_CLICK)
	{
		widgetFlags |= MP_GUI_WIDGET_FLAG_FOCUS_DOUBLE_CLICK;
	}
	mp_gui_widget_signals signals = mp_gui_widget_behaviour(gui, id, box, widgetFlags);

	if(((signals & MP_GUI_WIDGET_IS_HOVERED) && (~signals & MP_GUI_TEXT_FIELD_NO_HOVER_CURSOR))
	  && (signals & MP_GUI_WIDGET_HAS_FOCUS))
	{
		mp_gui_set_mouse_cursor(gui, MP_MOUSE_CURSOR_TEXT);
	}

	if(signals & MP_GUI_WIDGET_GOT_FOCUS)
	{
		memcpy(gui->editBuffer, text, (*textSize)*sizeof(u32));
		gui->editBufferSize = *textSize;
		gui->firstDisplayedChar = 0;
		gui->editCursor = 0;
		gui->editMark = (flags & MP_GUI_TEXT_FIELD_AUTOSELECT_ALL)? gui->editBufferSize : 0;
		gui->editCursorBlinkStart  = gui->input.beginFrameTime;
	}
	if(signals & MP_GUI_WIDGET_LOST_FOCUS)
	{
		//TODO(martin): extract in gui_edit_complete() (and use below)
		memcpy(text, gui->editBuffer, gui->editBufferSize*sizeof(u32));
		*textSize = gui->editBufferSize;
		gui->active = 0;
		textChanged = true;
	}

	if(signals & MP_GUI_WIDGET_ACTIVATE)
	{
		mp_gui_edit_set_cursor_from_mouse(gui, style, box.x + style->margin);
		if(!(input->mods & MP_KEYMOD_SHIFT))
		{
			gui->editMark = gui->editCursor;
		}
	}

	if(signals & MP_GUI_WIDGET_IS_ACTIVE)
	{
		mp_gui_edit_set_cursor_from_mouse(gui, style, box.x + style->margin);
	}

	if(signals & MP_GUI_WIDGET_HAS_FOCUS)
	{
		//NOTE(martin): replace selection with input characters
		mp_gui_edit_replace_selection_with_codepoints(gui, input->text.count, input->text.codePoints, flags & MP_GUI_TEXT_FILTER_MASK);

		//NOTE(martin): handle shortcuts
		for(int i=0; i<MP_GUI_EDIT_COMMAND_COUNT; i++)
		{
			const mp_gui_edit_command* command = &(MP_GUI_EDIT_COMMANDS[i]);
			if( (input->keys[command->key] & MP_KEY_STATE_PRESSED)
			  &&(input->mods == command->mods))
			{
				mp_gui_edit_perform_operation(gui, command->operation, command->move, command->direction, flags & MP_GUI_TEXT_FILTER_MASK);
				break;
			}
		}

		//NOTE(martin): text box focus shortcuts
		if((input->keys[MP_KEY_ENTER] & MP_KEY_STATE_PRESSED))
		{
			//TODO(martin): extract in gui_edit_complete() (and use below)
			memcpy(text, gui->editBuffer, gui->editBufferSize*sizeof(u32));
			*textSize = gui->editBufferSize;
			gui->focus = 0;
			textChanged = true;
		}
		else if((input->keys[MP_KEY_ESCAPE] & MP_KEY_STATE_PRESSED))
		{
			gui->focus = 0;
		}

		//NOTE(martin): if cursor is outside of box, slide the text inside the box
		if(gui->editCursor < gui->firstDisplayedChar)
		{
			gui->firstDisplayedChar = gui->editCursor;
		}
		else
		{
			//NOTE(martin): compute width between first displayed character and edit cursor
			mp_string32 codePoints = {gui->editBufferSize, gui->editBuffer};
			mp_string32 glyphs = mp_graphics_font_push_glyph_indices(gui->graphics, style->font, mem_scratch(), codePoints);
			vec2 leftDimensions = mp_graphics_get_glyphs_dimensions(gui->graphics,
																	mp_string32_slice(glyphs, gui->firstDisplayedChar, gui->editCursor));
			f32 displayedWidth = leftDimensions.x;
			if(displayedWidth + 2*style->margin >= box.w)
			{
				//NOTE(martin): if the computed width is larger than the textfield's box, increment first displayed char
				//              until that width is less than textfield's box

				mp_graphics_text_extents glyphExtents[MP_GUI_EDIT_BUFFER_MAX_SIZE];
				mp_graphics_font_get_glyph_extents(gui->graphics, style->font, glyphs, glyphExtents);

				for(int i=gui->firstDisplayedChar; i<gui->editCursor && (displayedWidth + 2*style->margin) >= box.w; i++)
				{
					displayedWidth -= glyphExtents[i].xAdvance * fontScale;
					gui->firstDisplayedChar++;
				}
			}
		}
	}

	//NOTE: compute text layout
	mp_graphics_context graphics = gui->graphics;

	f32 textX = box.x + style->margin;
	f32 baseLine = box.y + 0.5*(box.h + (fontExtents.capHeight + fontExtents.descent)*fontScale) - fontExtents.descent*fontScale;

	//NOTE(martin): draw text field background
	mp_graphics_set_font(graphics, style->font);
	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BG]);
	mp_graphics_rectangle_fill(graphics, box.x, box.y, box.w, box.h);

	//NOTE(martin): select text to draw and convert it to glyph indices
	u32* textToDraw = (signals & MP_GUI_WIDGET_HAS_FOCUS) ? gui->editBuffer : text;
	u32 glyphCount = (signals & MP_GUI_WIDGET_HAS_FOCUS) ? gui->editBufferSize : *textSize;

	mp_string32 codePoints = {glyphCount, textToDraw};
	mp_string32 glyphs = mp_graphics_font_push_glyph_indices(graphics, style->font, mem_scratch(), codePoints);

	mp_graphics_clip_push(graphics, box.x, box.y, box.w, box.h);

	//NOTE(martin): show text and selection/cursor
	if((signals & MP_GUI_WIDGET_HAS_FOCUS)
	  && gui->editMark != gui->editCursor)
	{
		u32 selLeft = minimum(gui->editMark, gui->editCursor);
		u32 selRight = maximum(gui->editMark, gui->editCursor);

		vec2 vecToFirst = mp_graphics_get_glyphs_dimensions(gui->graphics, mp_string32_slice(glyphs, 0, gui->firstDisplayedChar));
		vec2 vecToLeft = mp_graphics_get_glyphs_dimensions(gui->graphics, mp_string32_slice(glyphs, 0, selLeft));
		vec2 selectDimensions = mp_graphics_get_glyphs_dimensions(gui->graphics, mp_string32_slice(glyphs, selLeft, selRight));

		mp_aligned_rect selectBox = { textX + vecToLeft.x - vecToFirst.x,
		                              baseLine - fontExtents.ascent*fontScale,
		                              selectDimensions.x,
		                              selectDimensions.y };

		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT_SELECTION_BG]);
		mp_graphics_rectangle_fill(graphics, selectBox.x, selectBox.y, selectBox.w, selectBox.h);

		//NOTE(martin): draw text to start of selection
		mp_graphics_move_to(graphics, textX, baseLine);
		if(selLeft > gui->firstDisplayedChar)
		{
			mp_graphics_glyph_outlines(graphics, mp_string32_slice(glyphs, gui->firstDisplayedChar, selLeft));
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
			mp_graphics_fill(graphics);
			mp_graphics_move_to(graphics, selectBox.x, baseLine);
		}
		//NOTE(martin): draw selected text
		if(selRight > gui->firstDisplayedChar)
		{
			u32 firstSelChar = maximum(gui->firstDisplayedChar, selLeft);
			mp_graphics_glyph_outlines(graphics, mp_string32_slice(glyphs, firstSelChar, selRight));
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT_SELECTION_FG]);
			mp_graphics_fill(graphics);
			mp_graphics_move_to(graphics, selectBox.x + selectBox.w, baseLine);
		}
		//NOTE(martin): draw text after selection
		u32 firstTrailChar = maximum(gui->firstDisplayedChar, selRight);
		if(firstTrailChar < glyphCount)
		{
			mp_graphics_glyph_outlines(graphics, mp_string32_slice(glyphs, firstTrailChar, glyphCount));
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
			mp_graphics_fill(graphics);
		}
	}
	else
	{
		if((signals & MP_GUI_WIDGET_HAS_FOCUS)
		  && !((((u64)(input->beginFrameTime - gui->editCursorBlinkStart))<<1) & 1))
		{
			//NOTE(martin): draw edit cursor
			DEBUG_ASSERT(gui->editCursor - gui->firstDisplayedChar >= 0);

			vec2 leftDim = mp_graphics_get_glyphs_dimensions(gui->graphics,
			                                                 mp_string32_slice(glyphs, gui->firstDisplayedChar, gui->editCursor));
			f32 cursorX = textX + leftDim.x;
			f32 cursorY0 = baseLine + fontExtents.descent*fontScale;
			f32 cursorY1 = baseLine - fontExtents.ascent*fontScale;

			mp_graphics_move_to(graphics, cursorX, cursorY0);
			mp_graphics_line_to(graphics, cursorX, cursorY1);
			mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
			mp_graphics_set_width(graphics, 2);
			mp_graphics_stroke(graphics);
		}

		//NOTE(martin): draw whole text;
		mp_graphics_move_to(graphics, textX, baseLine);
		mp_graphics_glyph_outlines(graphics, mp_string32_slice(glyphs, gui->firstDisplayedChar, glyphCount));
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);
		mp_graphics_fill(graphics);
	}

	mp_graphics_clip_pop(graphics);

	//NOTE(martin): draw text field border
	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER]);
	mp_graphics_set_width(graphics, style->borderWidth);
	mp_graphics_rectangle_stroke(graphics, box.x, box.y, box.w, box.h);

	return(textChanged);
}

bool mp_gui_is_key_pressed(mp_gui_context* gui, mp_key_code key)
{
	return(gui->input.keys[key] & MP_KEY_STATE_PRESSED);
}

mp_key_mods mp_gui_key_mods(mp_gui_context* gui)
{
	return(gui->input.mods);
}

bool mp_gui_is_mouse_button_pressed(mp_gui_context* gui, mp_mouse_button button)
{
	return(gui->input.mouse.buttons[button] & MP_KEY_STATE_PRESSED);
}

bool mp_gui_is_mouse_button_down(mp_gui_context* gui, mp_mouse_button button)
{
	return(gui->input.mouse.buttons[button] & MP_KEY_STATE_DOWN);
}


bool mp_gui_is_mouse_button_simple_clicked(mp_gui_context* gui, mp_mouse_button button)
{
	return(gui->input.mouse.buttons[button] & MP_KEY_STATE_SIMPLE_CLICKED);
}

bool mp_gui_is_mouse_button_double_clicked(mp_gui_context* gui, mp_mouse_button button)
{
	return(gui->input.mouse.buttons[button] & MP_KEY_STATE_DOUBLE_CLICKED);
}

bool mp_gui_is_mouse_button_released(mp_gui_context* gui, mp_mouse_button button)
{
	return(gui->input.mouse.buttons[button] & MP_KEY_STATE_RELEASED);
}

f32 mp_gui_mouse_x(mp_gui_context* gui)
{
	mp_gui_transform tr = mp_gui_transform_top(gui);
	return(gui->input.mouse.x + tr.x);
}
f32 mp_gui_mouse_y(mp_gui_context* gui)
{
	mp_gui_transform tr = mp_gui_transform_top(gui);
	return(gui->input.mouse.y + tr.y);
}

f32 mp_gui_mouse_delta_x(mp_gui_context* gui)
{
	return(gui->input.mouse.deltaX);
}
f32 mp_gui_mouse_delta_y(mp_gui_context* gui)
{
	return(gui->input.mouse.deltaY);
}

f32 mp_gui_mouse_wheel_x(mp_gui_context* gui)
{
	return(gui->input.mouse.wheelX);
}
f32 mp_gui_mouse_wheel_y(mp_gui_context* gui)
{
	return(gui->input.mouse.wheelY);
}


//WARN(martin): temporary helpers added for QED convenience.
void mp_gui_cancel_text_input(mp_gui_context* gui)
{
	gui->input.text.count = 0;
}

f64 mp_gui_time(mp_gui_context* gui)
{
	return(gui->input.beginFrameTime);
}

void mp_gui_set_mouse_cursor(mp_gui_context* gui, mp_mouse_cursor cursor)
{
	gui->mouseCursor = cursor;
}

mp_mouse_cursor mp_gui_get_mouse_cursor(mp_gui_context* gui)
{
	return(gui->mouseCursor);
}

bool mp_gui_float_text_field(mp_gui_context* gui, const char* label, mp_aligned_rect box, float* number, mp_gui_text_field_flags flags)
{
	const u32 maxCodePoints = 64;
	char buff[maxCodePoints];
	utf32 codePoints[maxCodePoints];
	int ret = snprintf(buff, maxCodePoints, "%.3f", (double)(*number));
	if(ret < 0)
	{
		DEBUG_ASSERT(0);
		return(false);
	}
	u32 size = minimum(63, ret);
	for(int i=0; i<size; i++)
	{
		codePoints[i] = buff[i];
	}

	flags |= MP_GUI_TEXT_FILTER_DIGIT | MP_GUI_TEXT_FILTER_DOT;
	if(mp_gui_text_field(gui, label, box, maxCodePoints, &size, codePoints, flags))
	{
		for(int i=0; i<size && i<maxCodePoints; i++)
		{
			buff[i] = (char)codePoints[i];
		}
		buff[minimum(size, maxCodePoints-1)] = '\0';
		char* endptr = 0;
		float value = strtof(buff, &endptr);
		if(endptr == buff+size)
		{
			*number = value;
			return(true);
		}
	}
	return(false);
}

bool mp_gui_int_text_field(mp_gui_context* gui, const char* label, mp_aligned_rect box, int* number, mp_gui_text_field_flags flags)
{
	const u32 maxCodePoints = 64;
	char buff[maxCodePoints];
	utf32 codePoints[maxCodePoints];
	int ret = snprintf(buff, maxCodePoints, "%i", (*number));
	if(ret < 0)
	{
		DEBUG_ASSERT(0);
		return(false);
	}
	u32 size = minimum(63, ret);
	for(int i=0; i<size; i++)
	{
		codePoints[i] = buff[i];
	}

	flags |= MP_GUI_TEXT_FILTER_DIGIT;
	if(mp_gui_text_field(gui, label, box, maxCodePoints, &size, codePoints, flags))
	{
		for(int i=0; i<size && i<maxCodePoints; i++)
		{
			buff[i] = (char)codePoints[i];
		}
		buff[minimum(size, maxCodePoints-1)] = '\0';
		char* endptr = 0;
		int value = strtol(buff, &endptr, 10);
		if(endptr == buff+size)
		{
			*number = value;
			return(true);
		}
	}
	return(false);
}

void mp_gui_open_popup(mp_gui_context* gui, const char* label)
{
	//TODO(martin): coalesce with codepath in begin view ?
	mp_gui_id id = mp_gui_id_from_string(gui, label);
	mp_gui_view* view = mp_gui_view_find(gui, id);
	if(!view)
	{
		//NOTE(martin): if the view was not cached, create it
		view = mp_gui_view_alloc(gui, id);

		//TODO  init the view in alloc
		view->flags = MP_GUI_VIEW_FLAG_ROOT | MP_GUI_VIEW_FLAG_VSCROLL | MP_GUI_VIEW_FLAG_START_CLOSED;
		mp_gui_view_order_front(gui, view);
	}
	view->lastFrameDidOpen = gui->frameCounter;
	view->open = true;
	mp_gui_view_order_front(gui, view);
}

bool mp_gui_begin_popup(mp_gui_context* gui, const char* label, mp_aligned_rect box)
{
	mp_gui_transform tr = mp_gui_transform_top(gui);

	mp_gui_view_flags flags = MP_GUI_VIEW_FLAG_ROOT | MP_GUI_VIEW_FLAG_VSCROLL | MP_GUI_VIEW_FLAG_START_CLOSED;
	if(mp_gui_begin_view(gui, label, box, flags))
	{
		mp_gui_view* view = mp_gui_view_top(gui);
		mp_gui_view_order_front(gui, view);

		view->frame = box;
		view->nextFrame = box;
		view->viewport = (mp_aligned_rect){0, 0, box.w, box.h};

		if(  mp_gui_is_mouse_button_pressed(gui, MP_MOUSE_LEFT)
		  && !mp_gui_is_mouse_over(gui, (mp_aligned_rect){0, 0, box.w, box.h})
		  && view->lastFrameDidOpen != gui->frameCounter)
		{
			mp_gui_view_close(gui);
			return(false);
		}
		return(true);
	}
	else
	{
		return(false);
	}
}

void mp_gui_end_popup(mp_gui_context* gui)
{
	mp_gui_end_view(gui);
}

bool mp_gui_popup_menu(mp_gui_context* gui, const char* label, mp_aligned_rect buttonBox, u32 optionCount, mp_string* options, int* optionIndex)
{
	mp_gui_push_id_string(gui, label);

	mp_graphics_context graphics = gui->graphics;

	//NOTE(martin): button behaviour
	mp_gui_id id = mp_gui_id_from_string(gui, "button");
	bool buttonClicked = mp_gui_button_behaviour(gui, id, buttonBox);

	//NOTE(martin): draw dropmenu button frame
	mp_gui_style* style = mp_gui_style_top(gui);

	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BG]);
	mp_graphics_rectangle_fill(graphics, buttonBox.x, buttonBox.y, buttonBox.w, buttonBox.h);

	mp_aligned_rect buttonTextBox = {buttonBox.x, buttonBox.y, buttonBox.w - buttonBox.h, buttonBox.h};
	mp_aligned_rect buttonArrowBox = {buttonBox.x + buttonBox.w - buttonBox.h, buttonBox.y, buttonBox.h, buttonBox.h};

	//NOTE(martin): draw dropmenu text
	mp_gui_style optionStyle = *style;
	optionStyle.textAlign = MP_GUI_STYLE_TEXT_ALIGN_LEFT;
	mp_gui_draw_string(gui, options[*optionIndex], buttonTextBox, &optionStyle);

	//NOTE(martin): draw dropmenu arrow
	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT]);
	mp_graphics_rectangle_fill(graphics, buttonArrowBox.x, buttonArrowBox.y, buttonArrowBox.w, buttonArrowBox.h);

	f32 arrowCenterX = buttonArrowBox.x + buttonArrowBox.w/2;
	f32 arrowCenterY = buttonArrowBox.y + buttonArrowBox.h/2;
	f32 arrowRadius = buttonArrowBox.w/2 * 0.6;

	mp_graphics_move_to(graphics, arrowCenterX + arrowRadius*0.866, arrowCenterY - arrowRadius*0.5);
	mp_graphics_line_to(graphics, arrowCenterX - arrowRadius*0.866, arrowCenterY - arrowRadius*0.5);
	mp_graphics_line_to(graphics, arrowCenterX, arrowCenterY + arrowRadius);
	mp_graphics_set_color_rgba(graphics, 0.1, 0.1, 0.1, 1);
	mp_graphics_fill(graphics);

	//NOTE(martin): draw dropmenu button border
	if(id == gui->active && id == gui->hovered)
	{
		mp_graphics_set_width(graphics, style->borderWidthActive);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER_ACTIVE]);
	}
	else
	{
		mp_graphics_set_width(graphics, style->borderWidth);
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER]);
	}
	mp_graphics_rectangle_stroke(graphics, buttonBox.x, buttonBox.y, buttonBox.w, buttonBox.h);


	if(buttonClicked)
	{
		mp_gui_open_popup(gui, "panel");
	}

	mp_graphics_font_extents extents;
	mp_graphics_font_get_extents(graphics, style->font, &extents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, style->font, style->fontSize);
	f32 lineHeight = (extents.ascent + extents.descent + extents.leading)*fontScale;

	//TODO: measure max width correctly
	f32 optionHeight = lineHeight + style->margin;

	mp_aligned_rect panelRect = {buttonBox.x, buttonBox.y + buttonBox.h, 600, optionCount*optionHeight + style->margin};
	mp_gui_view* view = mp_gui_view_top(gui);

	f32 globalY = panelRect.y - view->scrollY + view->viewport.y + view->frame.y + view->refY;
	f32 globalX = panelRect.x - view->scrollX + view->viewport.x + view->frame.x + view->refX;

	if(globalY + panelRect.h > gui->input.displayHeight)
	{
		panelRect.y = buttonBox.y - panelRect.h;
	}
	if(globalX + panelRect.w > gui->input.displayWidth)
	{
		panelRect.x = buttonBox.x + buttonBox.w - panelRect.w;
	}



	//TODO: compute panel width / height

	mp_graphics_set_font(graphics, style->font);
	mp_graphics_set_font_size(graphics, style->fontSize);
	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT]);

	bool clicked = false;
	if(mp_gui_begin_popup(gui, "panel", panelRect))
	{
		mp_gui_view_set_frame(gui, panelRect);

		f32 y = style->margin;

		for(int i=0; i<optionCount; i++)
		{
			mp_aligned_rect optionBox = {0, y, panelRect.w, optionHeight};

			//TODO: draw background if selected, change style as appropriate
			//      detect click and select option
			if(mp_gui_is_mouse_over(gui, optionBox))
			{
				mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_TEXT_SELECTION_BG]);
				mp_graphics_rectangle_fill(graphics, optionBox.x, optionBox.y, optionBox.w, optionBox.h);
				if(mp_gui_is_mouse_button_pressed(gui, MP_MOUSE_LEFT))
				{
					*optionIndex = i;
					clicked = true;
					mp_gui_view_close(gui);
				}
			}
			mp_gui_draw_string(gui, options[i], optionBox, &optionStyle);
			y += optionHeight;
		}

	} mp_gui_end_popup(gui);

	mp_gui_pop_id(gui);

	return(clicked);
}

bool mp_gui_slider(mp_gui_context* gui, const char* label, mp_aligned_rect box, f32 thumbSize, f32* value)
{
	mp_gui_style* style = mp_gui_style_top(gui);
	mp_graphics_context graphics = gui->graphics;

	mp_gui_id id = mp_gui_push_id_string(gui, label);

	f32 nextValue = Clamp(*value, 0, 1);
	bool vertical = box.h > box.w;
	thumbSize = vertical ? Clamp(thumbSize, 1, box.h) : Clamp(thumbSize, 1, box.w);
	f32 runway = (vertical ? box.h : box.w) - thumbSize;

	mp_aligned_rect thumb = { vertical ? box.x : box.x + (box.w - thumbSize) * nextValue,
	                          vertical ? box.y + (box.h - thumbSize) * (1 - nextValue) : box.y,
				  vertical ? box.w : thumbSize,
				  vertical ? thumbSize : box.h};

	mp_gui_button_behaviour(gui, id, thumb);

	f32 delta = 0;
	if(id == gui->active)
	{
		delta = vertical ? -gui->input.mouse.deltaY : gui->input.mouse.deltaX;
		delta /= runway;
	}
	if(id == gui->hovered)
	{
		//NOTE(martin): wheel
		delta += vertical ? -gui->input.mouse.wheelY : gui->input.mouse.wheelX;
	}

	nextValue = Clamp(nextValue + delta, 0, 1);

	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BG]);
	mp_graphics_rectangle_fill(graphics, box.x, box.y, box.w, box.h);

	if(id == gui->active)
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT_ACTIVE]);
	}
	else
	{
		mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_ELT]);
	}
	mp_graphics_rectangle_fill(graphics, thumb.x, thumb.y, thumb.w, thumb.h);

	mp_graphics_set_color(graphics, style->colors[MP_GUI_STYLE_COLOR_BORDER]);
	mp_graphics_set_width(graphics, style->borderWidth);
	mp_graphics_rectangle_stroke(graphics, thumb.x, thumb.y, thumb.w, thumb.h);

	mp_gui_pop_id(gui);

	if(nextValue != *value)
	{
		*value = nextValue;
		return(true);
	}
	else
	{
		return(false);
	}
}


void mp_gui_view_close(mp_gui_context* gui)
{
	mp_gui_view* view = mp_gui_view_top(gui);
	if(view)
	{
		view->open = false;
		view->wasClosedInScope = true;
	}
}

mp_gui_io* mp_gui_context_get_io(mp_gui_context* gui)
{
	return(&gui->input);
}

mp_graphics_context mp_gui_get_graphics(mp_gui_context* gui)
{
	return(gui->graphics);
}

#undef LOG_SUBSYSTEM
