/************************************************************//**
*
*	@file: graphics.c
*	@author: Martin Fouilleul
*	@date: 12/07/2020
*	@revision:
*
*****************************************************************/
#include<math.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include"stb_truetype.h"

#include"lists.h"
#include"memory.h"
#include"macro_helpers.h"
#include"graphics_internal.h"

#define LOG_SUBSYSTEM "Graphics"

//---------------------------------------------------------------
// graphics backend bootstrap
//---------------------------------------------------------------

#ifdef MP_GRAPHICS_METAL_BACKEND
	mp_graphics_surface_data* mp_metal_surface_create_for_window(mp_window window);
#endif

/*TODO
#ifdef MP_GRAPHICS_NSGL_BACKEND
	mp_graphics_surface* mp_nsgl_surface_create_for_window(mp_window* window);
#endif
*/

typedef mp_graphics_surface_data*(*mp_graphics_surface_create_for_window_function)(mp_window window);

typedef struct mp_graphics_backend_bootstrap
{
	mp_graphics_backend_info info;
	mp_graphics_surface_create_for_window_function createForWindow;

	//TODO: createForImage...

} mp_graphics_backend_bootstrap;

enum
{
	#ifdef MP_GRAPHICS_METAL_BACKEND
		MP_GRAPHICS_METAL_BACKEND_INDEX,
	#endif

	#ifdef MP_GRAPHICS_NSGL_BACKEND
		MP_GRAPHICS_NSGL_BACKEND_INDEX,
	#endif

	MP_GRAPHICS_BACKEND_INDEX_MAX
};
const u32 MP_GRAPHICS_BACKEND_COUNT = MP_GRAPHICS_BACKEND_INDEX_MAX;

static const mp_graphics_backend_bootstrap MP_GRAPHICS_BACKEND_BOOTSTRAPS[MP_GRAPHICS_BACKEND_COUNT] = {
		#ifdef MP_GRAPHICS_METAL_BACKEND
		{
			.info = {
				.name = "Metal",
				.description = "Apple Metal API backend",
				.flags = MP_GRAPHICS_BACKEND_ACCELERATED
			},
			.createForWindow = mp_metal_surface_create_for_window
		}
		#endif
		//...
	};

u32 mp_graphics_get_backend_count()
{
	return(MP_GRAPHICS_BACKEND_COUNT);
}

u32 mp_graphics_get_backend_infos(u32 maxCount, mp_graphics_backend_info* backendInfos)
{
	u32 count = minimum(maxCount, MP_GRAPHICS_BACKEND_COUNT);
	for(int i=0; i<count; i++)
	{
		backendInfos[i] = MP_GRAPHICS_BACKEND_BOOTSTRAPS[i].info;
	}
	return(count);
}


//---------------------------------------------------------------
// internal graphics context structs
//---------------------------------------------------------------

typedef enum { MP_GRAPHICS_PATH_MOVE,
               MP_GRAPHICS_PATH_LINE,
	       MP_GRAPHICS_PATH_QUADRATIC,
	       MP_GRAPHICS_PATH_CUBIC } mp_graphics_path_elt_type;

typedef struct mp_graphics_path_elt
{
	mp_graphics_path_elt_type type;
	vec2 p[3];

} mp_graphics_path_elt;

typedef struct mp_graphics_path_descriptor
{
	u32 startIndex;
	u32 count;
	vec2 startPoint;

} mp_graphics_path_descriptor;

typedef struct mp_graphics_attributes
{
	f32 width;
	f32 tolerance;
	mp_graphics_color color;
	mp_graphics_color clearColor;
	mp_graphics_joint_type joint;
	f32 maxJointExcursion;
	mp_graphics_cap_type cap;

	mp_graphics_font font;
	f32 fontSize;

	mp_aligned_rect clip;

} mp_graphics_attributes;

typedef struct mp_graphics_rect
{
	f32 x;
	f32 y;
	f32 w;
	f32 h;
} mp_graphics_rect;

typedef struct mp_graphics_rounded_rect
{
	f32 x;
	f32 y;
	f32 w;
	f32 h;
	f32 r;
} mp_graphics_rounded_rect;

typedef enum { MP_GRAPHICS_CMD_CLEAR = 0,
	       MP_GRAPHICS_CMD_FILL,
	       MP_GRAPHICS_CMD_STROKE,
	       MP_GRAPHICS_CMD_RECT_FILL,
	       MP_GRAPHICS_CMD_RECT_STROKE,
	       MP_GRAPHICS_CMD_ROUND_RECT_FILL,
	       MP_GRAPHICS_CMD_ROUND_RECT_STROKE,
	       MP_GRAPHICS_CMD_ELLIPSE_FILL,
	       MP_GRAPHICS_CMD_ELLIPSE_STROKE,
	       MP_GRAPHICS_CMD_JUMP,
	       MP_GRAPHICS_CMD_MATRIX_PUSH,
	       MP_GRAPHICS_CMD_MATRIX_POP,
	       MP_GRAPHICS_CMD_CLIP_PUSH,
	       MP_GRAPHICS_CMD_CLIP_POP

	     } mp_graphics_primitive_cmd;

typedef struct mp_graphics_primitive
{
	mp_graphics_primitive_cmd cmd;
	mp_graphics_attributes attributes;

	union
	{
		mp_graphics_path_descriptor path;
		mp_graphics_rect rect;
		mp_graphics_rounded_rect roundedRect;
		utf32 codePoint;
		u32 jump;
		mp_mat2x3 matrix;
	};

} mp_graphics_primitive;

typedef struct mp_graphics_glyph_map_entry
{
	unicode_range range;
	u32 firstGlyphIndex;

} mp_graphics_glyph_map_entry;

typedef struct mp_graphics_glyph_info
{
	bool exists;
	utf32 codePoint;
	mp_graphics_path_descriptor pathDescriptor;
	mp_graphics_text_extents extents;
	//...

} mp_graphics_glyph_info;

typedef struct mp_graphics_font_info
{
	list_elt freeListElt;
	u32 generation;

	u32 rangeCount;
	u32 glyphCount;
	u32 outlineCount;
	mp_graphics_glyph_map_entry* glyphMap;
	mp_graphics_glyph_info*      glyphs;
	mp_graphics_path_elt* outlines;

	f32 unitsPerEm;
	mp_graphics_font_extents extents;

} mp_graphics_font_info;

const u32 MP_GRAPHICS_FONT_MAX_COUNT = 8;

const u32 MP_GRAPHICS_STREAM_MAX_COUNT = 128;

typedef struct mp_graphics_stream_data
{
	list_elt freeListElt;
	u32 generation;
	u64 frame;

	u32 firstCommand;
	u32 lastCommand;
	u32 count;

	bool pendingJump;

} mp_graphics_stream_data;

const u32 MP_GRAPHICS_MATRIX_STACK_MAX_DEPTH = 64;
const u32 MP_GRAPHICS_CLIP_STACK_MAX_DEPTH = 64;

const u32 MP_GRAPHICS_MAX_PATH_ELEMENT_COUNT = 2<<20;
const u32 MP_GRAPHICS_MAX_PRIMITIVE_COUNT = 8<<10;

typedef struct mp_graphics_context_data
{
	list_elt freeListElt;
	u32 generation;

	mp_graphics_font_info fonts[MP_GRAPHICS_FONT_MAX_COUNT];
	u32 fontsNextIndex;
	list_info fontsFreeList;

	mp_graphics_stream_data streams[MP_GRAPHICS_STREAM_MAX_COUNT];
	list_info streamsFreeList;
	u64 frameCounter;

	mp_graphics_stream_data* currentStream;

	mp_mat2x3 transform;
	mp_aligned_rect clip;

	mp_graphics_attributes attributes;
	bool textFlip;
	mp_graphics_path_elt pathElements[MP_GRAPHICS_MAX_PATH_ELEMENT_COUNT];
	mp_graphics_path_descriptor path;
	vec2 subPathStartPoint;
	vec2 subPathLastPoint;

	mp_mat2x3 matrixStack[MP_GRAPHICS_MATRIX_STACK_MAX_DEPTH];
	u32 matrixStackSize;

	mp_aligned_rect clipStack[MP_GRAPHICS_CLIP_STACK_MAX_DEPTH];
	u32 clipStackSize;

	u32 nextZIndex;
	u32 primitiveCount;
	mp_graphics_primitive primitives[MP_GRAPHICS_MAX_PRIMITIVE_COUNT];

	u32 vertexCount;
	u32 indexCount;
	mp_graphics_surface_vertex_layout vertexLayout;

} mp_graphics_context_data;

//---------------------------------------------------------------
// internal graphics resources, pools and graphics init
//---------------------------------------------------------------

const u32 MP_GRAPHICS_MAX_SURFACES = 256,
          MP_GRAPHICS_MAX_CONTEXTS = 256;

typedef struct mp_graphics_surface_slot
{
	list_elt freeListElt;
	u32 generation;
	mp_graphics_surface_data* surface;

} mp_graphics_surface_slot;

typedef struct mp_graphics_data
{
	bool init;
	list_info surfaceFreeList;
	list_info contextFreeList;

	mp_graphics_surface_slot surfaceSlots[MP_GRAPHICS_MAX_SURFACES];
	mp_graphics_context_data contexts[MP_GRAPHICS_MAX_CONTEXTS];

} mp_graphics_data;

static mp_graphics_data __mpGraphicsData = {};


void mp_graphics_init()
{
	if(!__mpGraphicsData.init)
	{
		//memset(&__mpGraphicsData, 0, sizeof(__mpGraphicsData));

		ListInit(&__mpGraphicsData.surfaceFreeList);
		for(int i=0; i<MP_GRAPHICS_MAX_SURFACES; i++)
		{
			__mpGraphicsData.surfaceSlots[i].generation = 1;
			ListAppend(&__mpGraphicsData.surfaceFreeList, &__mpGraphicsData.surfaceSlots[i].freeListElt);
		}

		ListInit(&__mpGraphicsData.contextFreeList);
		for(int i=0; i<MP_GRAPHICS_MAX_CONTEXTS; i++)
		{
			__mpGraphicsData.contexts[i].generation = 1;
			ListAppend(&__mpGraphicsData.contextFreeList, &__mpGraphicsData.contexts[i].freeListElt);
		}

		__mpGraphicsData.init = true;
	}
}

mp_graphics_surface_slot* mp_graphics_surface_slot_alloc()
{
	return(ListPopEntry(&__mpGraphicsData.surfaceFreeList, mp_graphics_surface_slot, freeListElt));
}

void mp_graphics_surface_slot_recycle(mp_graphics_surface_slot* slot)
{
	#ifdef DEBUG
		if(slot->generation == UINT32_MAX)
		{
			LOG_ERROR("surface slot generation wrap around\n");
		}
	#endif
	slot->generation++;
	ListPush(&__mpGraphicsData.surfaceFreeList, &slot->freeListElt);
}

mp_graphics_surface_data* mp_graphics_surface_ptr_from_handle(mp_graphics_surface surface)
{
	u32 index = surface.h>>32;
	u32 generation = surface.h & 0xffffffff;
	if(index >= MP_GRAPHICS_MAX_SURFACES)
	{
		return(0);
	}
	mp_graphics_surface_slot* slot = &__mpGraphicsData.surfaceSlots[index];
	if(slot->generation != generation)
	{
		return(0);
	}
	else
	{
		return(slot->surface);
	}
}

mp_graphics_surface mp_graphics_surface_handle_from_slot(mp_graphics_surface_slot* slot)
{
	DEBUG_ASSERT(  (slot - __mpGraphicsData.surfaceSlots) >= 0
	            && (slot - __mpGraphicsData.surfaceSlots) < MP_GRAPHICS_MAX_SURFACES);

	u64 h = ((u64)(slot - __mpGraphicsData.surfaceSlots))<<32
	       |((u64)(slot->generation));
	return((mp_graphics_surface){.h = h});
}

mp_graphics_surface mp_graphics_surface_null_handle()
{
	return((mp_graphics_surface){.h = 0});
}


mp_graphics_context_data* mp_graphics_context_alloc()
{
	return(ListPopEntry(&__mpGraphicsData.contextFreeList, mp_graphics_context_data, freeListElt));
}

void mp_graphics_context_recycle(mp_graphics_context_data* context)
{
	#ifdef DEBUG
		if(context->generation == UINT32_MAX)
		{
			LOG_ERROR("graphics context generation wrap around\n");
		}
	#endif
	context->generation++;
	ListPush(&__mpGraphicsData.contextFreeList, &context->freeListElt);
}

mp_graphics_context_data* mp_graphics_context_ptr_from_handle(mp_graphics_context handle)
{
	u32 index = handle.h>>32;
	u32 generation = handle.h & 0xffffffff;
	if(index >= MP_GRAPHICS_MAX_CONTEXTS)
	{
		return(0);
	}
	mp_graphics_context_data* context = &__mpGraphicsData.contexts[index];
	if(context->generation != generation)
	{
		return(0);
	}
	else
	{
		return(context);
	}
}

mp_graphics_context mp_graphics_context_handle_from_ptr(mp_graphics_context_data* context)
{
	DEBUG_ASSERT(  (context - __mpGraphicsData.contexts) >= 0
	            && (context - __mpGraphicsData.contexts) < MP_GRAPHICS_MAX_CONTEXTS);

	u64 h = ((u64)(context - __mpGraphicsData.contexts))<<32
	       |((u64)(context->generation));
	return((mp_graphics_context){.h = h});
}

mp_graphics_context mp_graphics_context_null_handle()
{
	return((mp_graphics_context){.h = 0});
}

//---------------------------------------------------------------
// graphics stream handles
//---------------------------------------------------------------
/*
	Graphics command stream handles are invalidated when the command stream is appended to the current stream,
	and at the end of the frame.
	Thus command streams handles contain the index of the stream, the frame of the stream, and a generation
	count inside that frame:

	0                                  40                       56           64
	+---------------------------------+------------------------+------------+
	|          frameCounter           |      generation        |    index   |
	+---------------------------------+------------------------+------------+
	               40 bits                     16 bits              8 bits

	(This gives use 2^40 frames, ie ~600 years at 60Hz, 65536 possible reuse per frames and max 256 simultaneous streams.)

	This way we can invalidate use of old handles in the same frame by incrementing the generation counter of the recycled stream,
	and we can invalidate all previous handles at the end of a frame by just incrementing the frame counter of the graphics context.
*/
mp_graphics_stream_data* mp_graphics_stream_alloc(mp_graphics_context_data* context)
{
	mp_graphics_stream_data* stream = ListPopEntry(&context->streamsFreeList, mp_graphics_stream_data, freeListElt);
	if(stream)
	{
		stream->frame = context->frameCounter;
		stream->pendingJump = false;
		stream->count = 0;
		stream->firstCommand = 0;
		stream->lastCommand = 0;
	}
	return(stream);
}

void mp_graphics_stream_recycle(mp_graphics_context_data* context, mp_graphics_stream_data* stream)
{
	#ifdef DEBUG
		if(stream->generation == UINT32_MAX)
		{
			LOG_ERROR("graphics command stream generation wrap around\n");
		}
	#endif
	stream->generation++;
	ListPush(&context->streamsFreeList, &stream->freeListElt);
}

mp_graphics_stream_data* mp_graphics_stream_ptr_from_handle(mp_graphics_context_data* context, mp_graphics_stream handle)
{
	u32 index = handle.h>>56;
	u32 generation = (handle.h>>40) & 0xffff;
	u64 frame = handle.h & 0xffffffffff;

	if(index >= MP_GRAPHICS_STREAM_MAX_COUNT)
	{
		return(0);
	}
	mp_graphics_stream_data* stream = &context->streams[index];
	if( stream->generation != generation
	  ||stream->frame != context->frameCounter)
	{
		return(0);
	}
	else
	{
		return(stream);
	}
}

mp_graphics_stream mp_graphics_stream_handle_from_ptr(mp_graphics_context_data* context, mp_graphics_stream_data* stream)
{
	DEBUG_ASSERT(  (stream - context->streams) >= 0
	            && (stream - context->streams) < MP_GRAPHICS_STREAM_MAX_COUNT);

	u64 h = ((u64)(stream - context->streams))<<56
	       |((u64)(stream->generation))<<40
	       |((u64)(context->frameCounter));

	return((mp_graphics_stream){.h = h});
}

mp_graphics_stream mp_graphics_stream_null_handle()
{
	return((mp_graphics_stream){.h = 0});
}

//---------------------------------------------------------------
// graphics surface forward functions
//---------------------------------------------------------------

mp_graphics_surface mp_graphics_surface_create_for_window(mp_window window, i32 backendIndex, mp_graphics_backend_flags flags)
{
	if(backendIndex <= MP_GRAPHICS_BACKEND_ANY)
	{
		//TODO: chose an index according to flags
		for(int i=0; i<MP_GRAPHICS_BACKEND_COUNT; i++)
		{
			if(MP_GRAPHICS_BACKEND_BOOTSTRAPS[i].info.flags & flags)
			{
				backendIndex = i;
				break;
			}
		}
		if(backendIndex <= MP_GRAPHICS_BACKEND_ANY)
		{
			LOG_ERROR("No graphics backend available for required flags\n");
			return(mp_graphics_surface_null_handle());
		}
	}
	else if(backendIndex >= MP_GRAPHICS_BACKEND_COUNT)
	{
		LOG_ERROR("Selected backend index %i greated than availble backend count\n", backendIndex);
		return(mp_graphics_surface_null_handle());
	}
	else  if(!(MP_GRAPHICS_BACKEND_BOOTSTRAPS[backendIndex].info.flags & flags))
	{
		LOG_ERROR("Selected backend '%s' doesn't match the required flags\n",
			  MP_GRAPHICS_BACKEND_BOOTSTRAPS[backendIndex].info.name);
		return(mp_graphics_surface_null_handle());
	}
	mp_graphics_surface_slot* slot = mp_graphics_surface_slot_alloc();
	if(!slot)
	{
		return(mp_graphics_surface_null_handle());
	}

	slot->surface = MP_GRAPHICS_BACKEND_BOOTSTRAPS[backendIndex].createForWindow(window);
	if(!slot->surface)
	{
		mp_graphics_surface_slot_recycle(slot);
		return(mp_graphics_surface_null_handle());
	}
	else
	{
		return(mp_graphics_surface_handle_from_slot(slot));
	}
}

void mp_graphics_surface_destroy(mp_graphics_surface surface)
{
	mp_graphics_surface_data* surfaceData = mp_graphics_surface_ptr_from_handle(surface);
	if(surfaceData)
	{
		surfaceData->destroy(surfaceData);
	}
}

void mp_graphics_surface_present(mp_graphics_surface surface)
{
	mp_graphics_surface_data* surfaceData = mp_graphics_surface_ptr_from_handle(surface);
	if(surfaceData)
	{
		surfaceData->present(surfaceData);
	}
}

//---------------------------------------------------------------
// internal graphics context functions
//---------------------------------------------------------------

mp_mat2x3 mp_mat2x3_mul_m(mp_mat2x3 lhs, mp_mat2x3 rhs)
{
	mp_mat2x3 res;
	res.m[0] = lhs.m[0]*rhs.m[0] + lhs.m[1]*rhs.m[3];
	res.m[1] = lhs.m[0]*rhs.m[1] + lhs.m[1]*rhs.m[4];
	res.m[2] = lhs.m[0]*rhs.m[2] + lhs.m[1]*rhs.m[5] + lhs.m[2];
	res.m[3] = lhs.m[3]*rhs.m[0] + lhs.m[4]*rhs.m[3];
	res.m[4] = lhs.m[3]*rhs.m[1] + lhs.m[4]*rhs.m[4];
	res.m[5] = lhs.m[3]*rhs.m[2] + lhs.m[4]*rhs.m[5] + lhs.m[5];

	return(res);
}

vec2 mp_mat2x3_mul(mp_mat2x3 m, vec2 p)
{
	f32 x = p.x*m.m[0] + p.y*m.m[1] + m.m[2];
	f32 y = p.x*m.m[3] + p.y*m.m[4] + m.m[5];
	return((vec2){x, y});
}

void mp_graphics_reset_z_index(mp_graphics_context_data* context)
{
	context->nextZIndex = 1;
}

u32 mp_graphics_get_next_z_index(mp_graphics_context_data* context)
{
	return(context->nextZIndex++);
}

///////////////////////////////////////  WIP  /////////////////////////////////////////////////////////////////////////
u32 mp_graphics_vertices_base_index(mp_graphics_context_data* context)
{
	return(context->vertexCount);
}

void mp_graphics_push_vertex(mp_graphics_context_data* context, vec2 pos, vec4 uv, mp_graphics_color color, u64 zIndex)
{
	mp_graphics_surface_vertex_layout* layout = &context->vertexLayout;

	DEBUG_ASSERT(context->vertexCount < layout->maxVertexCount);

	u32 offset = context->vertexCount;

	*(vec2*)(((char*)layout->posBuffer) + offset*layout->posStride) = pos;
	*(vec4*)(((char*)layout->uvBuffer) + offset*layout->uvStride) = uv;
	*(mp_graphics_color*)(((char*)layout->colorBuffer) + offset*layout->colorStride) = color;
	*(u32*)(((char*)layout->zIndexBuffer) + offset*layout->zIndexStride) = zIndex;
	*(mp_aligned_rect*)(((char*)layout->clipsBuffer) + offset*layout->clipsStride) = context->clip;

	context->vertexCount++;
}
///////////////////////////////////////  WIP  /////////////////////////////////////////////////////////////////////////

int* mp_graphics_reserve_indices(mp_graphics_context_data* context, u32 indexCount)
{
	mp_graphics_surface_vertex_layout* layout = &context->vertexLayout;

	ASSERT(context->indexCount + indexCount < layout->maxIndexCount);
	int* base = ((int*)layout->indexBuffer) + context->indexCount;
	context->indexCount += indexCount;
	return(base);
}

//-----------------------------------------------------------------------------------------------------------
// Path Filling
//-----------------------------------------------------------------------------------------------------------
//NOTE(martin): forward declarations
void mp_graphics_render_fill_cubic(mp_graphics_context_data* context, vec2 p[4], u32 zIndex, mp_graphics_color color);

//NOTE(martin): quadratics filling

void mp_graphics_render_fill_quadratic(mp_graphics_context_data* context, vec2 p[3], u32 zIndex, mp_graphics_color color)
{
	u32 baseIndex = mp_graphics_vertices_base_index(context);

	i32* indices = mp_graphics_reserve_indices(context, 3);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[0].x, p[0].y}),
	                        (vec4){0, 0, 0, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[1].x, p[1].y}),
	                        (vec4){0.5, 0, 0.5, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[2].x, p[2].y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
}

//NOTE(martin): cubic filling

void mp_graphics_split_and_fill_cubic(mp_graphics_context_data* context, vec2 p[4], f32 tSplit, u32 zIndex, mp_graphics_color color)
{
	int subVertexCount = 0;
	int subIndexCount = 0;

	f32 OneMinusTSplit = 1-tSplit;

	vec2 q0 = {OneMinusTSplit*p[0].x + tSplit*p[1].x,
		   OneMinusTSplit*p[0].y + tSplit*p[1].y};

	vec2 q1 = {OneMinusTSplit*p[1].x + tSplit*p[2].x,
		   OneMinusTSplit*p[1].y + tSplit*p[2].y};

	vec2 q2 = {OneMinusTSplit*p[2].x + tSplit*p[3].x,
		   OneMinusTSplit*p[2].y + tSplit*p[3].y};

	vec2 r0 = {OneMinusTSplit*q0.x + tSplit*q1.x,
		   OneMinusTSplit*q0.y + tSplit*q1.y};

	vec2 r1 = {OneMinusTSplit*q1.x + tSplit*q2.x,
		   OneMinusTSplit*q1.y + tSplit*q2.y};

	vec2 split = {OneMinusTSplit*r0.x + tSplit*r1.x,
		     OneMinusTSplit*r0.y + tSplit*r1.y};;

	vec2 subPointsLow[4] = {p[0], q0, r0, split};
	vec2 subPointsHigh[4] = {split, r1, q2, p[3]};

	//NOTE(martin): add base triangle
	u32 baseIndex = mp_graphics_vertices_base_index(context);
	i32* indices = mp_graphics_reserve_indices(context, 3);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[0].x, p[0].y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){split.x, split.y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[3].x, p[3].y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;

	mp_graphics_render_fill_cubic(context, subPointsLow, zIndex, color);
	mp_graphics_render_fill_cubic(context, subPointsHigh, zIndex, color);

	return;
}

void mp_graphics_render_fill_cubic(mp_graphics_context_data* context, vec2 p[4], u32 zIndex, mp_graphics_color color)
{
	LOG_DEBUG("graphics render fill cubic\n");

	vec4 testCoords[4];

	/*NOTE(martin): first convert the control points to power basis, multiplying by M3

		     | 1  0  0  0|
		M3 = |-3  3  0  0|
		     | 3 -6  3  0|
		     |-1  3 -3  1|
		ie:
		    c0 = p0
		    c1 = -3*p0 + 3*p1
		    c2 = 3*p0 - 6*p1 + 3*p2
		    c3 = -p0 + 3*p1 - 3*p2 + p3
	*/
	f32 c1x = 3.0*p[1].x - 3.0*p[0].x;
	f32 c1y = 3.0*p[1].y - 3.0*p[0].y;

	f32 c2x = 3.0*p[0].x + 3.0*p[2].x - 6.0*p[1].x;
	f32 c2y = 3.0*p[0].y + 3.0*p[2].y - 6.0*p[1].y;

	f32 c3x = 3.0*p[1].x - 3.0*p[2].x + p[3].x - p[0].x;
	f32 c3y = 3.0*p[1].y - 3.0*p[2].y + p[3].y - p[0].y;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//TODO(martin): we should do the tex coords computations in f64 and scale them to avoid f32 precision/range glitches in shader
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	c1x /= 10;
	c1y /= 10;
	c2x /= 10;
	c2y /= 10;
	c3x /= 10;
	c3y /= 10;

	/*NOTE(martin):
		now, compute determinants d0, d1, d2, d3, which gives the coefficients of the
	        inflection points polynomial:

		I(t, s) = d0*t^3 - 3*d1*t^2*s + 3*d2*t*s^2 - d3*s^3

		The roots of this polynomial are the inflection points of the parametric curve, in homogeneous
		coordinates (ie we can have an inflection point at inifinity with s=0).

		         |x3 y3 w3|              |x3 y3 w3|             |x3 y3 w3|              |x2 y2 w2|
		d0 = det |x2 y2 w2|    d1 = -det |x2 y2 w2|    d2 = det |x1 y1 w1|    d3 = -det |x1 y1 w1|
		         |x1 y1 w1|              |x0 y0 w0|             |x0 y0 w0|              |x0 y0 w0|

		In our case, the pi.w equal 1 (no point at infinity), so _in_the_power_basis_, w1 = w2 = w3 = 0 and w0 = 1
		(which also means d0 = 0)
	*/

	f32 d1 = c3y*c2x - c3x*c2y;
	f32 d2 = c3x*c1y - c3y*c1x;
	f32 d3 = c2y*c1x - c2x*c1y;

	//NOTE(martin): compute the second factor of the discriminant discr(I) = d1^2*(3*d2^2 - 4*d3*d1)
	f32 discrFactor2 = 3.0*Square(d2) - 4.0*d3*d1;

	//NOTE(martin): each following case gives the number of roots, hence the category of the parametric curve
	if(fabs(d1) < 0.1 && fabs(d2) < 0.1 && d3 != 0)
	{
		//NOTE(martin): quadratic degenerate case
		LOG_DEBUG("quadratic curve\n");

		//NOTE(martin): compute quadratic curve control point, which is at p0 + 1.5*(p1-p0) = 1.5*p1 - 0.5*p0
		vec2 quadControlPoints[3] = { p[0],
		                             {1.5*p[1].x - 0.5*p[0].x, 1.5*p[1].y - 0.5*p[0].y},
				  	     p[3]};

		mp_graphics_render_fill_quadratic(context, quadControlPoints, zIndex, color);
		return;
	}
	else if( (discrFactor2 > 0 && d1 != 0)
	  ||(discrFactor2 == 0 && d1 != 0))
	{
		//NOTE(martin): serpentine curve or cusp with inflection at infinity
		//              (these two cases are handled the same way).
		LOG_DEBUG("%s\n", (discrFactor2 > 0 && d1 != 0) ? "serpentine curve" : "cusp with inflection at infinity");

		//NOTE(martin): compute the solutions (tl, sl), (tm, sm), and (tn, sn) of the inflection point equation
		f32 tl = d2 + sqrt(discrFactor2/3);
		f32 sl = 2*d1;
		f32 tm = d2 - sqrt(discrFactor2/3);
		f32 sm = sl;

		/*NOTE(martin):
			the power basis coefficients of points k,l,m,n are collected into the rows of the 4x4 matrix F:

				| tl*tm            tl^3        tm^3        1 |
				| -sm*tl - sl*tm   -3sl*tl^2   -3*sm*tm^2  0 |
				| sl*sm            3*sl^2*tl   3*sm^2*tm   0 |
				| 0                -sl^3       -sm^3       0 |

			This matrix is then multiplied by M3^(-1) on the left which yelds the bezier coefficients of k, l, m, n
			which are assigned as a 4D texture coordinates to control points.


			                   | 1  0   0   0 |
				M3^(-1) =  | 1  1/3 0   0 |
				           | 1  2/3 1/3 0 |
					   | 1  1   1   1 |
		*/
		testCoords[0].x = tl*tm;
		testCoords[0].y = Cube(tl);
		testCoords[0].z = Cube(tm);

		testCoords[1].x = tl*tm - (sm*tl + sl*tm)/3;
		testCoords[1].y = Cube(tl) - sl*Square(tl);
		testCoords[1].z = Cube(tm) - sm*Square(tm);

		testCoords[2].x = tl*tm - (sm*tl + sl*tm)*2/3 + sl*sm/3;
		testCoords[2].y = Cube(tl) - 2*sl*Square(tl) + Square(sl)*tl;
		testCoords[2].z = Cube(tm) - 2*sm*Square(tm) + Square(sm)*tm;

		testCoords[3].x = tl*tm - (sm*tl + sl*tm) + sl*sm;
		testCoords[3].y = Cube(tl) - 3*sl*Square(tl) + 3*Square(sl)*tl - Cube(sl);
		testCoords[3].z = Cube(tm) - 3*sm*Square(tm) + 3*Square(sm)*tm - Cube(sm);
	}
	else if(discrFactor2 < 0 && d1 != 0)
	{
		//NOTE(martin): loop curve
		LOG_DEBUG("loop curve\n");

		f32 td = d2 + sqrt(-discrFactor2);
		f32 sd = 2*d1;
		f32 te = d2 - sqrt(-discrFactor2);
		f32 se = sd;

		//NOTE(martin): if one of the parameters (td/sd) or (te/se) is in the interval [0,1], the double point
		//              is inside the control points convex hull and would cause a shading anomaly. If this is
		//              the case, subdivide the curve at that point

		//TODO: study edge case where td/sd ~ 1 or 0 (which causes an infinite recursion in split and fill).
		//      quick fix for now is adding a little slop in the check...

		if(sd != 0 && td/sd < 0.99 && td/sd > 0.01)
		{
			LOG_DEBUG("split curve at first double point\n");
			mp_graphics_split_and_fill_cubic(context, p, td/sd, zIndex, color);
			return;
		}
		if(se != 0 && te/se < 0.99 && te/se > 0.01)
		{
			LOG_DEBUG("split curve at second double point\n");
			mp_graphics_split_and_fill_cubic(context, p, te/se, zIndex, color);
			return;
		}

		/*NOTE(martin):
			the power basis coefficients of points k,l,m,n are collected into the rows of the 4x4 matrix F:

				| td*te            td^2*te                 td*te^2                1 |
				| -se*td - sd*te   -se*td^2 - 2sd*te*td    -sd*te^2 - 2*se*td*te  0 |
				| sd*se            te*sd^2 + 2*se*td*sd    td*se^2 + 2*sd*te*se   0 |
				| 0                -sd^2*se                -sd*se^2               0 |

			This matrix is then multiplied by M3^(-1) on the left which yelds the bezier coefficients of k, l, m, n
			which are assigned as a 4D texture coordinates to control points.


			                   | 1  0   0   0 |
				M3^(-1) =  | 1  1/3 0   0 |
				           | 1  2/3 1/3 0 |
					   | 1  1   1   1 |
		*/
		testCoords[0].x = td*te;
		testCoords[0].y = Square(td)*te;
		testCoords[0].z = td*Square(te);

		testCoords[1].x = td*te - (se*td + sd*te)/3.0;
		testCoords[1].y = Square(td)*te - (se*Square(td) + 2.*sd*te*td)/3.0;
		testCoords[1].z = td*Square(te) - (sd*Square(te) + 2*se*td*te)/3.0;

		testCoords[2].x = td*te - 2.0*(se*td + sd*te)/3.0 + sd*se/3.0;
		testCoords[2].y = Square(td)*te - 2.0*(se*Square(td) + 2.0*sd*te*td)/3.0 + (te*Square(sd) + 2.0*se*td*sd)/3.0;
		testCoords[2].z = td*Square(te) - 2.0*(sd*Square(te) + 2.0*se*td*te)/3.0 + (td*Square(se) + 2.0*sd*te*se)/3.0;

		testCoords[3].x = td*te - (se*td + sd*te) + sd*se;
		testCoords[3].y = Square(td)*te - (se*Square(td) + 2.0*sd*te*td) + (te*Square(sd) + 2.0*se*td*sd) - Square(sd)*se;
		testCoords[3].z = td*Square(te) - (sd*Square(te) + 2.0*se*td*te) + (td*Square(se) + 2.0*sd*te*se) - sd*Square(se);
	}
	else if(d1 == 0 && d2 != 0)
	{
		//NOTE(martin): cusp with cusp at infinity
		LOG_DEBUG("cusp at infinity curve\n");

		f32 tl = d3;
		f32 sl = 3*d2;

		/*NOTE(martin):
			the power basis coefficients of points k,l,m,n are collected into the rows of the 4x4 matrix F:

				| tl    tl^3        1  1 |
				| -sl   -3sl*tl^2   0  0 |
				| 0     3*sl^2*tl   0  0 |
				| 0     -sl^3       0  0 |

			This matrix is then multiplied by M3^(-1) on the left which yelds the bezier coefficients of k, l, m, n
			which are assigned as a 4D texture coordinates to control points.


			                   | 1  0   0   0 |
				M3^(-1) =  | 1  1/3 0   0 |
				           | 1  2/3 1/3 0 |
					   | 1  1   1   1 |
		*/

		testCoords[0].x = tl;
		testCoords[0].y = Cube(tl);
		testCoords[0].z = 1;

		testCoords[1].x = tl - sl/3;
		testCoords[1].y = Cube(tl) - sl*Square(tl);
		testCoords[1].z = 1;

		testCoords[2].x = tl - sl*2/3;
		testCoords[2].y = Cube(tl) - 2*sl*Square(tl) + Square(sl)*tl;
		testCoords[2].z = 1;

		testCoords[3].x = tl - sl;
		testCoords[3].y = Cube(tl) - 3*sl*Square(tl) + 3*Square(sl)*tl - Cube(sl);
		testCoords[3].z = 1;
	}
	else if(d1 == 0 && d2 == 0 && d3 == 0)
	{
		//NOTE(martin): line or point degenerate case, ignored
		LOG_DEBUG("line or point curve (ignored)\n");
		return;
	}
	else
	{
		//TODO(martin): handle error ? put some epsilon slack on the conditions ?
		LOG_DEBUG("none of the above...\n");
		ASSERT(0, "not implemented yet !");
		return;
	}

	//NOTE(martin): compute convex hull indices using Gift wrapping / Jarvis' march algorithm
	int convexHullIndices[4];
	int leftMostPointIndex = 0;

	for(int i=0; i<4; i++)
	{
		if(p[i].x < p[leftMostPointIndex].x)
		{
			leftMostPointIndex = i;
		}
	}
	int currentPointIndex = leftMostPointIndex;
	int i=0;
	int convexHullCount = 0;

	do
	{
		convexHullIndices[i] = currentPointIndex;
		convexHullCount++;
		int bestGuessIndex = 0;

		for(int j=0; j<4; j++)
		{
			vec2 bestGuessEdge = {.x = p[bestGuessIndex].x - p[currentPointIndex].x,
		                              .y = p[bestGuessIndex].y - p[currentPointIndex].y};

			vec2 nextGuessEdge = {.x = p[j].x - p[currentPointIndex].x,
		                              .y = p[j].y - p[currentPointIndex].y};

			//NOTE(martin): if control point j is on the right of current edge, it is a best guess
			//              (in case of colinearity we choose the point which is farthest from the current point)

			f32 crossProduct = bestGuessEdge.x*nextGuessEdge.y - bestGuessEdge.y*nextGuessEdge.x;

			if(  bestGuessIndex == currentPointIndex
			  || crossProduct < 0)
			{
				bestGuessIndex = j;
			}
			else if(crossProduct == 0)
			{

				//NOTE(martin): if vectors v1, v2 are colinear and distinct, and ||v1|| > ||v2||,
				//               either abs(v1.x) > abs(v2.x) or abs(v1.y) > abs(v2.y)
				//               so we don't actually need to compute their norm to select the greatest
				//               (and if v1 and v2 are equal we don't have to update our best guess.)

				//TODO(martin): in case of colinearity we should rather select the edge that has the greatest dot product with last edge ??

				if(fabs(nextGuessEdge.x) > fabs(bestGuessEdge.x)
				  || fabs(nextGuessEdge.y) > fabs(bestGuessEdge.y))
				{
					bestGuessIndex = j;
				}
			}
		}
		i++;
		currentPointIndex = bestGuessIndex;

	} while(currentPointIndex != leftMostPointIndex && i<4);

	//TODO: quick fix, maybe later cull degenerate hulls beforehand
	if(convexHullCount <= 2)
	{
		//NOTE(martin): if convex hull has only two point, we have a degenerate cubic that displays nothing.
		return;
	}

	//NOTE(martin): rearrange convex hull to put p0 first
	int startIndex = -1;
	int orderedHullIndices[4];
	for(int i=0; i<convexHullCount; i++)
	{
		if(convexHullIndices[i] == 0)
		{
			startIndex = i;
		}
		if(startIndex >= 0)
		{
			orderedHullIndices[i-startIndex] = convexHullIndices[i];
		}
	}
	for(int i=0; i<startIndex; i++)
	{
		orderedHullIndices[convexHullCount-startIndex+i] = convexHullIndices[i];
	}

	//NOTE(martin): inside/outside tests for the two triangles. In the shader, the outside is defined by s*(k^3 - lm) > 0
	//              ie the 4th coordinate s flips the inside/outside test.
	//              We affect s such that control points p1 and p2 are always outside the covered area.

	if(convexHullCount <= 3)
	{
		//NOTE(martin): the convex hull is a triangle

		//NOTE(martin): when we degenerate from 4 control points to a 3 points convex hull, this means one of the
		//              control points p1 or p2 could be inside the covered area. We want to compute the test for the
		//              control point which belongs to the convex hull, as we know it should be outside the covered area.
		//
		//              Since there are 3 points in the hull and p0 is the first, and p3 belongs to the hull, this means we
		//              must select the point of the convex hull which is neither the first, nor p3.

		int testPointIndex = orderedHullIndices[1] == 3 ? orderedHullIndices[2] : orderedHullIndices[1];
		int outsideTest = 1;
		if(Cube(testCoords[testPointIndex].x)-testCoords[testPointIndex].y*testCoords[testPointIndex].z < 0)
		{
			outsideTest = -1;
		}

		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 3);

		for(int i=0; i<3; i++)
		{
			vec2 pos = mp_mat2x3_mul(context->transform, p[orderedHullIndices[i]]);
			vec4 uv = testCoords[orderedHullIndices[i]];
			uv.w = outsideTest;
			mp_graphics_push_vertex(context, pos, uv, color, zIndex);

			indices[i] = baseIndex + i;
		}
	}
	else if(orderedHullIndices[2] == 3)
	{
		//NOTE(martin): p1 and p2 are not on the same side of (p0,p3). The outside test can be different for
		//              the two triangles
		int outsideTest1 = 1;
		int outsideTest2 = 1;

		int testIndex = orderedHullIndices[1];
		if(Cube(testCoords[testIndex].x)-testCoords[testIndex].y*testCoords[testIndex].z < 0)
		{
			outsideTest1 = -1;
		}

		testIndex = orderedHullIndices[3];
		if(Cube(testCoords[testIndex].x)-testCoords[testIndex].y*testCoords[testIndex].z < 0)
		{
			outsideTest2 = -1;
		}

		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 6);

		mp_graphics_vertex vertices[6];

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[0]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[0]]), outsideTest1},
		                        color,
		                        zIndex);
		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[1]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[1]]), outsideTest1},
		                        color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[2]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[2]]), outsideTest1},
		                        color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[0]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[0]]), outsideTest2},
		                        color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[2]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[2]]), outsideTest2},
		                        color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p[orderedHullIndices[3]]),
		                        (vec4){vec4_expand_xyz(testCoords[orderedHullIndices[3]]), outsideTest2},
		                        color,
		                        zIndex);

		indices[0] = baseIndex + 0;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
		indices[3] = baseIndex + 3;
		indices[4] = baseIndex + 4;
		indices[5] = baseIndex + 5;
	}
	else
	{
		//NOTE(martin): if p1 and p2 are on the same side of (p0,p3), the outside test is the same for both triangles
		int outsideTest = 1;
		if(Cube(testCoords[1].x)-testCoords[1].y*testCoords[1].z < 0)
		{
			outsideTest = -1;
		}

		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 6);

		for(int i=0; i<4; i++)
		{
			mp_graphics_push_vertex(context,
		                        	mp_mat2x3_mul(context->transform, p[orderedHullIndices[i]]),
		                        	(vec4){vec4_expand_xyz(testCoords[orderedHullIndices[i]]), outsideTest},
		                        	color,
		                        	zIndex);
		}

		indices[0] = baseIndex + 0;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
		indices[3] = baseIndex + 0;
		indices[4] = baseIndex + 2;
		indices[5] = baseIndex + 3;
	}
}

//NOTE(martin): global path fill

void mp_graphics_render_fill(mp_graphics_context_data* context, mp_graphics_path_elt* elements, mp_graphics_path_descriptor* path, u32 zIndex, mp_graphics_color color)
{
	u32 eltCount = path->count;
	vec2 startPoint = path->startPoint;
	vec2 endPoint = path->startPoint;
	vec2 currentPoint = path->startPoint;

	for(int eltIndex=0; eltIndex<eltCount; eltIndex++)
	{
		mp_graphics_path_elt* elt = &(elements[eltIndex]);

		vec2 controlPoints[4] = {currentPoint, elt->p[0], elt->p[1], elt->p[2]};

		switch(elt->type)
		{
			case MP_GRAPHICS_PATH_MOVE:
			{
				startPoint = elt->p[0];
				endPoint = elt->p[0];
				currentPoint = endPoint;
				continue;
			} break;

			case MP_GRAPHICS_PATH_LINE:
			{
				endPoint = controlPoints[1];
			} break;

			case MP_GRAPHICS_PATH_QUADRATIC:
			{
				mp_graphics_render_fill_quadratic(context, controlPoints, zIndex, color);
				endPoint = controlPoints[2];

			} break;

			case MP_GRAPHICS_PATH_CUBIC:
			{
				mp_graphics_render_fill_cubic(context, controlPoints, zIndex, color);
				endPoint = controlPoints[3];
			} break;
		}

		//NOTE(martin): now fill interior triangle
		u32 baseIndex = mp_graphics_vertices_base_index(context);
		int* indices = mp_graphics_reserve_indices(context, 3);

		vec2 pos[3];
		pos[0] = mp_mat2x3_mul(context->transform, startPoint);
		pos[1] = mp_mat2x3_mul(context->transform, currentPoint);
		pos[2] = mp_mat2x3_mul(context->transform, endPoint);

		vec4 uv = {1, 1, 1, 1};

		for(int i=0; i<3; i++)
		{
			mp_graphics_push_vertex(context, pos[i], uv, color, zIndex);
			indices[i] = baseIndex + i;
		}

		currentPoint = endPoint;
	}
}

//-----------------------------------------------------------------------------------------------------------
// Path Stroking
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_render_stroke_line(mp_graphics_context_data* context, vec2 p[2], u32 zIndex, mp_graphics_attributes* attributes)
{
	//NOTE(martin): get normals multiplied by halfWidth
	f32 halfW = attributes->width/2;

	vec2 n0 = {p[0].y - p[1].y,
		   p[1].x - p[0].x};
	f32 norm0 = sqrt(n0.x*n0.x + n0.y*n0.y);
	n0.x *= halfW/norm0;
	n0.y *= halfW/norm0;


	mp_graphics_color color = attributes->color;

	u32 baseIndex = mp_graphics_vertices_base_index(context);
	i32* indices = mp_graphics_reserve_indices(context, 6);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[0].x + n0.x, p[0].y + n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[1].x + n0.x, p[1].y + n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[1].x - n0.x, p[1].y - n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p[0].x - n0.x, p[0].y - n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        color,
	                        zIndex);

	indices[0] = baseIndex;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;
}

void mp_graphics_offset_hull(int count, vec2* p, vec2* result, f32 offset)
{
//////////////////////////////////////////////////////////////////////////////////////
//WARN: quick fix for coincident middle control points
	if(count == 4 && (p[1].x - p[2].x < 0.01) && (p[1].y - p[2].y < 0.01))
	{
		vec2 hull3[3] = {p[0], p[1], p[3]};
		vec2 result3[3];
		mp_graphics_offset_hull(3, hull3, result3, offset);
		result[0] = result3[0];
		result[1] = result3[1];
		result[2] = result3[1];
		result[3] = result3[2];
		return;
	}
/////////////////////////////////////////////////////////////////////////////////////:

	//TODO(martin): review edge cases (coincident points ? colinear points ? control points pointing outward end point?)
	//NOTE(martin): first offset control point is just the offset of first control point
	vec2 n = {p[0].y - p[1].y,
	          p[1].x - p[0].x};
	f32 norm = sqrt(n.x*n.x + n.y*n.y);
	n.x *= offset/norm;
	n.y *= offset/norm;

	result[0].x = p[0].x + n.x;
	result[0].y = p[0].y + n.y;

	//NOTE(martin): subsequent offset control points are the intersection of offset control lines
	for(int i=1; i<count-1; i++)
	{
		vec2 p0 = p[i-1];
		vec2 p1 = p[i];
		vec2 p2 = p[i+1];

		//NOTE(martin): get normals
		vec2 n0 = {p0.y - p1.y,
			   p1.x - p0.x};
		f32 norm0 = sqrt(n0.x*n0.x + n0.y*n0.y);
		n0.x /= norm0;
		n0.y /= norm0;

		vec2 n1 = {p1.y - p2.y,
			   p2.x - p1.x};
		f32 norm1 = sqrt(n1.x*n1.x + n1.y*n1.y);
		n1.x /= norm1;
		n1.y /= norm1;

		/*NOTE(martin): let vector u = (n0+n1) and vector v = pIntersect - p1
		                then v = u * (2*offset / norm(u)^2)
			        (this can be derived from writing the pythagoras theorems in the triangles of the joint)
		*/
		vec2 u = {n0.x + n1.x, n0.y + n1.y};
		f32 uNormSquare = u.x*u.x + u.y*u.y;
		f32 alpha = 2*offset / uNormSquare;
		vec2 v = {u.x * alpha, u.y * alpha};

		result[i].x = p1.x + v.x;
		result[i].y = p1.y + v.y;

		ASSERT(!isnan(result[i].x));
		ASSERT(!isnan(result[i].y));
	}

	//NOTE(martin): last offset control point is just the offset of last control point
	n = (vec2){p[count-2].y - p[count-1].y,
	           p[count-1].x - p[count-2].x};
	norm = sqrt(n.x*n.x + n.y*n.y);
	n.x *= offset/norm;
	n.y *= offset/norm;

	result[count-1].x = p[count-1].x + n.x;
	result[count-1].y = p[count-1].y + n.y;
}

vec2 mp_graphics_quadratic_get_point(vec2 p[3], f32 t)
{
	vec2 r;

	f32 oneMt = 1-t;
	f32 oneMt2 = Square(oneMt);
	f32 t2 = Square(t);

	r.x = oneMt2*p[0].x + 2*oneMt*t*p[1].x + t2*p[2].x;
	r.y = oneMt2*p[0].y + 2*oneMt*t*p[1].y + t2*p[2].y;

	return(r);
}

void mp_graphics_quadratic_split(vec2 p[3], f32 t, vec2 outLeft[3], vec2 outRight[3])
{
	//NOTE(martin): split bezier curve p at parameter t, using De Casteljau's algorithm
	//              the q_n are the points along the hull's segments at parameter t
	//              s is the split point.

	f32 oneMt = 1-t;

	vec2 q0 = {oneMt*p[0].x + t*p[1].x,
		   oneMt*p[0].y + t*p[1].y};

	vec2 q1 = {oneMt*p[1].x + t*p[2].x,
		   oneMt*p[1].y + t*p[2].y};

	vec2 s = {oneMt*q0.x + t*q1.x,
		   oneMt*q0.y + t*q1.y};

	outLeft[0] = p[0];
	outLeft[1] = q0;
	outLeft[2] = s;

	outRight[0] = s;
	outRight[1] = q1;
	outRight[2] = p[2];
}


void mp_graphics_render_stroke_quadratic(mp_graphics_context_data* context, vec2 p[4], u32 zIndex, mp_graphics_attributes* attributes)
{
	#define CHECK_SAMPLE_COUNT 5
	f32 checkSamples[CHECK_SAMPLE_COUNT] = {1./6, 2./6, 3./6, 4./6, 5./6};

	vec2 positiveOffsetHull[3];
	vec2 negativeOffsetHull[3];

	mp_graphics_offset_hull(3, p, positiveOffsetHull, 0.5 * attributes->width);
	mp_graphics_offset_hull(3, p, negativeOffsetHull, -0.5 * attributes->width);

	//NOTE(martin): the distance d between the offset curve and the path must be between w/2-tolerance and w/2+tolerance
	//              thus, by constraining tolerance to be at most, 0.5*width, we can rewrite this condition like this:
	//
	//              (w/2-tolerance)^2 < d^2 < (w/2+tolerance)^2
	//
	//		we compute the maximum overshoot outside these bounds and split the curve at the corresponding parameter

	f32 tolerance = minimum(attributes->tolerance, 0.5 * attributes->width);
	f32 d2LowBound = Square(0.5 * attributes->width - attributes->tolerance);
	f32 d2HighBound = Square(0.5 * attributes->width + attributes->tolerance);

	f32 maxOvershoot = 0;
	f32 maxOvershootParameter = 0;

	for(int i=0; i<CHECK_SAMPLE_COUNT; i++)
	{
		f32 t = checkSamples[i];

		vec2 c = mp_graphics_quadratic_get_point(p, t);
		vec2 cp =  mp_graphics_quadratic_get_point(positiveOffsetHull, t);
		vec2 cn =  mp_graphics_quadratic_get_point(negativeOffsetHull, t);

		f32 positiveDistSquare = Square(c.x - cp.x) + Square(c.y - cp.y);
		f32 negativeDistSquare = Square(c.x - cn.x) + Square(c.y - cn.y);

		f32 positiveOvershoot = maximum(positiveDistSquare - d2HighBound, d2LowBound - positiveDistSquare);
		f32 negativeOvershoot = maximum(negativeDistSquare - d2HighBound, d2LowBound - negativeDistSquare);

		f32 overshoot = maximum(positiveOvershoot, negativeOvershoot);

		if(overshoot > maxOvershoot)
		{
			maxOvershoot = overshoot;
			maxOvershootParameter = t;
		}
	}

	if(maxOvershoot > 0)
	{
		//TODO(martin): split at maxErrorParameter and recurse
		vec2 splitLeft[3];
		vec2 splitRight[3];
		mp_graphics_quadratic_split(p, maxOvershootParameter, splitLeft, splitRight);
		mp_graphics_render_stroke_quadratic(context, splitLeft, zIndex, attributes);
		mp_graphics_render_stroke_quadratic(context, splitRight, zIndex, attributes);
	}
	else
	{
		//NOTE(martin): push the actual fill commands for the offset contour

		u32 zIndex = mp_graphics_get_next_z_index(context);

		mp_graphics_render_fill_quadratic(context, positiveOffsetHull, zIndex, attributes->color);
		mp_graphics_render_fill_quadratic(context, negativeOffsetHull, zIndex, attributes->color);

		//NOTE(martin):	add base triangles
		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 6);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, positiveOffsetHull[0]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, positiveOffsetHull[2]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, negativeOffsetHull[2]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, negativeOffsetHull[0]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		indices[0] = baseIndex + 0;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
		indices[3] = baseIndex + 0;
		indices[4] = baseIndex + 2;
		indices[5] = baseIndex + 3;
	}
	#undef CHECK_SAMPLE_COUNT
}

vec2 mp_graphics_cubic_get_point(vec2 p[4], f32 t)
{
	vec2 r;

	f32 oneMt = 1-t;
	f32 oneMt2 = Square(oneMt);
	f32 oneMt3 = oneMt2*oneMt;
	f32 t2 = Square(t);
	f32 t3 = t2*t;

	r.x = oneMt3*p[0].x + 3*oneMt2*t*p[1].x + 3*oneMt*t2*p[2].x + t3*p[3].x;
	r.y = oneMt3*p[0].y + 3*oneMt2*t*p[1].y + 3*oneMt*t2*p[2].y + t3*p[3].y;

	return(r);
}

void mp_graphics_cubic_split(vec2 p[4], f32 t, vec2 outLeft[4], vec2 outRight[4])
{
	//NOTE(martin): split bezier curve p at parameter t, using De Casteljau's algorithm
	//              the q_n are the points along the hull's segments at parameter t
	//              the r_n are the points along the (q_n, q_n+1) segments at parameter t
	//              s is the split point.

	f32 oneMt = 1-t;

	vec2 q0 = {oneMt*p[0].x + t*p[1].x,
		   oneMt*p[0].y + t*p[1].y};

	vec2 q1 = {oneMt*p[1].x + t*p[2].x,
		   oneMt*p[1].y + t*p[2].y};

	vec2 q2 = {oneMt*p[2].x + t*p[3].x,
		   oneMt*p[2].y + t*p[3].y};

	vec2 r0 = {oneMt*q0.x + t*q1.x,
		   oneMt*q0.y + t*q1.y};

	vec2 r1 = {oneMt*q1.x + t*q2.x,
		   oneMt*q1.y + t*q2.y};

	vec2 s = {oneMt*r0.x + t*r1.x,
		  oneMt*r0.y + t*r1.y};;

	outLeft[0] = p[0];
	outLeft[1] = q0;
	outLeft[2] = r0;
	outLeft[3] = s;

	outRight[0] = s;
	outRight[1] = r1;
	outRight[2] = q2;
	outRight[3] = p[3];
}

void mp_graphics_render_stroke_cubic(mp_graphics_context_data* context, vec2 p[4], u32 zIndex, mp_graphics_attributes* attributes)
{
	#define CHECK_SAMPLE_COUNT 5
	f32 checkSamples[CHECK_SAMPLE_COUNT] = {1./6, 2./6, 3./6, 4./6, 5./6};

	vec2 positiveOffsetHull[4];
	vec2 negativeOffsetHull[4];

	mp_graphics_offset_hull(4, p, positiveOffsetHull, 0.5 * attributes->width);
	mp_graphics_offset_hull(4, p, negativeOffsetHull, -0.5 * attributes->width);

	//NOTE(martin): the distance d between the offset curve and the path must be between w/2-tolerance and w/2+tolerance
	//              thus, by constraining tolerance to be at most, 0.5*width, we can rewrite this condition like this:
	//
	//              (w/2-tolerance)^2 < d^2 < (w/2+tolerance)^2
	//
	//		we compute the maximum overshoot outside these bounds and split the curve at the corresponding parameter

	f32 tolerance = minimum(attributes->tolerance, 0.5 * attributes->width);
	f32 d2LowBound = Square(0.5 * attributes->width - attributes->tolerance);
	f32 d2HighBound = Square(0.5 * attributes->width + attributes->tolerance);

	f32 maxOvershoot = 0;
	f32 maxOvershootParameter = 0;

	for(int i=0; i<CHECK_SAMPLE_COUNT; i++)
	{
		f32 t = checkSamples[i];

		vec2 c = mp_graphics_cubic_get_point(p, t);
		vec2 cp =  mp_graphics_cubic_get_point(positiveOffsetHull, t);
		vec2 cn =  mp_graphics_cubic_get_point(negativeOffsetHull, t);

		f32 positiveDistSquare = Square(c.x - cp.x) + Square(c.y - cp.y);
		f32 negativeDistSquare = Square(c.x - cn.x) + Square(c.y - cn.y);

		f32 positiveOvershoot = maximum(positiveDistSquare - d2HighBound, d2LowBound - positiveDistSquare);
		f32 negativeOvershoot = maximum(negativeDistSquare - d2HighBound, d2LowBound - negativeDistSquare);

		f32 overshoot = maximum(positiveOvershoot, negativeOvershoot);

		if(overshoot > maxOvershoot)
		{
			maxOvershoot = overshoot;
			maxOvershootParameter = t;
		}
	}

	if(maxOvershoot > 0)
	{
		//TODO(martin): split at maxErrorParameter and recurse
		vec2 splitLeft[4];
		vec2 splitRight[4];
		mp_graphics_cubic_split(p, maxOvershootParameter, splitLeft, splitRight);
		mp_graphics_render_stroke_cubic(context, splitLeft, zIndex, attributes);
		mp_graphics_render_stroke_cubic(context, splitRight, zIndex, attributes);
	}
	else
	{
		//NOTE(martin): push the actual fill commands for the offset contour

		u32 zIndex = mp_graphics_get_next_z_index(context);

		mp_graphics_render_fill_cubic(context, positiveOffsetHull, zIndex, attributes->color);
		mp_graphics_render_fill_cubic(context, negativeOffsetHull, zIndex, attributes->color);

		//NOTE(martin):	add base triangles
		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 6);


		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, positiveOffsetHull[0]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, positiveOffsetHull[3]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, negativeOffsetHull[3]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, negativeOffsetHull[0]),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		indices[0] = baseIndex + 0;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
		indices[3] = baseIndex + 0;
		indices[4] = baseIndex + 2;
		indices[5] = baseIndex + 3;
	}
	#undef CHECK_SAMPLE_COUNT
}

void mp_graphics_stroke_cap(mp_graphics_context_data* context, vec2 p0, vec2 direction, mp_graphics_attributes* attributes)
{
	//NOTE(martin): compute the tangent and normal vectors (multiplied by half width) at the cap point

	f32 dn = sqrt(Square(direction.x) + Square(direction.y));
	f32 alpha = 0.5 * attributes->width/dn;

	vec2 n0 = {-alpha*direction.y,
		    alpha*direction.x};

	vec2 m0 = {alpha*direction.x,
	           alpha*direction.y};

	u32 zIndex = mp_graphics_get_next_z_index(context);

	u32 baseIndex = mp_graphics_vertices_base_index(context);
	i32* indices = mp_graphics_reserve_indices(context, 6);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p0.x + n0.x, p0.y + n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        attributes->color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p0.x + n0.x + m0.x, p0.y + n0.y + m0.y}),
	                        (vec4){1, 1, 1, 1},
	                        attributes->color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p0.x - n0.x + m0.x, p0.y - n0.y + m0.y}),
	                        (vec4){1, 1, 1, 1},
	                        attributes->color,
	                        zIndex);

	mp_graphics_push_vertex(context,
	                        mp_mat2x3_mul(context->transform, (vec2){p0.x - n0.x, p0.y - n0.y}),
	                        (vec4){1, 1, 1, 1},
	                        attributes->color,
	                        zIndex);

	indices[0] = baseIndex;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;
}

void mp_graphics_stroke_joint(mp_graphics_context_data* context,
                              vec2 p0,
			      vec2 t0,
			      vec2 t1,
			      mp_graphics_attributes* attributes)
{
	//NOTE(martin): compute the normals at the joint point
	f32 norm_t0 = sqrt(Square(t0.x) + Square(t0.y));
	f32 norm_t1 = sqrt(Square(t1.x) + Square(t1.y));

	vec2 n0 = {-t0.y, t0.x};
	n0.x /= norm_t0;
	n0.y /= norm_t0;

	vec2 n1 = {-t1.y, t1.x};
	n1.x /= norm_t1;
	n1.y /= norm_t1;

	//NOTE(martin): the sign of the cross product determines if the normals are facing outwards or inwards the angle.
	//              we flip them to face outwards if needed
	f32 crossZ = n0.x*n1.y - n0.y*n1.x;
	if(crossZ > 0)
	{
		n0.x *= -1;
		n0.y *= -1;
		n1.x *= -1;
		n1.y *= -1;
	}

	u32 zIndex = mp_graphics_get_next_z_index(context);

	//NOTE(martin): use the same code as hull offset to find mitter point...
	/*NOTE(martin): let vector u = (n0+n1) and vector v = pIntersect - p1
		then v = u * (2*offset / norm(u)^2)
		(this can be derived from writing the pythagoras theorems in the triangles of the joint)
	*/
	f32 halfW = 0.5 * attributes->width;
	vec2 u = {n0.x + n1.x, n0.y + n1.y};
	f32 uNormSquare = u.x*u.x + u.y*u.y;
	f32 alpha = attributes->width / uNormSquare;
	vec2 v = {u.x * alpha, u.y * alpha};

	f32 excursionSquare = uNormSquare * Square(alpha - attributes->width/4);

	if(  attributes->joint == MP_GRAPHICS_JOINT_MITER
	  && excursionSquare <= Square(attributes->maxJointExcursion))
	{
		vec2 mitterPoint = {p0.x + v.x, p0.y + v.y};

		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 6);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p0),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
	                        	mp_mat2x3_mul(context->transform, (vec2){p0.x + n0.x*halfW, p0.y + n0.y*halfW}),
	                        	(vec4){1, 1, 1, 1},
	                        	attributes->color,
	                        	zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, mitterPoint),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
	                                mp_mat2x3_mul(context->transform, (vec2){p0.x + n1.x*halfW, p0.y + n1.y*halfW}),
	                                (vec4){1, 1, 1, 1},
	                                attributes->color,
	                                zIndex);

		indices[0] = baseIndex;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
		indices[3] = baseIndex;
		indices[4] = baseIndex + 2;
		indices[5] = baseIndex + 3;
	}
	else
	{
		//NOTE(martin): add a bevel joint
		u32 baseIndex = mp_graphics_vertices_base_index(context);
		i32* indices = mp_graphics_reserve_indices(context, 3);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, p0),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, (vec2){p0.x + n0.x*halfW, p0.y + n0.y*halfW}),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		mp_graphics_push_vertex(context,
		                        mp_mat2x3_mul(context->transform, (vec2){p0.x + n1.x*halfW, p0.y + n1.y*halfW}),
		                        (vec4){1, 1, 1, 1},
		                        attributes->color,
		                        zIndex);

		DEBUG_ASSERT(!isnan(n0.x) && !isnan(n0.y) && !isnan(n1.x) && !isnan(n1.y));

		indices[0] = baseIndex;
		indices[1] = baseIndex + 1;
		indices[2] = baseIndex + 2;
	}
}

void mp_graphics_render_stroke_element(mp_graphics_context_data* context,
                                      mp_graphics_path_elt* element,
				      mp_graphics_attributes* attributes,
				      vec2 currentPoint,
				      vec2* startTangent,
				      vec2* endTangent,
				      vec2* endPoint)
{
	vec2 controlPoints[4] = {currentPoint, element->p[0], element->p[1], element->p[2]};
	int endPointIndex = 0;
	u32 zIndex = mp_graphics_get_next_z_index(context);

	switch(element->type)
	{
		case MP_GRAPHICS_PATH_LINE:
			mp_graphics_render_stroke_line(context, controlPoints, zIndex, attributes);
			endPointIndex = 1;
			break;

		case MP_GRAPHICS_PATH_QUADRATIC:
			mp_graphics_render_stroke_quadratic(context, controlPoints, zIndex, attributes);
			endPointIndex = 2;
			break;

		case MP_GRAPHICS_PATH_CUBIC:
			mp_graphics_render_stroke_cubic(context, controlPoints, zIndex, attributes);
			endPointIndex = 3;
			break;

		case MP_GRAPHICS_PATH_MOVE:
			ASSERT(0, "should be unreachable");
			break;
	}

	*startTangent = (vec2){.x = controlPoints[1].x - controlPoints[0].x,
			      .y = controlPoints[1].y - controlPoints[0].y};

	*endTangent = (vec2){controlPoints[endPointIndex].x - controlPoints[endPointIndex-1].x,
			     controlPoints[endPointIndex].y - controlPoints[endPointIndex-1].y};

	*endPoint = controlPoints[endPointIndex];

}

u32 mp_graphics_render_stroke_subpath(mp_graphics_context_data* context,
                                     mp_graphics_path_elt* elements,
				      mp_graphics_path_descriptor* path,
				      mp_graphics_attributes* attributes,
				      u32 startIndex,
				      vec2 startPoint)
{
	u32 eltCount = path->count;
	DEBUG_ASSERT(startIndex < eltCount);

	vec2 currentPoint = startPoint;
	vec2 endPoint = {0, 0};
	vec2 previousEndTangent = {0, 0};
	vec2 firstTangent = {0, 0};
	vec2 startTangent = {0, 0};
	vec2 endTangent = {0, 0};

	//NOTE(martin): render first element and compute first tangent
	mp_graphics_render_stroke_element(context, elements + startIndex, attributes, currentPoint, &startTangent, &endTangent, &endPoint);

	firstTangent = startTangent;
	previousEndTangent = endTangent;
	currentPoint = endPoint;

	//NOTE(martin): render subsequent elements along with their joints
	u32 eltIndex = startIndex + 1;
	for(;
	    eltIndex<eltCount && elements[eltIndex].type != MP_GRAPHICS_PATH_MOVE;
	    eltIndex++)
	{
		mp_graphics_render_stroke_element(context, elements + eltIndex, attributes, currentPoint, &startTangent, &endTangent, &endPoint);

		if(attributes->joint != MP_GRAPHICS_JOINT_NONE)
		{
			mp_graphics_stroke_joint(context, currentPoint, previousEndTangent, startTangent, attributes);
		}
		previousEndTangent = endTangent;
		currentPoint = endPoint;
	}
	u32 subPathEltCount = eltIndex - (startIndex+1);

	//NOTE(martin): draw end cap / joint. We ensure there's at least two segments to draw a closing joint
	if(  subPathEltCount > 1
	  && startPoint.x == endPoint.x
	  && startPoint.y == endPoint.y)
	{
		if(attributes->joint != MP_GRAPHICS_JOINT_NONE)
		{
			//NOTE(martin): add a closing joint if the path is closed
			mp_graphics_stroke_joint(context, endPoint, endTangent, firstTangent, attributes);
		}
	}
	else if(attributes->cap == MP_GRAPHICS_CAP_SQUARE)
	{
		//NOTE(martin): add start and end cap
		mp_graphics_stroke_cap(context, startPoint, (vec2){-startTangent.x, -startTangent.y}, attributes);
		mp_graphics_stroke_cap(context, endPoint, startTangent, attributes);
	}

	return(eltIndex);
}


void mp_graphics_render_stroke(mp_graphics_context_data* context,
                              mp_graphics_path_elt* elements,
			      mp_graphics_path_descriptor* path,
			      mp_graphics_attributes* attributes)
{
	u32 eltCount = path->count;
	DEBUG_ASSERT(eltCount);

	vec2 startPoint = path->startPoint;
	u32 startIndex = 0;

	while(startIndex < eltCount)
	{
		//NOTE(martin): eliminate leading moves
		while(startIndex < eltCount && elements[startIndex].type == MP_GRAPHICS_PATH_MOVE)
		{
			startPoint = elements[startIndex].p[0];
			startIndex++;
		}

		if(startIndex < eltCount)
		{
			startIndex = mp_graphics_render_stroke_subpath(context, elements, path, attributes, startIndex, startPoint);
		}
	}
}

//-----------------------------------------------------------------------------------------------------------
// Fast shapes primitives
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_render_rectangle_fill(mp_graphics_context_data* context, mp_graphics_rect rect, mp_graphics_attributes* attributes)
{
	u32 baseIndex = mp_graphics_vertices_base_index(context);
	int32* indices = mp_graphics_reserve_indices(context, 6);

	u32 zIndex = mp_graphics_get_next_z_index(context);

	vec2 points[4] = {{rect.x, rect.y},
	                  {rect.x + rect.w, rect.y},
	                  {rect.x + rect.w, rect.y + rect.h},
	                  {rect.x, rect.y + rect.h}};

	vec4 uv = {1, 1, 1, 1};
	for(int i=0; i<4; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, points[i]);
		mp_graphics_push_vertex(context, pos, uv, attributes->color, zIndex);
	}
	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex + 0;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;
}

void mp_graphics_render_rectangle_stroke(mp_graphics_context_data* context, mp_graphics_rect rect, mp_graphics_attributes* attributes)
{
	//NOTE(martin): stroke a rectangle by fill two scaled rectangles with the same zIndex.
	u32 baseIndex = mp_graphics_vertices_base_index(context);
	int32* indices = mp_graphics_reserve_indices(context, 12);

	u32 zIndex = mp_graphics_get_next_z_index(context);

	//NOTE(martin): limit stroke width to the minimum dimension of the rectangle
	f32 width = minimum(attributes->width, minimum(rect.w, rect.h));
	f32 halfW = width/2;

	vec2 outerPoints[4] = {{rect.x - halfW, rect.y - halfW},
	                       {rect.x + rect.w + halfW, rect.y - halfW},
	                       {rect.x + rect.w + halfW, rect.y + rect.h + halfW},
	                       {rect.x - halfW, rect.y + rect.h + halfW}};

	vec2 innerPoints[4] = {{rect.x + halfW, rect.y + halfW},
	                       {rect.x + rect.w - halfW, rect.y + halfW},
	                       {rect.x + rect.w - halfW, rect.y + rect.h - halfW},
	                       {rect.x + halfW, rect.y + rect.h - halfW}};

	vec4 uv = {1, 1, 1, 1};
	for(int i=0; i<4; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, outerPoints[i]);
		mp_graphics_push_vertex(context, pos, uv, attributes->color, zIndex);
	}

	for(int i=0; i<4; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, innerPoints[i]);
		mp_graphics_push_vertex(context, pos, uv, attributes->color, zIndex);
	}

	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex + 0;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;
	indices[6] = baseIndex + 4;
	indices[7] = baseIndex + 5;
	indices[8] = baseIndex + 6;
	indices[9] = baseIndex + 4;
	indices[10] = baseIndex + 6;
	indices[11] = baseIndex + 7;
}

void mp_graphics_render_fill_arc_corner(mp_graphics_context_data* context, f32 x, f32 y, f32 rx, f32 ry, u32 zIndex, mp_graphics_color color)
{
	//NOTE(martin): draw a precomputed arc corner, using a bezier approximation
	u32 baseIndex = mp_graphics_vertices_base_index(context);
	int32* indices = mp_graphics_reserve_indices(context, 6);

	static const vec4 uvs[4] = {{-3.76797, -9.76362, 5.47912, -1},
	                            {-4.19896, -9.45223, 7.534, -1},
	                            {-4.19896, -7.534, 9.45223, -1},
	                            {-3.76797, -5.47912, 9.76362, -1}};

	f32 cx = rx*4*(sqrt(2)-1)/3;
	f32 cy = ry*4*(sqrt(2)-1)/3;

	vec2 points[4] = {{x, y + ry},
	                  {x, y + ry - cy},
	                  {x + rx - cx, y},
	                  {x + rx, y}};

	for(int i=0; i<4; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, points[i]);
		mp_graphics_push_vertex(context, pos, uvs[i], color, zIndex);
	}
	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex + 0;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;
}

void mp_graphics_render_rounded_rectangle_fill_with_z_index(mp_graphics_context_data* context,
							   mp_graphics_rounded_rect rect,
							   mp_graphics_attributes* attributes,
							   u32 zIndex)
{
	//NOTE(martin): draw a rounded rectangle by drawing a normal rectangle and 4 corners,
	//              approximating an arc by a precomputed bezier curve

	u32 baseIndex = mp_graphics_vertices_base_index(context);
	int32* indices = mp_graphics_reserve_indices(context, 18);

	//NOTE(martin): inner cutted corner rectangle
	vec2 points[8] = {{rect.x + rect.r, rect.y},
	                  {rect.x + rect.w - rect.r, rect.y},
	                  {rect.x + rect.w, rect.y + rect.r},
	                  {rect.x + rect.w, rect.y + rect.h - rect.r},
	                  {rect.x + rect.w - rect.r, rect.y + rect.h},
	                  {rect.x + rect.r, rect.y + rect.h},
	                  {rect.x, rect.y + rect.h - rect.r},
	                  {rect.x, rect.y + rect.r}};

	vec4 uv = {1, 1, 1, 1};

	for(int i=0; i<8; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, points[i]);
		mp_graphics_push_vertex(context, pos, uv, attributes->color, zIndex);
	}

	static const i32 fanIndices[18] = { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7 }; // inner fan
	for(int i=0; i<18; i++)
	{
		indices[i] = fanIndices[i] + baseIndex;
	}

	mp_graphics_render_fill_arc_corner(context, rect.x, rect.y, rect.r, rect.r, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x + rect.w, rect.y, -rect.r, rect.r, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x + rect.w, rect.y + rect.h, -rect.r, -rect.r, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x, rect.y + rect.h, rect.r, -rect.r, zIndex, attributes->color);
}


void mp_graphics_render_rounded_rectangle_fill(mp_graphics_context_data* context,
                                              mp_graphics_rounded_rect rect,
					      mp_graphics_attributes* attributes)
{
	u32 zIndex = mp_graphics_get_next_z_index(context);
	mp_graphics_render_rounded_rectangle_fill_with_z_index(context, rect, attributes, zIndex);
}

void mp_graphics_render_rounded_rectangle_stroke(mp_graphics_context_data* context,
						mp_graphics_rounded_rect rect,
						mp_graphics_attributes* attributes)
{
	//NOTE(martin): stroke rounded rectangle by filling two scaled rounded rectangles with the same zIndex
	f32 width = minimum(attributes->width, minimum(rect.w, rect.h));
	f32 halfW = width/2;

	mp_graphics_rounded_rect inner = {rect.x + halfW, rect.y + halfW, rect.w - width, rect.h - width, rect.r - halfW};
	mp_graphics_rounded_rect outer = {rect.x - halfW, rect.y - halfW, rect.w + width, rect.h + width, rect.r + halfW};

	u32 zIndex = mp_graphics_get_next_z_index(context);
	mp_graphics_render_rounded_rectangle_fill_with_z_index(context, outer, attributes, zIndex);
	mp_graphics_render_rounded_rectangle_fill_with_z_index(context, inner, attributes, zIndex);
}

void mp_graphics_render_ellipse_fill_with_z_index(mp_graphics_context_data* context,
                                                 mp_graphics_rect rect,
						 mp_graphics_attributes* attributes,
						 u32 zIndex)
{
	//NOTE(martin): draw a filled ellipse by drawing a diamond and 4 corners,
	//              approximating an arc by a precomputed bezier curve

	u32 baseIndex = mp_graphics_vertices_base_index(context);
	int32* indices = mp_graphics_reserve_indices(context, 6);

	f32 rx = rect.w/2;
	f32 ry = rect.h/2;

	//NOTE(martin): inner diamond
	vec2 points[4] = {{rect.x, rect.y + ry},
	                  {rect.x + rx, rect.y},
	                  {rect.x + rect.w, rect.y + ry},
	                  {rect.x + rx, rect.y + rect.h}};

	vec4 uv = {1, 1, 1, 1};

	for(int i=0; i<4; i++)
	{
		vec2 pos = mp_mat2x3_mul(context->transform, points[i]);
		mp_graphics_push_vertex(context, pos, uv, attributes->color, zIndex);
	}

	indices[0] = baseIndex + 0;
	indices[1] = baseIndex + 1;
	indices[2] = baseIndex + 2;
	indices[3] = baseIndex + 0;
	indices[4] = baseIndex + 2;
	indices[5] = baseIndex + 3;

	mp_graphics_render_fill_arc_corner(context, rect.x, rect.y, rx, ry, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x + rect.w, rect.y, -rx, ry, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x + rect.w, rect.y + rect.h, -rx, -ry, zIndex, attributes->color);
	mp_graphics_render_fill_arc_corner(context, rect.x, rect.y + rect.h, rx, -ry, zIndex, attributes->color);
}

void mp_graphics_render_ellipse_fill(mp_graphics_context_data* context, mp_graphics_rect rect, mp_graphics_attributes* attributes)
{
	u32 zIndex = mp_graphics_get_next_z_index(context);
	mp_graphics_render_ellipse_fill_with_z_index(context, rect, attributes, zIndex);
}

void mp_graphics_render_ellipse_stroke(mp_graphics_context_data* context, mp_graphics_rect rect, mp_graphics_attributes* attributes)
{
	//NOTE(martin): stroke by filling two scaled ellipsis with the same zIndex
	f32 width = minimum(attributes->width, minimum(rect.w, rect.h));
	f32 halfW = width/2;

	mp_graphics_rect inner = {rect.x + halfW, rect.y + halfW, rect.w - width, rect.h - width};
	mp_graphics_rect outer = {rect.x - halfW, rect.y - halfW, rect.w + width, rect.h + width};

	u32 zIndex = mp_graphics_get_next_z_index(context);
	mp_graphics_render_ellipse_fill_with_z_index(context, outer, attributes, zIndex);
	mp_graphics_render_ellipse_fill_with_z_index(context, inner, attributes, zIndex);
}

//-----------------------------------------------------------------------------------------------------------
// Graphics context lifetime
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_context_reset_streams(mp_graphics_context_data* context)
{
	ListInit(&context->streamsFreeList);
	for(int i=0; i<MP_GRAPHICS_STREAM_MAX_COUNT; i++)
	{
		ListAppend(&context->streamsFreeList, &context->streams[i].freeListElt);
		context->streams[i].generation = 1;
	}
	context->currentStream = mp_graphics_stream_alloc(context);
}

mp_graphics_context mp_graphics_context_create()
{
	mp_graphics_context_data* context = mp_graphics_context_alloc();
	if(!context)
	{
		return(mp_graphics_context_null_handle());
	}
	context->path.startIndex = 0;
	context->path.count = 0;
	context->nextZIndex = 1;
	context->attributes.clearColor = (mp_graphics_color){1, 1, 1, 1};
	context->attributes.color = (mp_graphics_color){0, 0, 0, 1};
	context->attributes.tolerance = 1;
	context->attributes.width = 10;
	context->attributes.clip = (mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX};

	context->transform = (mp_mat2x3){{1, 0, 0,
	                                  0, 1, 0}};

	context->fontsNextIndex = 0;
	ListInit(&context->fontsFreeList);

	mp_graphics_context_reset_streams(context);

	return(mp_graphics_context_handle_from_ptr(context));
}

void mp_graphics_context_destroy(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_context_recycle(context);
}

void mp_graphics_context_begin_draw(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->vertexCount = 0;
	context->indexCount = 0;
	context->path.startIndex = 0;
	context->path.count = 0;
}

mp_mat2x3 mp_graphics_matrix_stack_top(mp_graphics_context_data* context)
{
	if(context->matrixStackSize == 0)
	{
		return((mp_mat2x3){1, 0, 0,
				   0, 1, 0});
	}
	else
	{
		return(context->matrixStack[context->matrixStackSize-1]);
	}
}

void mp_graphics_matrix_stack_push(mp_graphics_context_data* context, mp_mat2x3 transform)
{
	if(context->matrixStackSize >= MP_GRAPHICS_MATRIX_STACK_MAX_DEPTH)
	{
		LOG_ERROR("matrix stack overflow\n");
	}
	else
	{
		context->matrixStack[context->matrixStackSize] = transform;
		context->matrixStackSize++;
		context->transform = transform;
	}
}

void mp_graphics_matrix_stack_pop(mp_graphics_context_data* context)
{
	if(context->matrixStackSize == 0)
	{
		LOG_ERROR("matrix stack underflow\n");
	}
	else
	{
		context->matrixStackSize--;
		context->transform = mp_graphics_matrix_stack_top(context);
	}
}


mp_aligned_rect mp_graphics_clip_stack_top(mp_graphics_context_data* context)
{
	if(context->clipStackSize == 0)
	{
		return((mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX});
	}
	else
	{
		return(context->clipStack[context->clipStackSize-1]);
	}
}

void mp_graphics_clip_stack_push(mp_graphics_context_data* context, mp_aligned_rect clip)
{
	if(context->clipStackSize >= MP_GRAPHICS_CLIP_STACK_MAX_DEPTH)
	{
		LOG_ERROR("clip stack overflow\n");
	}
	else
	{
		context->clipStack[context->clipStackSize] = clip;
		context->clipStackSize++;
		context->clip = clip;
	}
}

void mp_graphics_clip_stack_pop(mp_graphics_context_data* context)
{
	if(context->clipStackSize == 0)
	{
		LOG_ERROR("clip stack underflow\n");
	}
	else
	{
		context->clipStackSize--;
		context->clip = mp_graphics_clip_stack_top(context);
	}
}

void mp_graphics_do_clip_push(mp_graphics_context_data* context, mp_aligned_rect clip)
{
	//NOTE(martin): transform clip
	vec2 p0 = mp_mat2x3_mul(context->transform, (vec2){clip.x, clip.y});
	vec2 p1 = mp_mat2x3_mul(context->transform, (vec2){clip.x + clip.w, clip.y});
	vec2 p2 = mp_mat2x3_mul(context->transform, (vec2){clip.x + clip.w, clip.y + clip.h});
	vec2 p3 = mp_mat2x3_mul(context->transform, (vec2){clip.x, clip.y + clip.h});

	f32 x0 = minimum(p0.x, minimum(p1.x, minimum(p2.x, p3.x)));
	f32 y0 = minimum(p0.y, minimum(p1.y, minimum(p2.y, p3.y)));
	f32 x1 = maximum(p0.x, maximum(p1.x, maximum(p2.x, p3.x)));
	f32 y1 = maximum(p0.y, maximum(p1.y, maximum(p2.y, p3.y)));

	mp_aligned_rect current = mp_graphics_clip_stack_top(context);

	//NOTE(martin): intersect with current clip
	x0 = maximum(current.x, x0);
	y0 = maximum(current.y, y0);
	x1 = minimum(current.x + current.w, x1);
	y1 = minimum(current.y + current.h, y1);

	mp_aligned_rect r = {x0, y0, maximum(0, x1-x0), maximum(0, y1-y0)};
	mp_graphics_clip_stack_push(context, r);
}

void mp_graphics_context_flush(mp_graphics_context contextHandle, mp_graphics_surface surfaceHandle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(contextHandle);
	if(!context)
	{
		return;
	}
	mp_graphics_surface_data* surface = mp_graphics_surface_ptr_from_handle(surfaceHandle);
	if(!surface)
	{
		return;
	}

	context->vertexLayout = surface->get_vertex_layout(surface);

	mp_graphics_color clearColor = {0, 0, 0, 1};

	u32 count = context->primitiveCount;

	mp_graphics_stream_data* stream = context->currentStream;
	DEBUG_ASSERT(stream);
	DEBUG_ASSERT(stream->count <= count);

	u32 nextIndex = stream->firstCommand;

	mp_graphics_reset_z_index(context);
	context->transform = (mp_mat2x3){1, 0, 0,
	                                 0, 1, 0};
	context->clip = (mp_aligned_rect){-FLT_MAX/2, -FLT_MAX/2, FLT_MAX, FLT_MAX};

	for(int i=0; i<stream->count; i++)
	{
		if(nextIndex >= count)
		{
			LOG_ERROR("invalid location '%i' in graphics command buffer would cause an overrun\n", nextIndex);
			break;
		}
		mp_graphics_primitive* primitive = &(context->primitives[nextIndex]);
		nextIndex++;

		switch(primitive->cmd)
		{
			case MP_GRAPHICS_CMD_CLEAR:
			{
				//NOTE(martin): clear buffers
				context->vertexCount = 0;
				context->indexCount = 0;

				clearColor = primitive->attributes.clearColor;
			} break;

			case MP_GRAPHICS_CMD_FILL:
			{
				u32 zIndex = mp_graphics_get_next_z_index(context);
				mp_graphics_render_fill(context,
						       context->pathElements + primitive->path.startIndex,
						       &primitive->path,
						       zIndex,
						       primitive->attributes.color);
			} break;

			case MP_GRAPHICS_CMD_STROKE:
			{
				mp_graphics_render_stroke(context,
							 context->pathElements + primitive->path.startIndex,
							 &primitive->path,
							 &primitive->attributes);
			} break;


			case MP_GRAPHICS_CMD_RECT_FILL:
				mp_graphics_render_rectangle_fill(context, primitive->rect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_RECT_STROKE:
				mp_graphics_render_rectangle_stroke(context, primitive->rect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_ROUND_RECT_FILL:
				mp_graphics_render_rounded_rectangle_fill(context, primitive->roundedRect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_ROUND_RECT_STROKE:
				mp_graphics_render_rounded_rectangle_stroke(context, primitive->roundedRect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_ELLIPSE_FILL:
				mp_graphics_render_ellipse_fill(context, primitive->rect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_ELLIPSE_STROKE:
				mp_graphics_render_ellipse_stroke(context, primitive->rect, &primitive->attributes);
				break;

			case MP_GRAPHICS_CMD_JUMP:
			{
				if(primitive->jump == ~0)
				{
					//NOTE(martin): normal end of stream marker
					goto exit_command_loop;
				}
				else if(primitive->jump >= count)
				{
					LOG_ERROR("invalid jump location '%i' in graphics command buffer\n", primitive->jump);
					goto exit_command_loop;
				}
				else
				{
					nextIndex = primitive->jump;
				}
			} break;

			case MP_GRAPHICS_CMD_MATRIX_PUSH:
			{
				mp_mat2x3 transform = mp_graphics_matrix_stack_top(context);
				mp_graphics_matrix_stack_push(context, mp_mat2x3_mul_m(transform, primitive->matrix));
			} break;

			case MP_GRAPHICS_CMD_MATRIX_POP:
			{
				mp_graphics_matrix_stack_pop(context);
			} break;

			case MP_GRAPHICS_CMD_CLIP_PUSH:
			{
				//TODO(martin): use only aligned rect and avoid this
				mp_aligned_rect r = {primitive->rect.x, primitive->rect.y, primitive->rect.w, primitive->rect.h};
				mp_graphics_do_clip_push(context, r);
			} break;

			case MP_GRAPHICS_CMD_CLIP_POP:
			{
				mp_graphics_clip_stack_pop(context);
			} break;
		}
	}
	exit_command_loop: ;

	surface->draw_buffers(surface, context->vertexCount, context->indexCount, clearColor);

	//NOTE(martin): clear buffers
	context->primitiveCount = 0;
	context->vertexCount = 0;
	context->indexCount = 0;

	context->path.startIndex = 0;
	context->path.count = 0;

	//NOTE(martin): invalidate all streams
	context->frameCounter++;
	mp_graphics_context_reset_streams(context);
}

void mp_graphics_push_command(mp_graphics_context_data* context, mp_graphics_primitive primitive)
{
	//NOTE(martin): push primitive and updates current stream, eventually patching a pending jump.
	ASSERT(context->primitiveCount < MP_GRAPHICS_MAX_PRIMITIVE_COUNT);
	context->primitives[context->primitiveCount] = primitive;
	context->primitiveCount++;

	mp_graphics_stream_data* stream = context->currentStream;
	DEBUG_ASSERT(stream);

	u32 lastCommand = context->primitiveCount-1;
	if(stream->pendingJump)
	{
		DEBUG_ASSERT(context->primitives[stream->lastCommand].cmd == MP_GRAPHICS_CMD_JUMP);
		context->primitives[stream->lastCommand].jump = lastCommand;
		stream->pendingJump = false;
	}
	stream->lastCommand = lastCommand;
	stream->count++;
}

//-----------------------------------------------------------------------------------------------------------
// Graphics command streams
//-----------------------------------------------------------------------------------------------------------

mp_graphics_stream mp_graphics_stream_create(mp_graphics_context contextHandle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(contextHandle);
	if(!context)
	{
		return(mp_graphics_stream_null_handle());
	}

	mp_graphics_stream_data* stream = mp_graphics_stream_alloc(context);
	if(!stream)
	{
		return(mp_graphics_stream_null_handle());
	}

	return(mp_graphics_stream_handle_from_ptr(context, stream));
}

mp_graphics_stream mp_graphics_stream_swap(mp_graphics_context contextHandle, mp_graphics_stream streamHandle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(contextHandle);
	if(!context)
	{
		return(mp_graphics_stream_null_handle());
	}
	mp_graphics_stream_data* stream = mp_graphics_stream_ptr_from_handle(context, streamHandle);
	if(!stream)
	{
		return(mp_graphics_stream_null_handle());
	}

	if(stream == context->currentStream)
	{
		//NOTE(martin): prevent swapping for itself
		return(mp_graphics_stream_null_handle());
	}

	//NOTE(martin): push an unitialized jump to the current stream
	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_JUMP, .jump = ~0});
	context->currentStream->pendingJump = true;

	//NOTE(martin): set the new current stream
	mp_graphics_stream_data* oldStream = context->currentStream;
	context->currentStream = stream;

	if(!stream->count)
	{
		//NOTE(martin): if stream is new, set the first command to the current offset in command buffer
		stream->firstCommand = context->primitiveCount;
		stream->lastCommand = stream->firstCommand;
	}
	else
	{
		//NOTE(martin): else, we just make sure there is a pending jump to be patched when we push the next command
		DEBUG_ASSERT(stream->pendingJump);
	}

	return(mp_graphics_stream_handle_from_ptr(context, oldStream));
}

void mp_graphics_stream_append(mp_graphics_context contextHandle, mp_graphics_stream streamHandle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(contextHandle);
	if(!context)
	{
		return;
	}
	mp_graphics_stream_data* stream = mp_graphics_stream_ptr_from_handle(context, streamHandle);
	if(!stream)
	{
		return;
	}
	if(stream == context->currentStream)
	{
		//NOTE(martin): prevent appending to itself
		return;
	}

	if(stream->count)
	{
		//NOTE(martin): push a jump set to the appended stream's first command
		mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_JUMP, .jump = stream->firstCommand});

		//NOTE(martin): update current stream last command and make sure it's a jump
		context->currentStream->lastCommand = stream->lastCommand;
		context->currentStream->count += stream->count;
		context->currentStream->pendingJump = true;

		DEBUG_ASSERT(context->primitives[context->currentStream->lastCommand].cmd == MP_GRAPHICS_CMD_JUMP);
	}

	//NOTE(martin): invalidate appended stream
	mp_graphics_stream_recycle(context, stream);
}

//-----------------------------------------------------------------------------------------------------------
// Fonts management
//-----------------------------------------------------------------------------------------------------------

mp_graphics_font mp_graphics_font_create_from_memory(mp_graphics_context handle, u32 size, byte* buffer, u32 rangeCount, unicode_range* ranges)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		LOG_ERROR("invalid graphics context\n");
		return((mp_graphics_font){0});
	}
	mp_graphics_font_info* fontInfo = ListPopEntry(&context->fontsFreeList, mp_graphics_font_info, freeListElt);
	if(!fontInfo)
	{
		if(context->fontsNextIndex < MP_GRAPHICS_FONT_MAX_COUNT)
		{
			fontInfo = &(context->fonts[context->fontsNextIndex]);
			context->fontsNextIndex++;
			memset(fontInfo, 0, sizeof(*fontInfo));
			fontInfo->generation = 1;
		}
		else
		{
			LOG_ERROR("can't allocate new font\n");
			return((mp_graphics_font){0});
		}
	}

	stbtt_fontinfo stbttFontInfo;
	stbtt_InitFont(&stbttFontInfo, buffer, 0);

	//NOTE(martin): load font metrics data
	fontInfo->unitsPerEm = 1./stbtt_ScaleForMappingEmToPixels(&stbttFontInfo, 1);

	int ascent, descent, lineGap, x0, x1, y0, y1;
	stbtt_GetFontVMetrics(&stbttFontInfo, &ascent, &descent, &lineGap);
	stbtt_GetFontBoundingBox(&stbttFontInfo, &x0, &y0, &x1, &y1);

	fontInfo->extents.ascent = ascent;
	fontInfo->extents.descent = -descent;
	fontInfo->extents.leading = lineGap;
	fontInfo->extents.width = x1 - x0;

	stbtt_GetCodepointBox(&stbttFontInfo, 'x', &x0, &y0, &x1, &y1);
	fontInfo->extents.xHeight = y1 - y0;

	stbtt_GetCodepointBox(&stbttFontInfo, 'M', &x0, &y0, &x1, &y1);
	fontInfo->extents.capHeight = y1 - y0;

	//NOTE(martin): load codepoint ranges
	fontInfo->rangeCount = rangeCount;
	fontInfo->glyphMap = malloc_array(mp_graphics_glyph_map_entry, rangeCount);
	fontInfo->glyphCount = 0;

	for(int i=0; i<rangeCount; i++)
	{
		//NOTE(martin): initialize the map entry.
		//              The glyph indices are offseted by 1, to reserve 0 as an invalid glyph index.
		fontInfo->glyphMap[i].range = ranges[i];
		fontInfo->glyphMap[i].firstGlyphIndex = fontInfo->glyphCount + 1;
		fontInfo->glyphCount += ranges[i].count;
	}

	fontInfo->glyphs = malloc_array(mp_graphics_glyph_info, fontInfo->glyphCount);

	//NOTE(martin): first do a count of outlines
	int outlineCount = 0;
	for(int rangeIndex=0; rangeIndex<rangeCount; rangeIndex++)
	{
		utf32 codePoint = fontInfo->glyphMap[rangeIndex].range.firstCodePoint;
		u32 firstGlyphIndex = fontInfo->glyphMap[rangeIndex].firstGlyphIndex;
		u32 endGlyphIndex = firstGlyphIndex + fontInfo->glyphMap[rangeIndex].range.count;

		for(int glyphIndex = firstGlyphIndex;
		    glyphIndex < endGlyphIndex; glyphIndex++)
		{
			int stbttGlyphIndex = stbtt_FindGlyphIndex(&stbttFontInfo, codePoint);
			if(stbttGlyphIndex == 0)
			{
				//NOTE(martin): the codepoint is not found in the font
				codePoint++;
				continue;
			}
			//NOTE(martin): load glyph outlines
			stbtt_vertex* vertices = 0;
			outlineCount += stbtt_GetGlyphShape(&stbttFontInfo, stbttGlyphIndex, &vertices);
			stbtt_FreeShape(&stbttFontInfo, vertices);
			codePoint++;
		}
	}
	//NOTE(martin): allocate outlines
	fontInfo->outlines = malloc_array(mp_graphics_path_elt, outlineCount);
	fontInfo->outlineCount = 0;

	//NOTE(martin): load metrics and outlines
	for(int rangeIndex=0; rangeIndex<rangeCount; rangeIndex++)
	{
		utf32 codePoint = fontInfo->glyphMap[rangeIndex].range.firstCodePoint;
		u32 firstGlyphIndex = fontInfo->glyphMap[rangeIndex].firstGlyphIndex;
		u32 endGlyphIndex = firstGlyphIndex + fontInfo->glyphMap[rangeIndex].range.count;

		for(int glyphIndex = firstGlyphIndex;
		    glyphIndex < endGlyphIndex; glyphIndex++)
		{
			mp_graphics_glyph_info* glyph = &(fontInfo->glyphs[glyphIndex-1]);

			int stbttGlyphIndex = stbtt_FindGlyphIndex(&stbttFontInfo, codePoint);
			if(stbttGlyphIndex == 0)
			{
				//NOTE(martin): the codepoint is not found in the font, we zero the glyph info
				memset(glyph, 0, sizeof(*glyph));
				codePoint++;
				continue;
			}

			glyph->exists = true;
			glyph->codePoint = codePoint;

			//NOTE(martin): load glyph metric
			int xAdvance, xBearing, x0, y0, x1, y1;
			stbtt_GetGlyphHMetrics(&stbttFontInfo, stbttGlyphIndex, &xAdvance, &xBearing);
			stbtt_GetGlyphBox(&stbttFontInfo, stbttGlyphIndex, &x0, &y0, &x1, &y1);

			glyph->extents.xAdvance	= (f32)xAdvance;
			glyph->extents.yAdvance = 0;
			glyph->extents.xBearing = (f32)xBearing;
			glyph->extents.yBearing = y0;

			glyph->extents.width = x1 - x0;
			glyph->extents.height = y1 - y0;

			//NOTE(martin): load glyph outlines

			stbtt_vertex* vertices = 0;
			int vertexCount = stbtt_GetGlyphShape(&stbttFontInfo, stbttGlyphIndex, &vertices);

			glyph->pathDescriptor = (mp_graphics_path_descriptor){.startIndex = fontInfo->outlineCount,
			                                                      .count = vertexCount,
									      .startPoint = {0, 0}};

			mp_graphics_path_elt* elements = fontInfo->outlines + fontInfo->outlineCount;
			fontInfo->outlineCount += vertexCount;
			vec2 currentPos = {0, 0};

			for(int vertIndex = 0; vertIndex < vertexCount; vertIndex++)
			{
				f32 x = vertices[vertIndex].x;
				f32 y = vertices[vertIndex].y;
				f32 cx = vertices[vertIndex].cx;
				f32 cy = vertices[vertIndex].cy;
				f32 cx1 = vertices[vertIndex].cx1;
				f32 cy1 = vertices[vertIndex].cy1;

				switch(vertices[vertIndex].type)
				{
					case STBTT_vmove:
						elements[vertIndex].type = MP_GRAPHICS_PATH_MOVE;
						elements[vertIndex].p[0] = (vec2){x, y};
						break;

					case STBTT_vline:
						elements[vertIndex].type = MP_GRAPHICS_PATH_LINE;
						elements[vertIndex].p[0] = (vec2){x, y};
						break;

					case STBTT_vcurve:
					{
						elements[vertIndex].type = MP_GRAPHICS_PATH_QUADRATIC;
						elements[vertIndex].p[0] = (vec2){cx, cy};
						elements[vertIndex].p[1] = (vec2){x, y};
					} break;

					case STBTT_vcubic:
						elements[vertIndex].type = MP_GRAPHICS_PATH_CUBIC;
						elements[vertIndex].p[0] = (vec2){cx, cy};
						elements[vertIndex].p[1] = (vec2){cx1, cy1};
						elements[vertIndex].p[2] = (vec2){x, y};
						break;
				}
				currentPos = (vec2){x, y};
			}
			stbtt_FreeShape(&stbttFontInfo, vertices);
			codePoint++;
		}
	}
	u64 h = ((u64)(fontInfo - context->fonts))<<32 | ((u64)fontInfo->generation);
	return((mp_graphics_font){h});
}

mp_graphics_font_info* mp_graphics_get_font_info(mp_graphics_context_data* context, mp_graphics_font fontHandle)
{
	u32 fontIndex = (u32)(fontHandle.h >> 32);
	u32 fontGeneration = (u32)(fontHandle.h & 0xffffffff);

	if(  fontIndex >= MP_GRAPHICS_FONT_MAX_COUNT
	  || fontIndex >= context->fontsNextIndex
	  || fontGeneration != context->fonts[fontIndex].generation)
	{
		LOG_WARNING("invalid font handle\n");
		return(0);
	}
	return(&(context->fonts[fontIndex]));
}

void mp_graphics_font_destroy(mp_graphics_context handle, mp_graphics_font fontHandle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		LOG_ERROR("invalid graphics context\n");
		return;
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, fontHandle);
	if(!fontInfo)
	{
		return;
	}
	#ifdef DEBUG
		if(fontInfo->generation == UINT32_MAX)
		{
			LOG_ERROR("font info generation wrap around\n");
		}
	#endif
	fontInfo->generation++;

	free(fontInfo->glyphMap);
	free(fontInfo->glyphs);
	free(fontInfo->outlines);

	ListPush(&context->fontsFreeList, &fontInfo->freeListElt);
}

mp_string32 mp_graphics_font_get_glyph_indices_from_font_info(mp_graphics_font_info* fontInfo, mp_string32 codePoints, mp_string32 backing)
{
	u64 count = minimum(codePoints.len, backing.len);

	for(int i = 0; i<count; i++)
	{
		u32 glyphIndex = 0;
		for(int rangeIndex=0; rangeIndex < fontInfo->rangeCount; rangeIndex++)
		{
			if(codePoints.ptr[i] >= fontInfo->glyphMap[rangeIndex].range.firstCodePoint
			  && codePoints.ptr[i] < (fontInfo->glyphMap[rangeIndex].range.firstCodePoint + fontInfo->glyphMap[rangeIndex].range.count))
			{
				u32 rangeOffset = codePoints.ptr[i] - fontInfo->glyphMap[rangeIndex].range.firstCodePoint;
				glyphIndex = fontInfo->glyphMap[rangeIndex].firstGlyphIndex + rangeOffset;
				break;
			}
		}
		if(glyphIndex && !fontInfo->glyphs[glyphIndex].exists)
		{
			backing.ptr[i] = 0;
		}
		backing.ptr[i] = glyphIndex;
	}
	mp_string32 res = {.len = count, .ptr = backing.ptr};
	return(res);
}

u32 mp_graphics_font_get_glyph_index_from_font_info(mp_graphics_font_info* fontInfo, utf32 codePoint)
{
	u32 glyphIndex = 0;
	mp_string32 codePoints = {1, &codePoint};
	mp_string32 backing = {1, &glyphIndex};
	mp_graphics_font_get_glyph_indices_from_font_info(fontInfo, codePoints, backing);
	return(glyphIndex);
}

mp_string32 mp_graphics_font_get_glyph_indices(mp_graphics_context handle, mp_graphics_font font, mp_string32 codePoints, mp_string32 backing)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return((mp_string32){});
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, font);
	if(!fontInfo)
	{
		return((mp_string32){});
	}
	return(mp_graphics_font_get_glyph_indices_from_font_info(fontInfo, codePoints, backing));
}

mp_string32 mp_graphics_font_push_glyph_indices(mp_graphics_context handle, mp_graphics_font font, mem_arena* arena, mp_string32 codePoints)
{
	u32* buffer = mem_arena_alloc_array(arena, u32, codePoints.len);
	mp_string32 backing = {codePoints.len, buffer};
	return(mp_graphics_font_get_glyph_indices(handle, font, codePoints, backing));
}

u32 mp_graphics_font_get_glyph_index(mp_graphics_context handle, mp_graphics_font font, utf32 codePoint)
{
	u32 glyphIndex = 0;
	mp_string32 codePoints = {1, &codePoint};
	mp_string32 backing = {1, &glyphIndex};
	mp_graphics_font_get_glyph_indices(handle, font, codePoints, backing);
	return(glyphIndex);
}

mp_graphics_glyph_info* mp_graphics_font_get_glyph_info(mp_graphics_font_info* fontInfo, u32 glyphIndex)
{
	DEBUG_ASSERT(glyphIndex);
	DEBUG_ASSERT(glyphIndex < fontInfo->glyphCount);
	return(&(fontInfo->glyphs[glyphIndex-1]));
}

int mp_graphics_font_get_extents(mp_graphics_context handle, mp_graphics_font font, mp_graphics_font_extents* outExtents)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return(-1);
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, font);
	if(!fontInfo)
	{
		return(-1);
	}
	*outExtents = fontInfo->extents;
	return(0);
}

f32 mp_graphics_font_get_scale_for_em_pixels(mp_graphics_context handle, mp_graphics_font font, f32 emSize)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return(0);
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, font);
	if(!fontInfo)
	{
		return(0);
	}
	return(emSize/fontInfo->unitsPerEm);
}

void mp_graphics_font_get_glyph_extents_from_font_info(mp_graphics_font_info* fontInfo,
                                                       mp_string32 glyphIndices,
                                                       mp_graphics_text_extents* outExtents)
{
	for(int i=0; i<glyphIndices.len; i++)
	{
		if(!glyphIndices.ptr[i] || glyphIndices.ptr[i] >= fontInfo->glyphCount)
		{
			continue;
		}
		mp_graphics_glyph_info* glyph = mp_graphics_font_get_glyph_info(fontInfo, glyphIndices.ptr[i]);
		outExtents[i] = glyph->extents;
	}
}

int mp_graphics_font_get_glyph_extents(mp_graphics_context handle,
                                       mp_graphics_font font,
                                       mp_string32 glyphIndices,
                                       mp_graphics_text_extents* outExtents)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return(-1);
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, font);
	if(!fontInfo)
	{
		return(-1);
	}
	mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, glyphIndices, outExtents);
	return(0);
}

int mp_graphics_font_get_codepoint_extents(mp_graphics_context handle,
                                           mp_graphics_font font,
					   utf32 codePoint,
					   mp_graphics_text_extents* outExtents)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return(-1);
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, font);
	if(!fontInfo)
	{
		return(-1);
	}
	u32 glyphIndex = 0;
	mp_string32 codePoints = {1, &codePoint};
	mp_string32 backing = {1, &glyphIndex};
	mp_string32 glyphs = mp_graphics_font_get_glyph_indices_from_font_info(fontInfo, codePoints, backing);
	mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, glyphs, outExtents);
	return(0);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TODO: Deprecate this function and remove it from gui
////////////////////////////////////////////////////////////////////////////////////////////////////////////
vec2 mp_graphics_get_glyphs_dimensions(mp_graphics_context handle, mp_string32 glyphIndices)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return((vec2){});
	}
	if(!glyphIndices.len)
	{
		return((vec2){});
	}

	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, context->attributes.font);
	if(!fontInfo)
	{
		return((vec2){});
	}

	//NOTE(martin): find width of missing character
	//TODO(martin): should cache that at font creation...
	mp_graphics_text_extents missingGlyphExtents;
	u32 missingGlyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 0xfffd);

	if(missingGlyphIndex)
	{
		mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, (mp_string32){1, &missingGlyphIndex}, &missingGlyphExtents);
	}
	else
	{
		//NOTE(martin): could not find replacement glyph, try to get an 'x' to get a somewhat correct width
		//              to render an empty rectangle. Otherwise just render with the max font width
		f32 boxWidth = fontInfo->extents.width * 0.8;
		f32 xBearing = fontInfo->extents.width * 0.1;
		f32 xAdvance = fontInfo->extents.width;

		missingGlyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 'x');
		if(missingGlyphIndex)
		{
			mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, (mp_string32){1, &missingGlyphIndex}, &missingGlyphExtents);
		}
		else
		{
			missingGlyphExtents.xBearing = fontInfo->extents.width * 0.1;
			missingGlyphExtents.yBearing = 0;
			missingGlyphExtents.width = fontInfo->extents.width * 0.8;
			missingGlyphExtents.xAdvance = fontInfo->extents.width;
			missingGlyphExtents.yAdvance = 0;
		}
	}

	//NOTE(martin): accumulate text extents
	f32 width = 0;
	f32 x = 0;
	f32 y = 0;
	f32 lineHeight = fontInfo->extents.descent + fontInfo->extents.ascent;

	for(int i=0; i<glyphIndices.len; i++)
	{
		//TODO(martin): make it failsafe for fonts that don't have a glyph for the line-feed codepoint ?

		mp_graphics_glyph_info* glyph = 0;
		mp_graphics_text_extents extents;
		if(!glyphIndices.ptr[i] || glyphIndices.ptr[i] >= fontInfo->glyphCount)
		{
			extents = missingGlyphExtents;
		}
		else
		{
			glyph = mp_graphics_font_get_glyph_info(fontInfo, glyphIndices.ptr[i]);
			extents = glyph->extents;
		}
		x += extents.xAdvance;
		y += extents.yAdvance;

		if(glyph && glyph->codePoint == '\n')
		{
			width = maximum(width, x);
			x = 0;
			y -= lineHeight + fontInfo->extents.leading;
		}
	}
	width = maximum(width, x);

	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(handle, context->attributes.font, context->attributes.fontSize);
	return((vec2){width * fontScale, (-y + lineHeight) * fontScale});
}

mp_aligned_rect mp_graphics_text_bounding_box(mp_graphics_context handle, mp_string text)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return((mp_aligned_rect){});
	}
	if(!text.len || !text.ptr)
	{
		return((mp_aligned_rect){});
	}

	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, context->attributes.font);
	if(!fontInfo)
	{
		return((mp_aligned_rect){});
	}

	mem_arena* scratch = mem_scratch();
	mp_string32 codePoints = utf8_push_to_codepoints(scratch, text);
	mp_string32 glyphIndices = mp_graphics_font_push_glyph_indices(handle, context->attributes.font, scratch, codePoints);

	//NOTE(martin): find width of missing character
	//TODO(martin): should cache that at font creation...
	mp_graphics_text_extents missingGlyphExtents;
	u32 missingGlyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 0xfffd);

	if(missingGlyphIndex)
	{
		mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, (mp_string32){1, &missingGlyphIndex}, &missingGlyphExtents);
	}
	else
	{
		//NOTE(martin): could not find replacement glyph, try to get an 'x' to get a somewhat correct width
		//              to render an empty rectangle. Otherwise just render with the max font width
		f32 boxWidth = fontInfo->extents.width * 0.8;
		f32 xBearing = fontInfo->extents.width * 0.1;
		f32 xAdvance = fontInfo->extents.width;

		missingGlyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 'x');
		if(missingGlyphIndex)
		{
			mp_graphics_font_get_glyph_extents_from_font_info(fontInfo, (mp_string32){1, &missingGlyphIndex}, &missingGlyphExtents);
		}
		else
		{
			missingGlyphExtents.xBearing = fontInfo->extents.width * 0.1;
			missingGlyphExtents.yBearing = 0;
			missingGlyphExtents.width = fontInfo->extents.width * 0.8;
			missingGlyphExtents.xAdvance = fontInfo->extents.width;
			missingGlyphExtents.yAdvance = 0;
		}
	}

	//NOTE(martin): accumulate text extents
	f32 width = 0;
	f32 x = 0;
	f32 y = 0;
	f32 lineHeight = fontInfo->extents.descent + fontInfo->extents.ascent;

	for(int i=0; i<glyphIndices.len; i++)
	{
		//TODO(martin): make it failsafe for fonts that don't have a glyph for the line-feed codepoint ?

		mp_graphics_glyph_info* glyph = 0;
		mp_graphics_text_extents extents;
		if(!glyphIndices.ptr[i] || glyphIndices.ptr[i] >= fontInfo->glyphCount)
		{
			extents = missingGlyphExtents;
		}
		else
		{
			glyph = mp_graphics_font_get_glyph_info(fontInfo, glyphIndices.ptr[i]);
			extents = glyph->extents;
		}
		x += extents.xAdvance;
		y += extents.yAdvance;

		if(glyph && glyph->codePoint == '\n')
		{
			width = maximum(width, x);
			x = 0;
			y += lineHeight + fontInfo->extents.leading;
		}
	}
	width = maximum(width, x);

	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(handle, context->attributes.font, context->attributes.fontSize);
	mp_aligned_rect rect = {0,
	                        -fontInfo->extents.ascent * fontScale,
	                        width * fontScale,
	                        (y + lineHeight) * fontScale };
	return(rect);
}

//-----------------------------------------------------------------------------------------------------------
// Transform matrix settings
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_matrix_push(mp_graphics_context handle, mp_mat2x3 matrix)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}

	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_MATRIX_PUSH, .matrix = matrix});
}

void mp_graphics_matrix_pop(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_MATRIX_POP});
}
//-----------------------------------------------------------------------------------------------------------
// Graphics attributes settings
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_set_clear_color(mp_graphics_context handle, mp_graphics_color color)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.clearColor = color;
}

void mp_graphics_set_clear_color_rgba(mp_graphics_context handle, f32 r, f32 g, f32 b, f32 a)
{
	mp_graphics_set_clear_color(handle, (mp_graphics_color){r, g, b, a});
}

void mp_graphics_set_color(mp_graphics_context handle, mp_graphics_color color)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.color = color;
}

void mp_graphics_set_color_rgba(mp_graphics_context handle, f32 r, f32 g, f32 b, f32 a)
{
	mp_graphics_set_color(handle, (mp_graphics_color){r, g, b, a});
}

void mp_graphics_set_width(mp_graphics_context handle, f32 width)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.width = width;
}

void mp_graphics_set_tolerance(mp_graphics_context handle, f32 tolerance)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.tolerance = tolerance;
}

void mp_graphics_set_joint(mp_graphics_context handle, mp_graphics_joint_type joint)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.joint = joint;
}

void mp_graphics_set_max_joint_excursion(mp_graphics_context handle, f32 maxJointExcursion)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.maxJointExcursion = maxJointExcursion;
}

void mp_graphics_set_cap(mp_graphics_context handle, mp_graphics_cap_type cap)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.cap = cap;
}

void mp_graphics_set_font(mp_graphics_context handle, mp_graphics_font font)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.font = font;
}

void mp_graphics_set_font_size(mp_graphics_context handle, f32 fontSize)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->attributes.fontSize = fontSize;
}

void mp_graphics_set_text_flip(mp_graphics_context handle, bool flip)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	context->textFlip = flip;
}


//-----------------------------------------------------------------------------------------------------------
// Clip
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_clip_push(mp_graphics_context handle, f32 x, f32 y, f32 w, f32 h)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_CLIP_PUSH,
	                                                          .rect = (mp_graphics_rect){x, y, w, h}});
}

void mp_graphics_clip_pop(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_CLIP_POP});
}

//-----------------------------------------------------------------------------------------------------------
// Path construction
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_new_path(mp_graphics_context_data* context)
{
	context->path.startIndex += context->path.count;
	context->path.count = 0;
	context->subPathStartPoint = context->subPathLastPoint;
	context->path.startPoint = context->subPathStartPoint;
}

void mp_graphics_clear(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context, (mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_CLEAR, .attributes = context->attributes});
}

void mp_graphics_get_current_position(mp_graphics_context handle, f32* x, f32* y)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	*x = context->subPathLastPoint.x;
	*y = context->subPathLastPoint.y;
}

void mp_graphics_path_push_elements(mp_graphics_context_data* context, u32 count, mp_graphics_path_elt* elements)
{
	ASSERT(context->path.count + context->path.startIndex + count <= MP_GRAPHICS_MAX_PATH_ELEMENT_COUNT);
	memcpy(context->pathElements + context->path.startIndex + context->path.count, elements, count*sizeof(mp_graphics_path_elt));
	context->path.count += count;
}

void mp_graphics_path_push_element(mp_graphics_context_data* context, mp_graphics_path_elt elt)
{
	mp_graphics_path_push_elements(context, 1, &elt);
}



void mp_graphics_move_to(mp_graphics_context handle, f32 x, f32 y)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_path_push_element(context, ((mp_graphics_path_elt){.type = MP_GRAPHICS_PATH_MOVE, .p[0] = {x, y}}));
	context->subPathStartPoint = (vec2){x, y};
	context->subPathLastPoint = (vec2){x, y};
}

void mp_graphics_line_to(mp_graphics_context handle, f32 x, f32 y)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_path_push_element(context, ((mp_graphics_path_elt){.type = MP_GRAPHICS_PATH_LINE, .p[0] = {x, y}}));
	context->subPathLastPoint = (vec2){x, y};
}

void mp_graphics_quadratic_to(mp_graphics_context handle, f32 x1, f32 y1, f32 x2, f32 y2)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_path_push_element(context, ((mp_graphics_path_elt){.type = MP_GRAPHICS_PATH_QUADRATIC, .p = {{x1, y1}, {x2, y2}}}));
	context->subPathLastPoint = (vec2){x2, y2};
}

void mp_graphics_cubic_to(mp_graphics_context handle, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_path_push_element(context, ((mp_graphics_path_elt){.type = MP_GRAPHICS_PATH_CUBIC, .p = {{x1, y1}, {x2, y2}, {x3, y3}}}));
	context->subPathLastPoint = (vec2){x3, y3};
}

void mp_graphics_close_path(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	if(  context->subPathStartPoint.x != context->subPathLastPoint.x
	  || context->subPathStartPoint.y != context->subPathLastPoint.y)
	{
		mp_graphics_line_to(handle, context->subPathStartPoint.x, context->subPathStartPoint.y);
	}
	context->subPathStartPoint = context->subPathLastPoint;
}

mp_aligned_rect mp_graphics_glyph_outlines_from_font_info(mp_graphics_context_data* context, mp_graphics_font_info* fontInfo, mp_string32 glyphIndices)
{
	mp_graphics_context contextHandle = mp_graphics_context_handle_from_ptr(context);

	f32 startX = context->subPathLastPoint.x;
	f32 startY = context->subPathLastPoint.y;
	f32 maxWidth = 0;

	f32 scale = context->attributes.fontSize/fontInfo->unitsPerEm;

	for(int i=0; i<glyphIndices.len; i++)
	{
		u32 glyphIndex = glyphIndices.ptr[i];

		f32 xOffset = context->subPathLastPoint.x;
		f32 yOffset = context->subPathLastPoint.y;
		f32 flip = context->textFlip ? -1 : 1;

		if(!glyphIndex || glyphIndex >= fontInfo->glyphCount)
		{
			LOG_WARNING("code point is not present in font ranges\n");
			//NOTE(martin): try to find the replacement character
			glyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 0xfffd);
			if(!glyphIndex)
			{
				//NOTE(martin): could not find replacement glyph, try to get an 'x' to get a somewhat correct width
				//              to render an empty rectangle. Otherwise just render with the max font width
				f32 boxWidth = fontInfo->extents.width * 0.8;
				f32 xBearing = fontInfo->extents.width * 0.1;
				f32 xAdvance = fontInfo->extents.width;

				glyphIndex = mp_graphics_font_get_glyph_index_from_font_info(fontInfo, 'x');
				if(glyphIndex)
				{
					mp_graphics_glyph_info* glyph = &(fontInfo->glyphs[glyphIndex]);
					boxWidth = glyph->extents.width;
					xBearing = glyph->extents.xBearing;
					xAdvance = glyph->extents.xAdvance;
				}
				f32 oldStrokeWidth = context->attributes.width;

				mp_graphics_set_width(contextHandle, boxWidth*0.005);
				mp_graphics_rectangle_stroke(contextHandle,
				                             xOffset + xBearing * scale,
							     yOffset,
							     boxWidth * scale * flip,
							     fontInfo->extents.capHeight*scale);
				mp_graphics_set_width(contextHandle, oldStrokeWidth);
				mp_graphics_move_to(contextHandle, xOffset + xAdvance * scale, yOffset);
				maxWidth = maximum(maxWidth, xOffset + xAdvance*scale - startX);
				continue;
			}
		}

		mp_graphics_glyph_info* glyph = mp_graphics_font_get_glyph_info(fontInfo, glyphIndex);

		mp_graphics_path_push_elements(context, glyph->pathDescriptor.count, fontInfo->outlines + glyph->pathDescriptor.startIndex);

		mp_graphics_path_elt* elements = context->pathElements + context->path.count + context->path.startIndex - glyph->pathDescriptor.count;
		for(int eltIndex=0; eltIndex<glyph->pathDescriptor.count; eltIndex++)
		{
			for(int pIndex = 0; pIndex < 3; pIndex++)
			{
				elements[eltIndex].p[pIndex].x = elements[eltIndex].p[pIndex].x * scale + xOffset;
				elements[eltIndex].p[pIndex].y = elements[eltIndex].p[pIndex].y * scale * flip + yOffset;
			}
		}
		mp_graphics_move_to(contextHandle, xOffset + scale*glyph->extents.xAdvance, yOffset);

		maxWidth = maximum(maxWidth, xOffset + scale*glyph->extents.xAdvance - startX);
	}
	f32 lineHeight = (fontInfo->extents.ascent + fontInfo->extents.descent)*scale;
	mp_aligned_rect box = {startX, startY, maxWidth, context->subPathLastPoint.y - startY + lineHeight };
	return(box);
}

mp_aligned_rect mp_graphics_glyph_outlines(mp_graphics_context handle, mp_string32 glyphIndices)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return((mp_aligned_rect){});
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, context->attributes.font);
	if(!fontInfo)
	{
		return((mp_aligned_rect){});
	}
	return(mp_graphics_glyph_outlines_from_font_info(context, fontInfo, glyphIndices));
}

void mp_graphics_text_outlines(mp_graphics_context handle, mp_string text)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_font_info* fontInfo = mp_graphics_get_font_info(context, context->attributes.font);
	if(!fontInfo)
	{
		return;
	}

	mem_arena* scratch = mem_scratch();
	mp_string32 codePoints = utf8_push_to_codepoints(scratch, text);
	mp_string32 glyphIndices = mp_graphics_font_push_glyph_indices(handle, context->attributes.font, scratch, codePoints);

	mp_graphics_glyph_outlines_from_font_info(context, fontInfo, glyphIndices);
}

//-----------------------------------------------------------------------------------------------------------
// Path primitives commands
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_fill(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	if(context->path.count)
	{
		mp_graphics_push_command(context, ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_FILL, .path = context->path, .attributes = context->attributes}));
		mp_graphics_new_path(context);
	}
}

void mp_graphics_stroke(mp_graphics_context handle)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	if(context->path.count)
	{
		mp_graphics_push_command(context, ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_STROKE, .path = context->path, .attributes = context->attributes}));
		mp_graphics_new_path(context);
	}
}

//-----------------------------------------------------------------------------------------------------------
// Fast shapes primitives commands
//-----------------------------------------------------------------------------------------------------------

void mp_graphics_rectangle_fill(mp_graphics_context handle, f32 x, f32 y, f32 w, f32 h)
{
//	DEBUG_ASSERT(w>=0 && h>=0);

	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_RECT_FILL, .rect = (mp_graphics_rect){x, y, w, h}, .attributes = context->attributes}));
}
void mp_graphics_rectangle_stroke(mp_graphics_context handle, f32 x, f32 y, f32 w, f32 h)
{
//	DEBUG_ASSERT(w>=0 && h>=0);

	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_RECT_STROKE, .rect = (mp_graphics_rect){x, y, w, h}, .attributes = context->attributes}));
}

void mp_graphics_rounded_rectangle_fill(mp_graphics_context handle, f32 x, f32 y, f32 w, f32 h, f32 r)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ROUND_RECT_FILL,
		                              .roundedRect = (mp_graphics_rounded_rect){x, y, w, h, r},
					      .attributes = context->attributes}));
}

void mp_graphics_rounded_rectangle_stroke(mp_graphics_context handle, f32 x, f32 y, f32 w, f32 h, f32 r)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ROUND_RECT_STROKE,
		                              .roundedRect = (mp_graphics_rounded_rect){x, y, w, h, r},
					      .attributes = context->attributes}));
}

void mp_graphics_circle_fill(mp_graphics_context handle, f32 x, f32 y, f32 r)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ELLIPSE_FILL,
		                              .rect = (mp_graphics_rect){x-r, y-r, 2*r, 2*r},
					      .attributes = context->attributes}));
}

void mp_graphics_circle_stroke(mp_graphics_context handle, f32 x, f32 y, f32 r)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ELLIPSE_STROKE,
		                              .rect = (mp_graphics_rect){x-r, y-r, 2*r, 2*r},
					      .attributes = context->attributes}));
}

void mp_graphics_ellipse_fill(mp_graphics_context handle, f32 x, f32 y, f32 rx, f32 ry)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ELLIPSE_FILL,
		                              .rect = (mp_graphics_rect){x-rx, y-ry, 2*rx, 2*ry},
					      .attributes = context->attributes}));
}

void mp_graphics_ellipse_stroke(mp_graphics_context handle, f32 x, f32 y, f32 rx, f32 ry)
{
	mp_graphics_context_data* context = mp_graphics_context_ptr_from_handle(handle);
	if(!context)
	{
		return;
	}
	mp_graphics_push_command(context,
	             ((mp_graphics_primitive){.cmd = MP_GRAPHICS_CMD_ELLIPSE_STROKE,
		                              .rect = (mp_graphics_rect){x-rx, y-ry, 2*rx, 2*ry},
					      .attributes = context->attributes}));
}

void mp_graphics_arc(mp_graphics_context handle, f32 x, f32 y, f32 r, f32 arcAngle, f32 startAngle)
{
	f32 endAngle = startAngle + arcAngle;

	while(startAngle < endAngle)
	{
		f32 smallAngle = minimum(endAngle - startAngle, M_PI/4.);
		if(smallAngle < 0.001)
		{
			break;
		}

		vec2 v0 = {cos(smallAngle/2), sin(smallAngle/2)};
		vec2 v1 = {(4-v0.x)/3, (1-v0.x)*(3-v0.x)/(3*v0.y)};
		vec2 v2 = {v1.x, -v1.y};
		vec2 v3 = {v0.x, -v0.y};

		f32 rotAngle = smallAngle/2 + startAngle;
		f32 rotCos = cos(rotAngle);
		f32 rotSin = sin(rotAngle);

		mp_mat2x3 t = {r*rotCos, -r*rotSin, x,
			       r*rotSin, r*rotCos, y};

		v0 = mp_mat2x3_mul(t, v0);
		v1 = mp_mat2x3_mul(t, v1);
		v2 = mp_mat2x3_mul(t, v2);
		v3 = mp_mat2x3_mul(t, v3);

		mp_graphics_move_to(handle, v0.x, v0.y);
		mp_graphics_cubic_to(handle, v1.x, v1.y, v2.x, v2.y, v3.x, v3.y);

		startAngle += smallAngle;
	}
}

#undef LOG_SUBSYSTEM
