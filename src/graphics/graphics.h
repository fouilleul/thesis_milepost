/************************************************************//**
*
*	@file: graphics.h
*	@author: Martin Fouilleul
*	@date: 12/07/2020
*	@revision:
*
*****************************************************************/
#ifndef __GRAPHICS_H_
#define __GRAPHICS_H_

#include"typedefs.h"
#include"utf8.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct mp_mat2x3
{
	f32 m[6];
} mp_mat2x3;

typedef enum { MP_GRAPHICS_COORDS_2D_DISPLAY_CENTER,
               MP_GRAPHICS_COORDS_2D_DISPLAY_TOP_LEFT,
	       MP_GRAPHICS_COORDS_2D_DISPLAY_BOTTOM_LEFT } mp_graphics_coordinate_system;

typedef struct mp_graphics_color
{
	union
	{
		struct
		{
			f32 r;
			f32 g;
			f32 b;
			f32 a;
		};
		f32 components[4];
	};
} mp_graphics_color;

typedef enum {MP_GRAPHICS_JOINT_MITER = 0,
              MP_GRAPHICS_JOINT_BEVEL,
	      MP_GRAPHICS_JOINT_NONE } mp_graphics_joint_type;

typedef enum {MP_GRAPHICS_CAP_NONE = 0,
              MP_GRAPHICS_CAP_SQUARE } mp_graphics_cap_type;

typedef struct mp_graphics_font_extents
{
	f32 ascent;    // the extent above the baseline (by convention a positive value extends above the baseline)
	f32 descent;   // the extent below the baseline (by convention, positive value extends below the baseline)
	f32 leading;   // spacing between one row's descent and the next row's ascent
	f32 xHeight;   // height of the lower case letter 'x'
	f32 capHeight; // height of the upper case letter 'M'
	f32 width;     // maximum width of the font

} mp_graphics_font_extents;

typedef struct mp_graphics_text_extents
{
	f32 xBearing;
	f32 yBearing;
	f32 width;
	f32 height;
	f32 xAdvance;
	f32 yAdvance;

} mp_graphics_text_extents;

typedef struct mp_graphics_font { u64 h; } mp_graphics_font;
typedef struct mp_graphics_context { u64 h; } mp_graphics_context;
typedef struct mp_graphics_surface { u64 h; } mp_graphics_surface;
typedef struct mp_graphics_stream { u64 h; } mp_graphics_stream;

#include"platform_app.h" // mp_window, mp_aligned_rect

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics backends infos
//------------------------------------------------------------------------------------------

typedef u32 mp_graphics_backend_flags;
const mp_graphics_backend_flags MP_GRAPHICS_BACKEND_DEFAULT     = 0x00,
                                MP_GRAPHICS_BACKEND_SOFTWARE    = 0x01,
                                MP_GRAPHICS_BACKEND_ACCELERATED = 0x01<<1;

const i32 MP_GRAPHICS_BACKEND_ANY = -1;

typedef struct mp_graphics_backend_info
{
	const char* name;
	const char* description;

	mp_graphics_backend_flags flags;

} mp_graphics_backend_info;

u32 mp_graphics_get_backend_count();
u32 mp_graphics_get_backend_infos(u32 maxCount, mp_graphics_backend_info* backendInfos);

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics init
//------------------------------------------------------------------------------------------
void mp_graphics_init();

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics surface
//------------------------------------------------------------------------------------------
mp_graphics_surface mp_graphics_surface_create_for_window(mp_window window, i32 backendIndex, mp_graphics_backend_flags flags);
void mp_graphics_surface_destroy(mp_graphics_surface surface);
void mp_graphics_surface_present(mp_graphics_surface surface);

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics context lifetime
//------------------------------------------------------------------------------------------
mp_graphics_context mp_graphics_context_create();
void mp_graphics_context_destroy(mp_graphics_context context);
void mp_graphics_context_flush(mp_graphics_context context, mp_graphics_surface surface);

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics command streams
//------------------------------------------------------------------------------------------
mp_graphics_stream mp_graphics_stream_create(mp_graphics_context context);
mp_graphics_stream mp_graphics_stream_swap(mp_graphics_context context, mp_graphics_stream stream);
void mp_graphics_stream_append(mp_graphics_context context, mp_graphics_stream stream);

//------------------------------------------------------------------------------------------
//NOTE(martin): fonts management
//------------------------------------------------------------------------------------------
mp_graphics_font mp_graphics_font_create_from_memory(mp_graphics_context context, u32 size, byte* buffer, u32 rangeCount, unicode_range* ranges);
void mp_graphics_font_destroy(mp_graphics_context context, mp_graphics_font font);

//NOTE(martin): the following int valued functions return -1 if font is invalid or codepoint is not present in font//
//TODO(martin): add enum error codes

int mp_graphics_font_get_extents(mp_graphics_context context, mp_graphics_font font, mp_graphics_font_extents* outExtents);
f32 mp_graphics_font_get_scale_for_em_pixels(mp_graphics_context context, mp_graphics_font font, f32 emSize);

//NOTE(martin): if you need to process more than one codepoint, first convert your codepoints to glyph indices, then use the
//              glyph index versions of the functions, which can take an array of glyph indices.

mp_string32 mp_graphics_font_get_glyph_indices(mp_graphics_context context, mp_graphics_font font, mp_string32 codePoints, mp_string32 backing);
mp_string32 mp_graphics_font_push_glyph_indices(mp_graphics_context context, mp_graphics_font font, mem_arena* arena, mp_string32 codePoints);

u32 mp_graphics_font_get_glyph_index(mp_graphics_context context, mp_graphics_font font, utf32 codePoint);

int mp_graphics_font_get_codepoint_extents(mp_graphics_context context, mp_graphics_font font, utf32 codePoint, mp_graphics_text_extents* outExtents);

int mp_graphics_font_get_glyph_extents(mp_graphics_context context,
                                       mp_graphics_font font,
                                       mp_string32 glyphIndices,
                                       mp_graphics_text_extents* outExtents);

mp_aligned_rect mp_graphics_text_bounding_box(mp_graphics_context context, mp_string text);

//TODO: remove, replace usage with mp_graphics_text_bounding_box()
vec2 mp_graphics_get_glyphs_dimensions(mp_graphics_context context, mp_string32 glyphIndices);

//------------------------------------------------------------------------------------------
//NOTE(martin): matrix settings
//------------------------------------------------------------------------------------------
void mp_graphics_matrix_push(mp_graphics_context context, mp_mat2x3 matrix);
void mp_graphics_matrix_pop(mp_graphics_context context);

//------------------------------------------------------------------------------------------
//NOTE(martin): clipping
//------------------------------------------------------------------------------------------
void mp_graphics_clip_push(mp_graphics_context context, f32 x, f32 y, f32 w, f32 h);
void mp_graphics_clip_pop(mp_graphics_context context);

//------------------------------------------------------------------------------------------
//NOTE(martin): graphics attributes setting
//------------------------------------------------------------------------------------------
void mp_graphics_set_clear_color(mp_graphics_context context, mp_graphics_color color);
void mp_graphics_set_clear_color_rgba(mp_graphics_context context, f32 r, f32 g, f32 b, f32 a);
void mp_graphics_set_color(mp_graphics_context context, mp_graphics_color color);
void mp_graphics_set_color_rgba(mp_graphics_context context, f32 r, f32 g, f32 b, f32 a);
void mp_graphics_set_width(mp_graphics_context context, f32 width);
void mp_graphics_set_tolerance(mp_graphics_context context, f32 tolerance);
void mp_graphics_set_joint(mp_graphics_context context, mp_graphics_joint_type joint);
void mp_graphics_set_max_joint_excursion(mp_graphics_context context, f32 maxJointExcursion);
void mp_graphics_set_cap(mp_graphics_context context, mp_graphics_cap_type cap);
void mp_graphics_set_font(mp_graphics_context context, mp_graphics_font font);
void mp_graphics_set_font_size(mp_graphics_context context, f32 size);
void mp_graphics_set_text_flip(mp_graphics_context context, bool flip);
//------------------------------------------------------------------------------------------
//NOTE(martin): path construction
//------------------------------------------------------------------------------------------

void mp_graphics_get_current_position(mp_graphics_context context, f32* x, f32* y);

void mp_graphics_move_to(mp_graphics_context context, f32 x, f32 y);
void mp_graphics_line_to(mp_graphics_context context, f32 x, f32 y);
void mp_graphics_quadratic_to(mp_graphics_context context, f32 x1, f32 y1, f32 x2, f32 y2);
void mp_graphics_cubic_to(mp_graphics_context context, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3);
void mp_graphics_close_path(mp_graphics_context context);

mp_aligned_rect mp_graphics_glyph_outlines(mp_graphics_context context, mp_string32 glyphIndices);
void mp_graphics_text_outlines(mp_graphics_context context, mp_string string);

//------------------------------------------------------------------------------------------
//NOTE(martin): clear/fill/stroke
//------------------------------------------------------------------------------------------
void mp_graphics_clear(mp_graphics_context context);
void mp_graphics_fill(mp_graphics_context context);
void mp_graphics_stroke(mp_graphics_context context);

//------------------------------------------------------------------------------------------
//NOTE(martin): 'fast' shapes primitives
//------------------------------------------------------------------------------------------
void mp_graphics_rectangle_fill(mp_graphics_context context, f32 x, f32 y, f32 w, f32 h);
void mp_graphics_rectangle_stroke(mp_graphics_context context, f32 x, f32 y, f32 w, f32 h);
void mp_graphics_rounded_rectangle_fill(mp_graphics_context context, f32 x, f32 y, f32 w, f32 h, f32 r);
void mp_graphics_rounded_rectangle_stroke(mp_graphics_context context, f32 x, f32 y, f32 w, f32 h, f32 r);
void mp_graphics_ellipse_fill(mp_graphics_context context, f32 x, f32 y, f32 rx, f32 ry);
void mp_graphics_ellipse_stroke(mp_graphics_context context, f32 x, f32 y, f32 rx, f32 ry);
void mp_graphics_circle_fill(mp_graphics_context context, f32 x, f32 y, f32 r);
void mp_graphics_circle_stroke(mp_graphics_context context, f32 x, f32 y, f32 r);


void mp_graphics_arc(mp_graphics_context handle, f32 x, f32 y, f32 r, f32 arcAngle, f32 startAngle);

//------------------------------------------------------------------------------------------
//NOTE(martin): images
//------------------------------------------------------------------------------------------
typedef struct mp_graphics_texture { u64 h; } mp_graphics_texture;

mp_graphics_texture mp_graphics_texture_create_rgba8(mp_graphics_surface surface, u32 width, u32 height);
void mp_graphics_texture_destroy(mp_graphics_surface surface, mp_graphics_texture texture);

u32 mp_graphics_texture_width(mp_graphics_surface surface, mp_graphics_texture texture);
u32 mp_graphics_texture_height(mp_graphics_surface surface, mp_graphics_texture texture);
void* mp_graphics_texture_data(mp_graphics_surface surface, mp_graphics_texture texture);

void mp_graphics_blit(mp_graphics_context context, mp_graphics_texture texture, mp_aligned_rect rect);

//TODO: or set as a brush / pattern and use it to fill shapes...

#ifdef __cplusplus
} // extern "C"
#endif

#endif //__GRAPHICS_H_
