
#ifndef __VERTEX_H__
#define __VERTEX_H__

#include<simd/simd.h>

#define RENDERER_TILE_BUFFER_SIZE 4096
#define RENDERER_TILE_SIZE 16
#define RENDERER_MAX_TILES 65536

#define RENDERER_DEBUG_TILE_VISITED 0xf00d
#define RENDERER_DEBUG_TILE_BUFFER_OVERFLOW 0xdead

typedef struct mp_vertex
{
	vector_float2 pos;
	vector_float4 uv;
	vector_float4 color;
	vector_float4 clip;
	int zIndex;

} mp_vertex;

typedef struct mp_triangle_data
{
	uint i0;
	uint i1;
	uint i2;
	uint zIndex;

	vector_float2 p0;
	vector_float2 p1;
	vector_float2 p2;

	int bias0;
	int bias1;
	int bias2;

} mp_triangle_data;

#endif //__VERTEX_H__
