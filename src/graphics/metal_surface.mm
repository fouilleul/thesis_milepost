/************************************************************//**
*
*	@file: graphics.mm
*	@author: Martin Fouilleul
*	@date: 12/07/2020
*	@revision:
*
*****************************************************************/
#import<Metal/Metal.h>
#import<QuartzCore/CAMetalLayer.h>
#include<simd/simd.h>

#include"graphics_internal.h"
#include"macro_helpers.h"
#include"osx_app.h"
#include"vertex.h"

#define LOG_SUBSYSTEM "Graphics"

_Atomic(bool) DEBUG_VERTEX_LAYOUT_ACTIVE;

static const u32 MP_METAL_MAX_DRAWABLES_IN_FLIGHT = 3;

typedef struct mp_graphics_metal_surface
{
	mp_graphics_surface_data interface;

	// permanent metal resources
	id<MTLDevice> device;
	CAMetalLayer* metalLayer;
	id<MTLCommandQueue> commandQueue;

	id<MTLComputePipelineState> tilingPipeline;
	id<MTLComputePipelineState> sortingPipeline;
	id<MTLComputePipelineState> boxingPipeline;
	id<MTLComputePipelineState> computePipeline;
	id<MTLRenderPipelineState> renderPipeline;

	vector_uint2 viewPort;

	// textures and buffers
	id<MTLTexture> outTexture;
	id<MTLBuffer> vertexBuffer;
	id<MTLBuffer> indexBuffer;
	id<MTLBuffer> tileCounters;
	id<MTLBuffer> tilesArray;
	id<MTLBuffer> triangleArray;
	id<MTLBuffer> boxArray;

	u32 vertexCount;
	u32 indexCount;

	// transient metal resources
	id<CAMetalDrawable>  drawable;
	id<MTLCommandBuffer> commandBuffer;

	dispatch_semaphore_t drawableSemaphore;

} mp_graphics_metal_surface;

void mp_metal_surface_acquire_drawable_and_command_buffer(mp_graphics_metal_surface* surface)
{@autoreleasepool{
	/*WARN(martin): this is super important
		When the app is put in the background, it seems that if there are buffers in flight, the combination
		of the compute pass and the render pass causes drawables to be leaked. This causes the gpu to allocate
		more and more drawables, until the app crashes.
		(note: the drawable objects themselves are released once the app comes back to the forefront, but the
		memory allocated in the GPU is never freed...)

		In background the gpu seems to create drawable if none is available instead of actually
		blocking on nextDrawable. These drawable never get freed.
		This is not a problem if our shader is fast enough, since a previous drawable becomes
		available before we finish the frame. But we want to protect against it anyway

		The normal blocking mechanism of nextDrawable seems useless, so we implement our own scheme by
		counting the number of drawables available with a semaphore that gets decremented here and
		incremented in the presentedHandler of the drawable.
		Thus we ensure that we don't consume more drawables than we are able to draw.

		//TODO: we _also_ should stop trying to render if we detect that the app is in the background
		or occluded, but we can't count only on this because there is a potential race between the
		notification of background mode and the rendering.

		//TODO: We should set a reasonable timeout and skip the frame and log an error in case we are stalled
		for too long.
	*/
	dispatch_semaphore_wait(surface->drawableSemaphore, DISPATCH_TIME_FOREVER);

	surface->drawable = [surface->metalLayer nextDrawable];
	ASSERT(surface->drawable != nil);

	//TODO: make this a weak reference if we use ARC
	dispatch_semaphore_t semaphore = surface->drawableSemaphore;
	[surface->drawable addPresentedHandler:^(id<MTLDrawable> drawable){
		dispatch_semaphore_signal(semaphore);
		}];

	//NOTE(martin): create a command buffer
	surface->commandBuffer = [surface->commandQueue commandBuffer];

	[surface->commandBuffer retain];
	[surface->drawable retain];
}}

void mp_metal_surface_draw_buffers(mp_graphics_surface_data* interface, u32 vertexCount, u32 indexCount, mp_graphics_color clearColor)
{
	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)interface;

	@autoreleasepool
	{
		if(surface->commandBuffer == nil || surface->commandBuffer == nil)
		{
			mp_metal_surface_acquire_drawable_and_command_buffer(surface);
		}

		ASSERT(indexCount * sizeof(i32) < [surface->indexBuffer length]);

		CGSize  viewportSize = surface->metalLayer.drawableSize;

		//-----------------------------------------------------------
		//NOTE(martin): encode the clear counter
		//-----------------------------------------------------------
		id<MTLBlitCommandEncoder> blitEncoder = [surface->commandBuffer blitCommandEncoder];
		[blitEncoder fillBuffer: surface->tileCounters range: NSMakeRange(0, RENDERER_MAX_TILES*sizeof(uint)) value: 0];
		[blitEncoder endEncoding];

		//-----------------------------------------------------------
		//NOTE(martin): encode the boxing pass
		//-----------------------------------------------------------
		id<MTLComputeCommandEncoder> boxEncoder = [surface->commandBuffer computeCommandEncoder];
		[boxEncoder setComputePipelineState: surface->boxingPipeline];
		[boxEncoder setBuffer: surface->vertexBuffer offset:0 atIndex: 0];
		[boxEncoder setBuffer: surface->indexBuffer offset:0 atIndex: 1];
		[boxEncoder setBuffer: surface->triangleArray offset:0 atIndex: 2];
		[boxEncoder setBuffer: surface->boxArray offset:0 atIndex: 3];

		MTLSize boxGroupSize = MTLSizeMake(surface->boxingPipeline.maxTotalThreadsPerThreadgroup, 1, 1);
		MTLSize boxGridSize = MTLSizeMake(indexCount/3, 1, 1);

		[boxEncoder dispatchThreads: boxGridSize threadsPerThreadgroup: boxGroupSize];
		[boxEncoder endEncoding];

		//-----------------------------------------------------------
		//NOTE(martin): encode the tiling pass
		//-----------------------------------------------------------

		id<MTLComputeCommandEncoder> tileEncoder = [surface->commandBuffer computeCommandEncoder];
		[tileEncoder setComputePipelineState: surface->tilingPipeline];
		[tileEncoder setBuffer: surface->boxArray offset:0 atIndex: 0];
		[tileEncoder setBuffer: surface->tileCounters offset:0 atIndex: 1];
		[tileEncoder setBuffer: surface->tilesArray offset:0 atIndex: 2];
		[tileEncoder setBytes: &surface->viewPort length: sizeof(surface->viewPort) atIndex: 3];

		[tileEncoder dispatchThreads: boxGridSize threadsPerThreadgroup: boxGroupSize];
		[tileEncoder endEncoding];

		//-----------------------------------------------------------
		//NOTE(martin): encode the sorting pass
		//-----------------------------------------------------------

		id<MTLComputeCommandEncoder> sortEncoder = [surface->commandBuffer computeCommandEncoder];
		[sortEncoder setComputePipelineState: surface->sortingPipeline];
		[sortEncoder setBuffer: surface->tileCounters offset:0 atIndex: 0];
		[sortEncoder setBuffer: surface->triangleArray offset:0 atIndex: 1];
		[sortEncoder setBuffer: surface->tilesArray offset:0 atIndex: 2];
		[sortEncoder setBytes: &surface->viewPort length: sizeof(surface->viewPort) atIndex: 3];

		u32     nTilesX = (viewportSize.width + RENDERER_TILE_SIZE - 1)/RENDERER_TILE_SIZE;
		u32     nTilesY = (viewportSize.height + RENDERER_TILE_SIZE - 1)/RENDERER_TILE_SIZE;

		MTLSize sortGroupSize = MTLSizeMake(surface->boxingPipeline.maxTotalThreadsPerThreadgroup, 1, 1);
		MTLSize sortGridSize = MTLSizeMake(nTilesX*nTilesY, 1, 1);

		[sortEncoder dispatchThreads: sortGridSize threadsPerThreadgroup: sortGroupSize];
		[sortEncoder endEncoding];

		//-----------------------------------------------------------
		//NOTE(martin): create compute encoder and encode commands
		//-----------------------------------------------------------
		vector_float4 clearColorVec4 = {clearColor.r, clearColor.g, clearColor.b, clearColor.a};

		id<MTLComputeCommandEncoder> encoder = [surface->commandBuffer computeCommandEncoder];
		[encoder setComputePipelineState:surface->computePipeline];
		[encoder setTexture: surface->outTexture atIndex: 0];
		[encoder setBuffer: surface->vertexBuffer offset:0 atIndex: 0];
		[encoder setBuffer: surface->tileCounters offset:0 atIndex: 1];
		[encoder setBuffer: surface->tilesArray offset:0 atIndex: 2];
		[encoder setBuffer: surface->triangleArray offset:0 atIndex: 3];
		[encoder setBuffer: surface->boxArray offset:0 atIndex: 4];
		[encoder setBytes: &clearColorVec4 length: sizeof(vector_float4) atIndex: 5];

		//TODO: check that we don't exceed maxTotalThreadsPerThreadgroup
		DEBUG_ASSERT(RENDERER_TILE_SIZE*RENDERER_TILE_SIZE <= surface->computePipeline.maxTotalThreadsPerThreadgroup);
		MTLSize threadGridSize = MTLSizeMake(viewportSize.width, viewportSize.height, 1);
		MTLSize threadGroupSize = MTLSizeMake(RENDERER_TILE_SIZE, RENDERER_TILE_SIZE, 1);

		[encoder dispatchThreads: threadGridSize threadsPerThreadgroup:threadGroupSize];
		[encoder endEncoding];

		//-----------------------------------------------------------
		//NOTE(martin): acquire drawable, create render encoder to blit texture to framebuffer
		//-----------------------------------------------------------

		MTLRenderPassDescriptor* renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
		renderPassDescriptor.colorAttachments[0].texture = surface->drawable.texture;
		renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;

		id<MTLRenderCommandEncoder> renderEncoder = [surface->commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		[renderEncoder setRenderPipelineState: surface->renderPipeline];
		[renderEncoder setFragmentTexture: surface->outTexture atIndex: 0];
		[renderEncoder drawPrimitives: MTLPrimitiveTypeTriangle
			 vertexStart: 0
			 vertexCount: 3 ];
		[renderEncoder endEncoding];
	}
}

void mp_metal_surface_present(mp_graphics_surface_data* interface)
{
	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)interface;
	@autoreleasepool
	{
		//NOTE(martin): present drawable and commit command buffer
		[surface->commandBuffer presentDrawable: surface->drawable];

		[surface->commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> cb){DEBUG_VERTEX_LAYOUT_ACTIVE = false;}];

		[surface->commandBuffer commit];


		[surface->commandBuffer waitUntilCompleted];

		//NOTE(martin): acquire next frame resources
		[surface->commandBuffer release];
		surface->commandBuffer = nil;
		[surface->drawable release];
		surface->drawable = nil;

		mp_metal_surface_acquire_drawable_and_command_buffer(surface);
	}
}

void mp_metal_surface_destroy(mp_graphics_surface_data* interface)
{
	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)interface;

	@autoreleasepool
	{
		[surface->outTexture release];

		[surface->vertexBuffer release];
		[surface->indexBuffer release];
		[surface->tilesArray release];
		[surface->triangleArray release];
		[surface->boxArray release];

		[surface->commandQueue release];
		[surface->computePipeline release];

		[surface->metalLayer release];
		[surface->device release];
	}
}

void mp_metal_surface_window_resized(mp_graphics_surface_data* interface, u32 width, u32 height)
{
	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)interface;

	@autoreleasepool
	{
		//TODO(martin): actually detect scaling
		f32 scale = surface->metalLayer.contentsScale;
		CGSize drawableSize = (CGSize){.width = width * scale, .height = height * scale};
		surface->metalLayer.drawableSize = drawableSize;

		[surface->outTexture release];

		MTLTextureDescriptor* texDesc = [[MTLTextureDescriptor alloc] init];
		texDesc.textureType = MTLTextureType2D;
		texDesc.storageMode = MTLStorageModePrivate;
		texDesc.usage = MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
		texDesc.pixelFormat = MTLPixelFormatBGRA8Unorm;// MTLPixelFormatBGRA8Unorm_sRGB;
		texDesc.width = surface->metalLayer.drawableSize.width;
		texDesc.height = surface->metalLayer.drawableSize.height;

		surface->outTexture = [surface->device newTextureWithDescriptor:texDesc];

		surface->viewPort.x = surface->metalLayer.drawableSize.width;
		surface->viewPort.y = surface->metalLayer.drawableSize.height;
	}
}

static const f32 MP_METAL_SURFACE_CONTENTS_SCALING = 2;

static const int MP_METAL_DEFAULT_BUFFER_LENGTH = 4<<20;

mp_graphics_surface_vertex_layout mp_metal_surface_get_vertex_layout(mp_graphics_surface_data* interface)
{
	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)interface;

	char* vertexBase = (char*)[surface->vertexBuffer contents];

	mp_graphics_surface_vertex_layout layout = {
		.maxVertexCount = MP_METAL_DEFAULT_BUFFER_LENGTH,
	        .maxIndexCount = MP_METAL_DEFAULT_BUFFER_LENGTH,
	        .posBuffer = vertexBase + offsetof(mp_vertex, pos),
	        .posStride = sizeof(mp_vertex),
	        .uvBuffer = vertexBase + offsetof(mp_vertex, uv),
	        .uvStride = sizeof(mp_vertex),
	        .colorBuffer = vertexBase + offsetof(mp_vertex, color),
	        .colorStride = sizeof(mp_vertex),
	        .zIndexBuffer = vertexBase + offsetof(mp_vertex, zIndex),
	        .zIndexStride = sizeof(mp_vertex),
	        .clipsBuffer = vertexBase + offsetof(mp_vertex, clip),
	        .clipsStride = sizeof(mp_vertex),
	        .indexBuffer = [surface->indexBuffer contents],
	        .indexStride = sizeof(int)};

	if(DEBUG_VERTEX_LAYOUT_ACTIVE)
	{
		LOG_WARNING("debug vertex layout still active\n");
	}
	DEBUG_VERTEX_LAYOUT_ACTIVE = true;

	return(layout);
}


mp_graphics_surface_data* mp_metal_surface_create_for_window_ptr(mp_window_data* window)
{
	DEBUG_VERTEX_LAYOUT_ACTIVE = false;

	mp_graphics_metal_surface* surface = (mp_graphics_metal_surface*)malloc(sizeof(mp_graphics_metal_surface));

	//NOTE(martin): setup interface functions
	surface->interface.destroy = mp_metal_surface_destroy;
	surface->interface.draw_buffers = mp_metal_surface_draw_buffers;
	surface->interface.window_resized = mp_metal_surface_window_resized;
	surface->interface.present = mp_metal_surface_present;
	surface->interface.get_vertex_layout = mp_metal_surface_get_vertex_layout;

	//TODO: decide what we do if there is already another surface for that window ?
	window->surface = (mp_graphics_surface_data*)surface;

	@autoreleasepool
	{
		surface->drawableSemaphore = dispatch_semaphore_create(MP_METAL_MAX_DRAWABLES_IN_FLIGHT);

		//-----------------------------------------------------------
		//NOTE(martin): create a metal device and a metal layer and
		//-----------------------------------------------------------
		surface->device = MTLCreateSystemDefaultDevice();
		[surface->device retain];
		surface->metalLayer = [CAMetalLayer layer];
		[surface->metalLayer retain];
		surface->metalLayer.device = surface->device;

		NSWindow* nativeWindow = window->nsWindow;
		NSView* contentView = [nativeWindow contentView];

		//-----------------------------------------------------------
		//NOTE(martin): set the size and scaling
		//-----------------------------------------------------------
		CGSize size = contentView.bounds.size;
		size.width *= MP_METAL_SURFACE_CONTENTS_SCALING;
		size.height *= MP_METAL_SURFACE_CONTENTS_SCALING;
		surface->metalLayer.drawableSize = size;
		surface->metalLayer.contentsScale = MP_METAL_SURFACE_CONTENTS_SCALING;

		surface->metalLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;

		surface->viewPort.x = surface->metalLayer.drawableSize.width;
		surface->viewPort.y = surface->metalLayer.drawableSize.height;

		contentView.wantsLayer = YES;
		contentView.layer = surface->metalLayer;

		//NOTE(martin): handling resizing
		contentView.layerContentsRedrawPolicy = NSViewLayerContentsRedrawDuringViewResize;
		surface->metalLayer.autoresizingMask = kCALayerHeightSizable | kCALayerWidthSizable;
		surface->metalLayer.needsDisplayOnBoundsChange = YES;

		//-----------------------------------------------------------
		//NOTE(martin): create a command queue
		//-----------------------------------------------------------
		surface->commandQueue = [surface->device newCommandQueue];
		[surface->commandQueue retain];

		//-----------------------------------------------------------
		//NOTE(martin): create our output texture
		//-----------------------------------------------------------
		MTLTextureDescriptor* texDesc = [[MTLTextureDescriptor alloc] init];
		texDesc.textureType = MTLTextureType2D;
		texDesc.storageMode = MTLStorageModePrivate;
		texDesc.usage = MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
		texDesc.pixelFormat = MTLPixelFormatBGRA8Unorm;// MTLPixelFormatBGRA8Unorm_sRGB;
		texDesc.width = surface->metalLayer.drawableSize.width;
		texDesc.height = surface->metalLayer.drawableSize.height;

		surface->outTexture = [surface->device newTextureWithDescriptor:texDesc];
		//TODO(martin): retain ?

		//-----------------------------------------------------------
		//NOTE(martin): create buffers for vertex and index
		//-----------------------------------------------------------

		MTLResourceOptions bufferOptions = MTLResourceCPUCacheModeWriteCombined
		                                 | MTLResourceStorageModeShared;

		surface->indexBuffer = [surface->device newBufferWithLength: MP_METAL_DEFAULT_BUFFER_LENGTH*sizeof(int)
		                                        options: bufferOptions];

		surface->vertexBuffer = [surface->device newBufferWithLength: MP_METAL_DEFAULT_BUFFER_LENGTH*sizeof(mp_vertex)
		                                        options: bufferOptions];

		surface->indexCount = MP_METAL_DEFAULT_BUFFER_LENGTH;
		surface->vertexCount = MP_METAL_DEFAULT_BUFFER_LENGTH;

		surface->tilesArray = [surface->device newBufferWithLength: RENDERER_TILE_BUFFER_SIZE*sizeof(int)*RENDERER_MAX_TILES
							options: MTLResourceStorageModePrivate];

		surface->triangleArray = [surface->device newBufferWithLength: MP_METAL_DEFAULT_BUFFER_LENGTH*sizeof(mp_triangle_data)
							options: MTLResourceStorageModePrivate];

		surface->boxArray = [surface->device newBufferWithLength: MP_METAL_DEFAULT_BUFFER_LENGTH*sizeof(vector_float4)
							options: MTLResourceStorageModePrivate];

		//TODO(martin): retain ?
		//-----------------------------------------------------------
		//NOTE(martin): create and initialize tile counters
		//-----------------------------------------------------------
		surface->tileCounters = [surface->device newBufferWithLength: RENDERER_MAX_TILES*sizeof(uint)
		                                         options: MTLResourceStorageModePrivate];
		id<MTLCommandBuffer> commandBuffer = [surface->commandQueue commandBuffer];
		id<MTLBlitCommandEncoder> blitEncoder = [commandBuffer blitCommandEncoder];
		[blitEncoder fillBuffer: surface->tileCounters range: NSMakeRange(0, RENDERER_MAX_TILES*sizeof(uint)) value: 0];
		[blitEncoder endEncoding];
		[commandBuffer commit];

		//-----------------------------------------------------------
		//NOTE(martin): load the library
		//-----------------------------------------------------------

		//TODO(martin): filepath magic to find metallib path when not in the working directory
		char* shaderPath = 0;
		mp_app_get_resource_path("../resources/shader.metallib", &shaderPath);
		NSString* metalFileName = [[NSString alloc] initWithCString: shaderPath encoding: NSUTF8StringEncoding];
		free(shaderPath);
		NSError* err = 0;
		id<MTLLibrary> library = [surface->device newLibraryWithFile: metalFileName error:&err];
		if(err != nil)
		{
			const char* errStr = [[err localizedDescription] UTF8String];
			LOG_ERROR("error : %s\n", errStr);
			return(0);
		}
		id<MTLFunction> tilingFunction = [library newFunctionWithName:@"TileKernel2"];
		id<MTLFunction> sortingFunction = [library newFunctionWithName:@"SortKernel"];
		id<MTLFunction> boxingFunction = [library newFunctionWithName:@"BoundingBoxKernel"];
		id<MTLFunction> computeFunction = [library newFunctionWithName:@"RenderKernel2"];
		id<MTLFunction> vertexFunction = [library newFunctionWithName:@"VertexShader"];
		id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"FragmentShader"];

		//-----------------------------------------------------------
		//NOTE(martin): setup our data layout and pipeline state
		//-----------------------------------------------------------
		NSError* error = NULL;
		surface->computePipeline = [surface->device newComputePipelineStateWithFunction: computeFunction
		                                                                           error:&error];
		ASSERT(surface->computePipeline);

		MTLComputePipelineDescriptor* tilingPipelineDesc = [[MTLComputePipelineDescriptor alloc] init];
		tilingPipelineDesc.computeFunction = tilingFunction;
//		tilingPipelineDesc.threadGroupSizeIsMultipleOfThreadExecutionWidth = true;

		surface->tilingPipeline = [surface->device newComputePipelineStateWithDescriptor: tilingPipelineDesc
		                                           options: MTLPipelineOptionNone
		                                           reflection: nil
		                                           error: &error];

		MTLComputePipelineDescriptor* sortingPipelineDesc = [[MTLComputePipelineDescriptor alloc] init];
		sortingPipelineDesc.computeFunction = sortingFunction;
//		sortingPipelineDesc.threadGroupSizeIsMultipleOfThreadExecutionWidth = true;

		surface->sortingPipeline = [surface->device newComputePipelineStateWithDescriptor: sortingPipelineDesc
		                                           options: MTLPipelineOptionNone
		                                           reflection: nil
		                                           error: &error];

		MTLComputePipelineDescriptor* boxingPipelineDesc = [[MTLComputePipelineDescriptor alloc] init];
		boxingPipelineDesc.computeFunction = boxingFunction;
//		boxingPipelineDesc.threadGroupSizeIsMultipleOfThreadExecutionWidth = true;

		surface->boxingPipeline = [surface->device newComputePipelineStateWithDescriptor: boxingPipelineDesc
		                                           options: MTLPipelineOptionNone
		                                           reflection: nil
		                                           error: &error];
		//-----------------------------------------------------------
		//NOTE(martin): setup our render pipeline state
		//-----------------------------------------------------------
		// create and initialize the pipeline state descriptor
		MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
		pipelineStateDescriptor.label = @"My simple pipeline";
		pipelineStateDescriptor.vertexFunction = vertexFunction;
		pipelineStateDescriptor.fragmentFunction = fragmentFunction;
		pipelineStateDescriptor.colorAttachments[0].pixelFormat = surface->metalLayer.pixelFormat;

		// create render pipeline
		surface->renderPipeline = [surface->device newRenderPipelineStateWithDescriptor: pipelineStateDescriptor error:&err];
		if(err != nil)
		{
			const char* errStr = [[err localizedDescription] UTF8String];
			const char* descStr = [[err localizedFailureReason] UTF8String];
			const char* recovStr = [[err localizedRecoverySuggestion] UTF8String];
			LOG_ERROR("(%li) %s. %s. %s\n", [err code], errStr, descStr, recovStr);
			return(0);
		}

		//NOTE(martin): command buffer and drawable are set on demand and at the end of each present() call
		surface->drawable = nil;
		surface->commandBuffer = nil;
	}
	return((mp_graphics_surface_data*)surface);
}




extern "C" mp_graphics_surface_data* mp_metal_surface_create_for_window(mp_window window)
{
	mp_window_data* windowData = mp_window_ptr_from_handle(window);
	if(!windowData)
	{
		//TODO(martin): use handles also for surface...
		return(0);
	}
	else
	{
		return(mp_metal_surface_create_for_window_ptr(windowData));
	}
}


///////////////////TODO context destroy

#undef LOG_SUBSYSTEM
