/************************************************************//**
*
*	@file: graphics_internal.h
*	@author: Martin Fouilleul
*	@date: 10/10/2020
*	@revision:
*
*****************************************************************/
#ifndef __GRAPHICS_INTERNAL_H_
#define __GRAPHICS_INTERNAL_H_

#include"graphics.h"

typedef struct mp_graphics_vertex
{
	vec2 pos;
	mp_graphics_color col;
	vec4 uv;
	int zIndex;

} mp_graphics_vertex ;

typedef struct mp_graphics_surface_vertex_layout
{
	u32 maxVertexCount;
	u32 maxIndexCount;

	void* posBuffer;
	u32 posStride;

	void* uvBuffer;
	u32 uvStride;

	void* colorBuffer;
	u32 colorStride;

	void* zIndexBuffer;
	u32 zIndexStride;

	void* clipsBuffer;
	u32 clipsStride;

	void* indexBuffer;
	u32 indexStride;

} mp_graphics_surface_vertex_layout;

typedef struct mp_graphics_surface_data mp_graphics_surface_data;

typedef void (*mp_graphics_surface_destroy_function)(mp_graphics_surface_data* surface);
typedef void (*mp_graphics_surface_draw_buffers_function)(mp_graphics_surface_data* surface, u32 vertexCount, u32 indexCount, mp_graphics_color clearColor);
typedef void (*mp_graphics_surface_window_resized_function)(mp_graphics_surface_data* surface, u32 width, u32 height);
typedef void (*mp_graphics_surface_present_function)(mp_graphics_surface_data* surface);

typedef mp_graphics_surface_vertex_layout (*mp_graphics_surface_vertex_layout_function)(mp_graphics_surface_data* surface);

typedef struct mp_graphics_surface_data
{
	mp_graphics_surface_destroy_function destroy;
	mp_graphics_surface_draw_buffers_function draw_buffers;
	mp_graphics_surface_window_resized_function window_resized;
	mp_graphics_surface_present_function present;

	mp_graphics_surface_vertex_layout_function get_vertex_layout;

} mp_graphics_surface_data;



#endif //__GRAPHICS_INTERNAL_H_
