/************************************************************//**
*
*	@file: milepost.c
*	@author: Martin Fouilleul
*	@date: 13/02/2021
*	@revision:
*
*****************************************************************/

#include"util/debug_log.c"
#include"memory.c"
#include"strings.c"
#include"util/utf8.c"

#include"graphics/graphics.c"
#include"app/gui.c"

//TODO: guard these under platform-specific #ifdefs
#include"platform/unix_base_allocator.c"
#include"platform/osx_clock.c"
#include"platform/unix_rng.c"
#include"platform/posix_thread.c"
#include"platform/posix_socket.c"
