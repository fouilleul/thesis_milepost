/************************************************************//**
*
*	@file: milepost.h
*	@author: Martin Fouilleul
*	@date: 13/02/2021
*	@revision:
*
*****************************************************************/
#ifndef __MILEPOST_H_
#define __MILEPOST_H_


#include"typedefs.h"
#include"macro_helpers.h"
#include"debug_log.h"
#include"lists.h"
#include"memory.h"
#include"strings.h"
#include"utf8.h"
#include"random.h"

#include"platform_clock.h"
#include"platform_rng.h"
#include"platform_socket.h"
#include"platform_thread.h"

#include"platform_app.h"
#include"graphics.h"
#include"gui.h"

#endif //__MILEPOST_H_
