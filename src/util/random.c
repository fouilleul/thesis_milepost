/************************************************************//**
*
*	@file: random.c
*	@author: Martin Fouilleul
*	@date: 15/03/2020
*	@revision:
*
*****************************************************************/
#include<math.h>

#include"random.h"
#include"debug_log.h"

f64 RandomUniform()
{
	//NOTE(martin): uniform distribution in [0,1]
	u64 u = RandomU64();
	return(u/(f64)UINT64_MAX);
}

f64 RandomNormal(f64 mu, f64 sigma)
{
	//NOTE(martin): normal distribution N(mu, sigma^2), where sigma > 0
	DEBUG_ASSERT(sigma > 0, "sigma must greater than 0");

	//NOTE(martin): draw two samples from a standard uniform distribution
	f64 u = RandomUniform();
	f64 v = RandomUniform();

	//NOTE(martin): transform range to (0,1) (open bounds)
	//              the least f64 number greater than 0 is a = DBL_MIN.
	//              the greatest f64 number less than 1 is b = 1-0.5*DBL_EPSILON
	//		we open the interval by computing u = a + (b-a)*u

	u = DBL_MIN + (1-0.5*DBL_EPSILON-DBL_MIN)*u ;
	v = DBL_MIN + (1-0.5*DBL_EPSILON-DBL_MIN)*v ;

	DEBUG_ASSERT(  u != 0 && u != 1&& v != 0 && v != 1);

	//NOTE(martin): apply the box/mueller transformation to get a normally distributed variable,
	//              and transform it according to the desired parameters

	f64 x = sqrt(-2*log(u))*cos(2*M_PI*v);
	x = mu + sigma*x;

	DEBUG_ASSERT(!isnan(x) && !isinf(x), "should not generate nan or inf values");
	return(x);
}

f64 RandomLognormal(f64 mu, f64 sigma)
{
	//NOTE(martin): lognormal distribution exp(N(mu, sigma^2))
	f64 n = RandomNormal(mu, sigma);
	f64 x = exp(n);

	DEBUG_ASSERT(!isnan(x) && !isinf(x), "should not generate nan or inf values");
	return(x);
}

f64 RandomPareto(f64 scale, f64 shape)
{
	//NOTE(martin): pareto distribution, where scale > 0, shape > 0
	DEBUG_ASSERT(scale > 0, "scale must be greater than 0");
	DEBUG_ASSERT(shape > 0, "shape must be greater than 0");

	f64 u = RandomUniform();
	f64 x = scale / pow(u, 1./shape);

	DEBUG_ASSERT(!isnan(x) && !isinf(x), "should not generate nan or inf values");
	return(x);
}
