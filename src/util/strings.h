/************************************************************//**
*
*	@file: strings.h
*	@author: Martin Fouilleul
*	@date: 29/05/2021
*	@revision:
*
*****************************************************************/
#ifndef __STRINGS_H_
#define __STRINGS_H_

#include"typedefs.h"
#include"lists.h"
#include"memory.h"

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------------------
// string slices as values
//----------------------------------------------------------------------------------
typedef struct mp_string
{
	u64 len;
	char* ptr;
} mp_string;

#define mp_string_lit(s) ((mp_string){.len = sizeof(s)-1, .ptr = (char*)(s)})
#define mp_string_unbox(s) (int)((s).len), ((s).ptr)

mp_string mp_string_from_buffer(u64 len, char* buffer);
mp_string mp_string_from_cstring(char* str);
mp_string mp_string_slice(mp_string s, u64 start, u64 end);

mp_string mp_string_push_buffer(mem_arena* arena, u64 len, char* buffer);
mp_string mp_string_push_cstring(mem_arena* arena, const char* str);
mp_string mp_string_push_copy(mem_arena* arena, mp_string s);
mp_string mp_string_push_slice(mem_arena* arena, mp_string s, u64 start, u64 end);

mp_string mp_string_pushfv(mem_arena* arena, const char* format, va_list args);
mp_string mp_string_pushf(mem_arena* arena, const char* format, ...);

int mp_string_cmp(mp_string s1, mp_string s2);

char* mp_string_to_cstring(mem_arena* arena, mp_string string);
//----------------------------------------------------------------------------------
// string lists
//----------------------------------------------------------------------------------
typedef struct mp_string_elt
{
	list_elt listElt;
	mp_string string;
} mp_string_elt;

typedef struct mp_string_list
{
	list_info list;
	u64 eltCount;
	u64 len;
} mp_string_list;

void mp_string_list_push(mem_arena* arena, mp_string_list* list, mp_string str);
void mp_string_list_pushf(mem_arena* arena, mp_string_list* list, const char* format, ...);

mp_string mp_string_list_join(mem_arena* arena, mp_string_list list);
mp_string_list mp_string_split(mem_arena* arena, mp_string str, mp_string_list separators);

//----------------------------------------------------------------------------------
// u32 strings
//----------------------------------------------------------------------------------
typedef struct mp_string32
{
	u64 len;
	u32* ptr;
} mp_string32;

mp_string32 mp_string32_from_buffer(u64 len, u32* buffer);
mp_string32 mp_string32_slice(mp_string32 s, u64 start, u64 end);

mp_string32 mp_string32_push_buffer(mem_arena* arena, u64 len, u32* buffer);
mp_string32 mp_string32_push_copy(mem_arena* arena, mp_string32 s);
mp_string32 mp_string32_push_slice(mem_arena* arena, mp_string32 s, u64 start, u64 end);


//----------------------------------------------------------------------------------
// Paths helpers
//----------------------------------------------------------------------------------
mp_string mp_path_directory(mp_string fullPath);
mp_string mp_path_base_name(mp_string fullPath);

#ifdef __cplusplus
} // extern "C"
#endif


#endif //__STRINGS_H_
