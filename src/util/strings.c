/************************************************************//**
*
*	@file: strings.c
*	@author: Martin Fouilleul
*	@date: 29/05/2021
*	@revision:
*
*****************************************************************/
#include<string.h>	// strlen(), strcpy(), etc.
#include<stdarg.h>	// va_list() etc.
#include"debug_log.h"
#include"strings.h"

#define LOG_SUBSYSTEM "Strings"

//----------------------------------------------------------------------------------
// string slices as values
//----------------------------------------------------------------------------------

mp_string mp_string_from_buffer(u64 len, char* buffer)
{
	return((mp_string){.len = len, .ptr = buffer});
}

mp_string mp_string_from_cstring(char* str)
{
	return(mp_string_from_buffer(strlen(str), (char*)str));
}

mp_string mp_string_slice(mp_string s, u64 start, u64 end)
{
	ASSERT(start <= end && start <= s.len && end <= s.len);
	return((mp_string){.len = end - start, .ptr = s.ptr + start});
}

mp_string mp_string_push_buffer(mem_arena* arena, u64 len, char* buffer)
{
	mp_string str = {};
	str.len = len;
	str.ptr = mem_arena_alloc_array(arena, char, len);
	memcpy(str.ptr, buffer, len);
	return(str);
}

mp_string mp_string_push_cstring(mem_arena* arena, const char* str)
{
	return(mp_string_push_buffer(arena, strlen(str), (char*)str));
}

mp_string mp_string_push_copy(mem_arena* arena, mp_string s)
{
	return(mp_string_push_buffer(arena, mp_string_unbox(s)));
}

mp_string mp_string_push_slice(mem_arena* arena, mp_string s, u64 start, u64 end)
{
	mp_string slice = mp_string_slice(s, start, end);
	return(mp_string_push_copy(arena, slice));
}

mp_string mp_string_pushfv(mem_arena* arena, const char* format, va_list args)
{
	//NOTE(martin):
	//	We first compute the number of characters to write passing a size of 0.
	//      then we allocate len+1 (since vsnprint always terminates with a '\0').
	//      We could call vsprintf since we know the size, but I'm not sure there's a hard
	//      guarantee that vsprintf and vsnprintf write the same number of characters in all
	//      and every case, and that would be a difficult bug to spot, so it seems better to
	//      waste one byte and be safe.
	char dummy;
	mp_string str = {};
	va_list argCopy;
	va_copy(argCopy, args);
	str.len = vsnprintf(&dummy, 0, format, argCopy);
	va_end(argCopy);

	str.ptr = mem_arena_alloc_array(arena, char, str.len + 1);
	vsnprintf((char*)str.ptr, str.len + 1, format, args);
	return(str);
}

mp_string mp_string_pushf(mem_arena* arena, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	mp_string str = mp_string_pushfv(arena, format, args);
	va_end(args);
	return(str);
}

int mp_string_cmp(mp_string s1, mp_string s2)
{
	int res = strncmp(s1.ptr, s2.ptr, minimum(s1.len, s2.len));
	if(!res)
	{
		res = (s1.len < s2.len)? -1 : ((s1.len == s2.len)? 0 : 1);
	}
	return(res);
}

char* mp_string_to_cstring(mem_arena* arena, mp_string string)
{
	char* cstr = mem_arena_alloc_array(arena, char, string.len+1);
	memcpy(cstr, string.ptr, string.len);
	cstr[string.len] = '\0';
	return(cstr);
}

//----------------------------------------------------------------------------------
// string lists
//----------------------------------------------------------------------------------

void mp_string_list_init(mp_string_list* list)
{
	ListInit(&list->list);
	list->eltCount = 0;
	list->len = 0;
}

void mp_string_list_push(mem_arena* arena, mp_string_list* list, mp_string str)
{
	mp_string_elt* elt = mem_arena_alloc_type(arena, mp_string_elt);
	elt->string = str;
	ListAppend(&list->list, &elt->listElt);
	list->eltCount++;
	list->len += str.len;
}

void mp_string_list_pushf(mem_arena* arena, mp_string_list* list, const char* format, ...)
{
	va_list args;
	va_start(args, format);
	mp_string str = mp_string_pushfv(arena, format, args);
	va_end(args);
	mp_string_list_push(arena, list, str);
}

mp_string mp_string_list_collate(mem_arena* arena, mp_string_list list, mp_string prefix, mp_string separator, mp_string postfix)
{
	mp_string str = {};
	str.len = prefix.len + list.len + list.eltCount*separator.len + postfix.len;
	str.ptr = mem_arena_alloc_array(arena, char, str.len);
	char* dst = str.ptr;
	memcpy(dst, prefix.ptr, prefix.len);
	dst += prefix.len;

	mp_string_elt* elt = ListFirstEntry(&list.list, mp_string_elt, listElt);
	if(elt)
	{
		memcpy(dst, elt->string.ptr, elt->string.len);
		dst += elt->string.len;
		elt = ListNextEntry(&list.list, elt, mp_string_elt, listElt);
	}

	for( ; elt != 0; elt = ListNextEntry(&list.list, elt, mp_string_elt, listElt))
	{
		memcpy(dst, separator.ptr, separator.len);
		dst += separator.len;
		memcpy(dst, elt->string.ptr, elt->string.len);
		dst += elt->string.len;
	}
	memcpy(dst, postfix.ptr, postfix.len);
	return(str);
}

mp_string mp_string_list_join(mem_arena* arena, mp_string_list list)
{
	mp_string empty = {.len = 0, .ptr = 0};
	return(mp_string_list_collate(arena, list, empty, empty, empty));
}

mp_string_list mp_string_split(mem_arena* arena, mp_string str, mp_string_list separators)
{
	mp_string_list list = {};
	ListInit(&list.list);

	char* ptr = str.ptr;
	char* end = str.ptr + str.len;
	char* subStart = ptr;
	for(; ptr < end; ptr++)
	{
		//NOTE(martin): search all separators and try to match them to the current ptr
		mp_string* foundSep = 0;
		for_each_in_list(&separators.list, elt, mp_string_elt, listElt)
		{
			mp_string* separator = &elt->string;
			bool equal = true;
			for(u64 offset = 0;
			    (offset < separator->len) && (ptr+offset < end);
			    offset++)
			{
				if(separator->ptr[offset] != ptr[offset])
				{
					equal = false;
					break;
				}
			}
			if(equal)
			{
				foundSep = separator;
				break;
			}
		}
		if(foundSep)
		{
			//NOTE(martin): we found a separator. If the start of the current substring is != ptr,
			//              the current substring is not empty and we emit the substring
			if(ptr != subStart)
			{
				mp_string sub = mp_string_from_buffer(ptr-subStart, subStart);
				mp_string_list_push(arena, &list, sub);
			}
			ptr += foundSep->len - 1; //NOTE(martin): ptr is incremented at the end of the loop
			subStart = ptr+1;
		}
	}
	//NOTE(martin): emit the last substring
	if(ptr != subStart)
	{
		mp_string sub = mp_string_from_buffer(ptr-subStart, subStart);
		mp_string_list_push(arena, &list, sub);
	}
	return(list);
}

//----------------------------------------------------------------------------------
// u32 strings
//----------------------------------------------------------------------------------
mp_string32 mp_string32_from_buffer(u64 len, u32* buffer)
{
	return((mp_string32){.len = len, .ptr = buffer});
}

mp_string32 mp_string32_slice(mp_string32 s, u64 start, u64 end)
{
	ASSERT(start <= end && start <= s.len && end <= s.len);
	return((mp_string32){.len = end - start, .ptr = s.ptr + start});
}

mp_string32 mp_string32_push_buffer(mem_arena* arena, u64 len, u32* buffer)
{
	mp_string32 str = {};
	str.len = len;
	str.ptr = mem_arena_alloc_array(arena, u32, len);
	memcpy(str.ptr, buffer, len*sizeof(u32));
	return(str);
}

mp_string32 mp_string32_push_copy(mem_arena* arena, mp_string32 s)
{
	return(mp_string32_push_buffer(arena, s.len, s.ptr));
}

mp_string32 mp_string32_push_slice(mem_arena* arena, mp_string32 s, u64 start, u64 end)
{
	mp_string32 slice = mp_string32_slice(s, start, end);
	return(mp_string32_push_copy(arena, slice));
}

//----------------------------------------------------------------------------------
// Paths helpers
//----------------------------------------------------------------------------------
mp_string mp_path_directory(mp_string fullPath)
{
	i64 lastSlashIndex = -1;

	for(i64 i = fullPath.len-1; i >= 0; i--)
	{
		if(fullPath.ptr[i] == '/')
		{
			lastSlashIndex = i;
			break;
		}
	}
	mp_string directory = mp_string_slice(fullPath, 0, lastSlashIndex+1);
	return(directory);
}

mp_string mp_path_base_name(mp_string fullPath)
{
	i64 lastSlashIndex = -1;

	for(i64 i = fullPath.len-1; i >= 0; i--)
	{
		if(fullPath.ptr[i] == '/')
		{
			lastSlashIndex = i;
			break;
		}
	}

	mp_string basename = mp_string_slice(fullPath, lastSlashIndex+1, fullPath.len);
	return(basename);
}



#undef LOG_SUBSYSTEM
