/************************************************************//**
*
*	@file: random.h
*	@author: Martin Fouilleul
*	@date: 15/03/2020
*	@revision:
*
*****************************************************************/
#ifndef __RANDOM_H_
#define __RANDOM_H_

#include"platform_rng.h"

f64 RandomUniform();				// uniform distribution in [0,1]
f64 RandomNormal(f64 mu, f64 sigma);    // normal distribution N(mu, sigma^2), where sigma > 0
f64 RandomLognormal(f64 mu, f64 sigma); // lognormal distribution exp(N(mu, sigma^2))
f64 RandomPareto(f64 scale, f64 shape);         // pareto distribution, where scale > 0, shape > 0


#endif //__RANDOM_H_
