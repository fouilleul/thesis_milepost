
#include<stdio.h>
#include<unistd.h>

#define LOG_DEFAULT_LEVEL LOG_LEVEL_MESSAGE
#define LOG_COMPILE_DEBUG

#include"milepost.h"

#define LOG_SUBSYSTEM "Main"


struct my_app
{
	mp_window window;
	mp_graphics_font font;
	mp_graphics_surface surface;
	mp_graphics_context context;

	u32 codePointCount;
	u32* codePoints;
};

void draw_stuff(mp_graphics_context context, mp_graphics_font font, mp_graphics_surface surface, u32 codePointCount, utf32* codePoints)
{
	f64 startFrameTime = ClockGetTime(SYS_CLOCK_MONOTONIC);

	mp_graphics_font_extents extents;
	mp_graphics_font_get_extents(context, font, &extents);
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(context, font, 32);

	f32 lineHeight = fontScale*(extents.ascent + extents.descent + extents.leading);
	f32 textX = 10;
	f32 textY = 1200 - lineHeight;

	mp_graphics_set_clear_color_rgba(context, 1, 1, 1, 1);
	mp_graphics_clear(context);

	mp_graphics_set_font(context, font);
	mp_graphics_set_font_size(context, 32);
	mp_graphics_set_color_rgba(context, 0, 0, 0, 1);

	mp_graphics_move_to(context, textX, textY);

	int startIndex = 0;
	while(startIndex < codePointCount)
	{
		bool lineBreak = false;
		int subIndex = 0;
		for(; (startIndex+subIndex) < codePointCount && subIndex < 512; subIndex++)
		{
			if(codePoints[startIndex + subIndex] == '\n')
			{
				lineBreak = true;
				break;
			}
		}
		ASSERT(subIndex < 512 && (startIndex+subIndex)<=codePointCount);
		u32 glyphs[512];
		mp_graphics_font_get_glyph_indices(context, font, subIndex, codePoints+startIndex, glyphs);

		mp_graphics_glyph_outlines(context, subIndex, glyphs);
		mp_graphics_fill(context);

		if(lineBreak)
		{
			textY -= lineHeight;
			mp_graphics_move_to(context, textX, textY);
			startIndex++;
		}
		startIndex += subIndex;
	}
	f64 startFlushTime = ClockGetTime(SYS_CLOCK_MONOTONIC);
	mp_graphics_context_flush(context, surface);

	f64 startPresentTime = ClockGetTime(SYS_CLOCK_MONOTONIC);
	mp_graphics_surface_present(surface);

	f64 endFrameTime = ClockGetTime(SYS_CLOCK_MONOTONIC);

	f64 frameTime = (endFrameTime - startFrameTime);
	printf("frame time: %.2fms (%.2fFPS), draw = %f.2ms, flush = %.2fms, present = %.2fms\n",
		      frameTime*1000,
		      1./frameTime,
		      (startFlushTime - startFrameTime)*1000,
		      (startPresentTime - startFlushTime)*1000,
		      (endFrameTime - startPresentTime)*1000);

}

void process_event(mp_event* event, void* userData)
{
	my_app* app = (my_app*)userData;
	mp_graphics_context context = app->context;
	mp_graphics_surface surface = app->surface;
	mp_graphics_font font = app->font;

	switch(event->type)
	{
		case MP_EVENT_KEYBOARD_CHAR:
		{
			printf("entered char %s\n", event->character.sequence);
		} break;

		case MP_EVENT_WINDOW_CLOSE:
		{
			mp_graphics_context_destroy(context);
			mp_graphics_surface_destroy(surface);
			app->window = mp_window_null_handle();

			mp_do_quit();
		} break;

		case MP_EVENT_FRAME:
		{

			if(!mp_window_handle_is_null(app->window))
			{
				draw_stuff(context, font, surface, app->codePointCount, app->codePoints);
			}
		} break;

		default:
			break;
	}
}

static const char* TEST_STRING =
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam enim, aliquam in placerat luctus, rutrum in quam.\n" \
"Cras urna elit, pellentesque ac ipsum at, lobortis scelerisque eros. Aenean et turpis nibh. Maecenas lectus augue, eleifend\n" \
"nec efficitur eu, faucibus eget turpis. Suspendisse vel nulla mi. Duis imperdiet neque orci, ac ultrices orci molestie a.\n"
"Etiam malesuada vulputate hendrerit. Cras ultricies diam in lectus finibus, eu laoreet diam rutrum.\n" \
"\n" \
"Etiam dictum orci arcu, ac fermentum leo dapibus lacinia. Integer vitae elementum ex. Vestibulum tempor nunc eu hendrerit\n" \
"ornare. Nunc pretium ligula sit amet massa pulvinar, vitae imperdiet justo bibendum. Maecenas consectetur elementum mi, sed\n" \
"vehicula neque pulvinar sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tortor erat, accumsan in laoreet\n" \
"quis, placerat nec enim. Nulla facilisi. Morbi vitae nibh ligula. Suspendisse in molestie magna, eget aliquet mauris. Sed \n" \
"aliquam faucibus magna.\n" \
"\n" \
"Sed metus odio, imperdiet et consequat non, faucibus nec risus. Suspendisse facilisis sem neque, id scelerisque dui mattis sit\n" \
"amet. Nullam tincidunt nisl nec dui dignissim mattis. Proin fermentum ornare ipsum. Proin eleifend, mi vitae porttitor placerat,\n" \
"neque magna elementum turpis, eu aliquet mi urna et leo. Pellentesque interdum est mauris, sed pellentesque risus blandit in.\n" \
"Phasellus dignissim consequat eros, at aliquam elit finibus posuere. Proin suscipit tortor leo, id vulputate odio lobortis in.\n" \
"Vestibulum et orci ligula. Sed scelerisque nunc non nisi aliquam, vel eleifend felis suscipit. Integer posuere sapien elit, \n" \
"lacinia ultricies nibh sodales nec.\n" \
"\n" \
"Etiam aliquam purus sit amet purus ultricies tristique. Nunc maximus nunc quis magna ornare, vel interdum urna fermentum.\n" \
"Vestibulum cursus nisl ut nulla egestas, quis mattis elit venenatis. Praesent malesuada mi non magna aliquam fringilla eget eu\n" \
"turpis. Integer suscipit elit vel consectetur vulputate. Integer euismod, erat eget elementum tempus, magna metus consectetur\n" \
"elit, sed feugiat urna sapien sodales sapien. Sed sit amet varius nunc. Curabitur sodales nunc justo, ac scelerisque ipsum semper\n" \
"eget. Integer ornare, velit ut hendrerit dapibus, erat mauris commodo justo, vel semper urna justo non mauris. Proin blandit,\n" \
"enim ut posuere placerat, leo nibh tristique eros, ut pulvinar sapien elit eget enim. Pellentesque et mauris lectus. Curabitur\n" \
"quis lobortis leo, sit amet egestas dui. Nullam ut sapien eu justo lacinia ultrices. Ut tincidunt, sem non luctus tempus, felis\n" \
"purus imperdiet nisi, non ultricies libero ipsum eu augue. Mauris at luctus enim.";

int main()
{
	LogLevel(LOG_LEVEL_MESSAGE);
	#if __has_feature(objc_arc)
		LOG_MESSAGE("ARC enabled\n");
	#else
		LOG_MESSAGE("ARC not enabled\n");
	#endif

	LOG_MESSAGE("init\n");
	mp_init();

	LOG_MESSAGE("create window\n");
	mp_aligned_rect rect = {.x = 100, .y = 100, .w = 980, .h = 600};
	mp_window window = mp_window_create(rect, "test", 0);

	mp_window_bring_to_front_and_focus(window);
	mp_set_target_fps(60);

	LOG_MESSAGE("create surface\n");
	mp_graphics_surface surface = mp_window_get_surface(window);

	LOG_MESSAGE("create graphics context\n");
	mp_graphics_context context = mp_graphics_context_create();

	unicode_range ranges[5] = {UNICODE_RANGE_BASIC_LATIN,
	                        UNICODE_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT,
				UNICODE_RANGE_LATIN_EXTENDED_A,
				UNICODE_RANGE_LATIN_EXTENDED_B,
				UNICODE_RANGE_SPECIALS};
	LOG_MESSAGE("load font\n");
	char* fontPath = 0;
	mp_app_get_resource_path("../resources/OpenSansLatinSubset.ttf", &fontPath);
	FILE* fontFile = fopen(fontPath, "r");
	free(fontPath);
	fseek(fontFile, 0, SEEK_END);
	int size = ftell(fontFile);
	rewind(fontFile);
	unsigned char* buffer = (unsigned char*)malloc(size);
	fread(buffer, 1, size, fontFile);
	fclose(fontFile);

	LOG_MESSAGE("create font\n");
	mp_graphics_font font = mp_graphics_font_create_from_memory(context, size, buffer, 5, ranges);

	LOG_MESSAGE("set process callback\n");
	my_app app = {window, font, surface, context};

	app.codePointCount = utf8_codepoint_count_for_string(mp_string_lit(TEST_STRING));
	app.codePoints = malloc_array(utf32, app.codePointCount);
	utf8_to_codepoints(app.codePointCount, app.codePoints, mp_string_lit(TEST_STRING));

	mp_app_set_process_event_callback(process_event, &app);

	mp_event_loop();

	if(!mp_window_handle_is_null(app.window))
	{
		mp_graphics_context_destroy(context);
		mp_graphics_surface_destroy(surface);
		mp_window_destroy(window);
	}
	mp_terminate();
	return(0);
}
