#include<stdio.h>

#define LOG_DEFAULT_LEVEL LOG_LEVEL_DEBUG
#define LOG_COMPILE_DEBUG

#include"milepost.h"

#define LOG_SUBSYSTEM "Main"

const u32 DEMO_WINDOW_DEFAULT_WIDTH = 1600,
          DEMO_WINDOW_DEFAULT_HEIGHT = 1200;

const mp_graphics_color DEMO_COLOR_CLEAR = {0.005, 0.005, 0.005, 1};

typedef struct demo_info
{
	mp_graphics_surface surface;
	mp_graphics_context graphics;
	mp_graphics_font font;
	mp_gui_context* gui;

	f32 windowWidth;
	f32 windowHeight;

} demo_info;

void demo_gui(demo_info* demo)
{
	mp_gui_context* gui = demo->gui;

	mp_gui_begin_frame(gui);
	{
		mp_gui_view_flags flags = MP_GUI_VIEW_FLAG_ROOT
		                        | MP_GUI_VIEW_FLAG_TITLED
		                        | MP_GUI_VIEW_FLAG_RESIZEABLE
		                        | MP_GUI_VIEW_FLAG_SCROLL;

		mp_gui_begin_view(gui, "Test view 3", (mp_aligned_rect){700, 200, 600, 800}, flags);
		{
			const int optionCount = 4;
			mp_string options[4] = {mp_string_lit("option one"),
			                        mp_string_lit("option two"),
			                        mp_string_lit("option three"),
			                        mp_string_lit("option four")};

			static int optionIndex = 0;

			if(mp_gui_popup_menu(gui, "popup", (mp_aligned_rect){300, 600, 300, 50}, optionCount, options, &optionIndex))
			{
				printf("selected option #%i (%.*s)\n", optionIndex, (int)options[optionIndex].len, options[optionIndex].ptr);
			}

		} mp_gui_end_view(gui);

		mp_gui_begin_view(gui, "Test view 2", (mp_aligned_rect){500, 400, 600, 450}, flags);
		{
			static utf32 codePoints[256];
			static u32 codePointsSize = 0;
			if(mp_gui_text_field(gui, "text", (mp_aligned_rect){50, 50, 500, 80}, 256, &codePointsSize, codePoints, 0))
			{
				//...
			}
			//TODO: show modifiers keys and last key pressed...

			static bool check = false;
			mp_gui_checkbox(gui, "check", (mp_aligned_rect){50, 150, 50, 50}, &check);

		} mp_gui_end_view(gui);

		mp_gui_begin_view(gui, "Test view 1", (mp_aligned_rect){200, 100, 600, 450}, flags);
		{
			if(mp_gui_text_button(gui, "Button 1", (mp_aligned_rect){50, 40, 200, 80}))
			{
			}

			if(mp_gui_text_button(gui, "Button 2", (mp_aligned_rect){50, 140, 200, 80}))
			{
			}

			if(mp_gui_text_button(gui, "Button 3", (mp_aligned_rect){50, 240, 200, 80}))
			{
			}

			static f32 slider1 = 0;
			static f32 slider2 = 0;
			static f32 slider3 = 0;

			mp_gui_slider(gui, "slider_1", (mp_aligned_rect){300, 40, 200, 50}, 80, &slider1);
			mp_gui_slider(gui, "slider_2", (mp_aligned_rect){300, 140, 200, 50}, 80, &slider2);
			mp_gui_slider(gui, "slider_3", (mp_aligned_rect){300, 240, 200, 50}, 80, &slider3);

		} mp_gui_end_view(gui);


	} mp_gui_end_frame(gui);
}

void demo_event_callback(mp_event* event, void* userData)
{
	demo_info* demo = (demo_info*)userData;

	mp_gui_process_event(mp_gui_context_get_io(demo->gui), event);
	switch(event->type)
	{
		case MP_EVENT_WINDOW_CLOSE:
		{
			mp_do_quit();
		} break;

		case MP_EVENT_WINDOW_RESIZE:
		{
			demo->windowWidth = event->frame.rect.w * 2;
			demo->windowHeight = event->frame.rect.h * 2;
		} break;

		case MP_EVENT_FRAME:
		{
			mp_graphics_set_clear_color(demo->graphics, DEMO_COLOR_CLEAR);
			mp_graphics_clear(demo->graphics);

			demo_gui(demo);

			mp_graphics_context_flush(demo->graphics, demo->surface);
			mp_graphics_surface_present(demo->surface);
		} break;

		default:
			break;
	}
}

int main()
{
	LogLevel(LOG_LEVEL_DEBUG);

	//NOTE(martin): load font
	char* fontPath = 0;
	mp_app_get_resource_path("../resources/Andale Mono.ttf", &fontPath);
	FILE* fontFile = fopen(fontPath, "r");
	free(fontPath);
	if(!fontFile)
	{
		LOG_ERROR("Could not load font file '%s'\n", fontPath);
		return(-1);
	}
	unsigned char* fontData = 0;
	fseek(fontFile, 0, SEEK_END);
	u32 fontDataSize = ftell(fontFile);
	rewind(fontFile);
	fontData = (unsigned char*)malloc(fontDataSize);
	fread(fontData, 1, fontDataSize, fontFile);
	fclose(fontFile);

	//NOTE(martin): init app and create window
	mp_init();

	mp_aligned_rect windowRect = {.x = 100, .y = 100, .w = 0.5*DEMO_WINDOW_DEFAULT_WIDTH, .h = 0.5*DEMO_WINDOW_DEFAULT_HEIGHT};

	mp_window window = mp_window_create(windowRect, "GUI Demo", 0);

	//NOTE(martin): create graphics context
	mp_graphics_surface surface = mp_window_get_surface(window);
	mp_graphics_context graphics = mp_graphics_context_create();

	//NOTE(martin): create font
	unicode_range ranges[5] = {UNICODE_RANGE_BASIC_LATIN,
	                        UNICODE_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT,
				UNICODE_RANGE_LATIN_EXTENDED_A,
				UNICODE_RANGE_LATIN_EXTENDED_B,
				UNICODE_RANGE_SPECIALS};

	mp_graphics_font font = mp_graphics_font_create_from_memory(graphics, fontDataSize, fontData, 5, ranges);
	free(fontData);
	f32 fontSize = 34;
	f32 fontScale = mp_graphics_font_get_scale_for_em_pixels(graphics, font, fontSize);
	mp_graphics_font_extents fontExtents;
	mp_graphics_font_get_extents(graphics, font, &fontExtents);

	mp_graphics_set_clear_color(graphics, DEMO_COLOR_CLEAR);
	mp_graphics_set_font(graphics, font);
	mp_graphics_set_font_size(graphics, fontSize);

	demo_info demo;
	demo.surface = surface;
	demo.graphics = graphics;
	demo.font = font;
	demo.gui = mp_gui_context_create(DEMO_WINDOW_DEFAULT_WIDTH, DEMO_WINDOW_DEFAULT_HEIGHT, demo.graphics, font);

	mp_app_set_process_event_callback(demo_event_callback, &demo);

	//NOTE(martin): run

	mp_window_bring_to_front_and_focus(window);

	mp_event_loop();

	//NOTE(martin): cleanup
	mp_graphics_font_destroy(demo.graphics, font);
	mp_graphics_context_destroy(demo.graphics);
	mp_window_destroy(window);
	mp_terminate();

	return(0);
}

#undef LOG_SUBSYSTEM
