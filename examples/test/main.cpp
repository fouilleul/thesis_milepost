
#include<stdio.h>
#include<unistd.h>

#define LOG_DEFAULT_LEVEL LOG_LEVEL_MESSAGE
#define LOG_COMPILE_DEBUG

#include"milepost.h"

#define LOG_SUBSYSTEM "Main"


struct my_app
{
	mp_window window;
	mp_graphics_font font;
	mp_graphics_surface surface;
	mp_graphics_context context;
};

void draw_stuff(mp_graphics_context context, mp_graphics_font font, mp_graphics_surface surface)
{
	mp_graphics_matrix_push(context, (mp_mat2x3){1, 0, 800,
					             0, 1, 600});

	//draw stuff
	mp_graphics_set_clear_color_rgba(context, 1, 1, 1, 1);
	mp_graphics_clear(context);

/*
	mp_graphics_set_color_rgba(context, 0, 0, 1, 1);
	mp_graphics_move_to(context, 0, 0);
	mp_graphics_line_to(context, 200, 0);
	mp_graphics_line_to(context, 200, 200);
	mp_graphics_fill(context);
//*/


//*
	mp_graphics_set_color_rgba(context, 1, 0, 0, 1);
	mp_graphics_move_to(context, 0, 0);
	mp_graphics_cubic_to(context, 0, 100, 200, 100, 200, 200);
	mp_graphics_cubic_to(context, 200, 300, 300, 300, 400, 300);
	mp_graphics_cubic_to(context, 500, 300, 500, 200, 500, 100);
	mp_graphics_cubic_to(context, 500, 0, 300, 100, 300, 0);
	mp_graphics_cubic_to(context, 300, -100, 0, -100, 0, 0);
	mp_graphics_fill(context);

	mp_graphics_clip_push(context, 0, 0, 200, 300);

	mp_graphics_set_color_rgba(context, 0, 0, 1, 1);
	mp_graphics_move_to(context, -100, 0);
	mp_graphics_cubic_to(context, -100, 100, 100, 100, 100, 200);
	mp_graphics_cubic_to(context, 100, 300, 200, 300, 300, 300);
	mp_graphics_cubic_to(context, 400, 300, 400, 200, 400, 100);
	mp_graphics_cubic_to(context, 400, 0, 200, 100, 200, 0);
	mp_graphics_cubic_to(context, 200, -100, -100, -100, -100, 0);
	mp_graphics_fill(context);

	mp_graphics_clip_pop(context);

	mp_graphics_set_width(context, 10);
	mp_graphics_set_tolerance(context, 1);
	mp_graphics_set_joint(context, MP_GRAPHICS_JOINT_MITER);
	mp_graphics_set_max_joint_excursion(context, 32);
	mp_graphics_set_color_rgba(context, 0, 1, 0, 1);

	mp_graphics_set_cap(context, MP_GRAPHICS_CAP_SQUARE);

	mp_graphics_move_to(context, -100, 0);
	mp_graphics_cubic_to(context, -100, 300, 200, 0, 200, 300);
	mp_graphics_cubic_to(context, 300, 200, 300, 400, 400, 100);
	mp_graphics_close_path(context);

	mp_graphics_move_to(context, 0, -100);
	mp_graphics_line_to(context, 100, -100);

	mp_graphics_move_to(context, 150, -100);
	mp_graphics_line_to(context, 250, -100);

	mp_graphics_move_to(context, 300, -100);
	mp_graphics_line_to(context, 400, -100);

	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0, 0, 1, 1);
	mp_graphics_rectangle_fill(context, -300, -100, 100, 100);

	mp_graphics_set_width(context, 10);
	mp_graphics_rectangle_stroke(context, -300, -300, 100, 100);

	mp_graphics_set_color_rgba(context, 1, 0, 0, 1);
	mp_graphics_rounded_rectangle_fill(context, -200, 200, 200, 100, 30);

	mp_graphics_set_color_rgba(context, 1, 0, 1, 1);
	mp_graphics_rounded_rectangle_stroke(context, -600, -500, 200, 100, 30);

	mp_graphics_ellipse_stroke(context, -700, -300, 100, 70);
	mp_graphics_set_width(context, 5);

	f32 fontSize = 48*2;

	mp_graphics_font_extents extents;
	mp_graphics_font_get_extents(context, font, &extents);
	f32 scale = mp_graphics_font_get_scale_for_em_pixels(context, font, fontSize);

	mp_graphics_text_extents aExtents;
	mp_graphics_font_get_codepoint_extents(context, font, 'a', &aExtents);

	mp_graphics_text_extents bExtents;
	mp_graphics_font_get_codepoint_extents(context, font, 226, &bExtents);


	mp_graphics_set_width(context, 2);

	mp_graphics_set_color_rgba(context, 1, 0, 0, 1);
	mp_graphics_move_to(context, 0, 0);
	mp_graphics_line_to(context, 1500, 0);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0, 1, 0, 1);
	mp_graphics_move_to(context, 0, extents.ascent * scale);
	mp_graphics_line_to(context, 1500, extents.ascent * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0, 0, 1, 1);
	mp_graphics_move_to(context, 0, extents.xHeight * scale);
	mp_graphics_line_to(context, 1500, extents.xHeight * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 1, 0, 1, 1);
	mp_graphics_move_to(context, 0, -extents.descent * scale);
	mp_graphics_line_to(context, 1500, -extents.descent * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0, 1, 1, 1);
	mp_graphics_move_to(context, 0, -extents.descent * scale);
	mp_graphics_line_to(context, 0, extents.ascent * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0.5, 1, 0.5, 1);
	mp_graphics_move_to(context, aExtents.xBearing * scale, -extents.descent * scale);
	mp_graphics_line_to(context, aExtents.xBearing * scale, extents.ascent * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0.5, 0.5, 1, 1);
	mp_graphics_move_to(context, aExtents.xAdvance * scale, -extents.descent * scale);
	mp_graphics_line_to(context, aExtents.xAdvance * scale, extents.ascent * scale);
	mp_graphics_stroke(context);

	mp_graphics_set_color_rgba(context, 0.5, 0.5, 1, 1);
	mp_graphics_move_to(context, (aExtents.xAdvance + bExtents.xBearing) * scale, -extents.descent * scale);
	mp_graphics_line_to(context, (aExtents.xAdvance + bExtents.xBearing) * scale, extents.ascent * scale);
	mp_graphics_stroke(context);


	mp_graphics_set_font(context, font);
	mp_graphics_set_font_size(context, fontSize);
	mp_graphics_set_color_rgba(context, 0, 0, 0, 1);

	mp_graphics_move_to(context, 0, 0);

	mp_string string = mp_string_lit("aâ\ndéfghijklmnq");
	utf32 codepoints[16];
	utf8_to_codepoints(16, codepoints, string);
	u32 glyphs[16];
	mp_graphics_font_get_glyph_indices(context, font, 16, codepoints, glyphs);
	mp_graphics_glyph_outlines(context, 16, glyphs);
	mp_graphics_fill(context);

	mp_graphics_move_to(context, 0, -200);
	mp_graphics_set_font_size(context, 40);
	mp_graphics_glyph_outlines(context, 16, glyphs);
	mp_graphics_fill(context);
//*/

	mp_graphics_set_color_rgba(context, 1, 1, 0, 0.6);
	mp_graphics_rectangle_fill(context, -10, -10, 200, 100);

	mp_graphics_matrix_pop(context);

	mp_graphics_context_flush(context, surface);
	mp_graphics_surface_present(surface);
}

void process_event(mp_event* event, void* userData)
{
	my_app* app = (my_app*)userData;
	mp_graphics_context context = app->context;
	mp_graphics_surface surface = app->surface;
	mp_graphics_font font = app->font;

	switch(event->type)
	{
		case MP_EVENT_KEYBOARD_CHAR:
		{
			printf("entered char %s\n", event->character.sequence);
		} break;

		case MP_EVENT_WINDOW_CLOSE:
		{
			mp_graphics_context_destroy(context);
			mp_graphics_surface_destroy(surface);
			app->window = mp_window_null_handle();

			mp_do_quit();
		} break;

		case MP_EVENT_FRAME:
		{
			if(!mp_window_handle_is_null(app->window))
			{
				draw_stuff(context, font, surface);
			}
		} break;

		default:
			break;
	}
}

int main()
{
	LogLevel(LOG_LEVEL_MESSAGE);
	#if __has_feature(objc_arc)
		LOG_MESSAGE("ARC enabled\n");
	#else
		LOG_MESSAGE("ARC not enabled\n");
	#endif

	LOG_MESSAGE("init\n");
	mp_init();

	LOG_MESSAGE("create window\n");
	mp_aligned_rect rect = {.x = 100, .y = 100, .w = 800, .h = 600};
	mp_window window = mp_window_create(rect, "test", 0);

	mp_window_bring_to_front_and_focus(window);
	mp_set_target_fps(60);

	LOG_MESSAGE("create surface\n");
	mp_graphics_surface surface = mp_window_get_surface(window);

	LOG_MESSAGE("create graphics context\n");
	mp_graphics_context context = mp_graphics_context_create();

	unicode_range ranges[5] = {UNICODE_RANGE_BASIC_LATIN,
	                        UNICODE_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT,
				UNICODE_RANGE_LATIN_EXTENDED_A,
				UNICODE_RANGE_LATIN_EXTENDED_B,
				UNICODE_RANGE_SPECIALS};
	LOG_MESSAGE("load font\n");
	char* fontPath = 0;
	mp_app_get_resource_path("../resources/OpenSansLatinSubset.ttf", &fontPath);
	FILE* fontFile = fopen(fontPath, "r");
	free(fontPath);
	fseek(fontFile, 0, SEEK_END);
	int size = ftell(fontFile);
	rewind(fontFile);
	unsigned char* buffer = (unsigned char*)malloc(size);
	fread(buffer, 1, size, fontFile);
	fclose(fontFile);

	LOG_MESSAGE("create font\n");
	mp_graphics_font font = mp_graphics_font_create_from_memory(context, size, buffer, 5, ranges);

	LOG_MESSAGE("set process callback\n");
	my_app app = {window, font, surface, context};
	mp_app_set_process_event_callback(process_event, &app);

	mp_event_loop();

	if(!mp_window_handle_is_null(app.window))
	{
		mp_graphics_context_destroy(context);
		mp_graphics_surface_destroy(surface);
		mp_window_destroy(window);
	}
	mp_terminate();
	return(0);
}
