
#include<stdio.h>
#include"memory.c"
#include"strings.c"


int main()
{
	mem_arena arena;
	mem_arena_init(&arena);

	mp_string string = mp_string_lit("apple, banana, , pear, mango,, strawberry, ");
	mp_string splitters[2] = {mp_string_lit(","), mp_string_lit(" ")};

	mp_string_list list = mp_string_split(&arena, string, 2, splitters);

	for_each_in_list(&list.list, elt, mp_string_elt, listElt)
	{
		printf("%.*s\n", mp_string_unbox(elt->string));
	}

	mp_string joined = mp_string_list_collate(&arena, &list, mp_string_lit("{"), mp_string_lit(", "), mp_string_lit("}"));

	printf("%.*s\n", mp_string_unbox(joined));

	return(0);
}
